//
//  CIGAboutViewController.swift
//  CIG
//
//  Created by Harry on 11/1/17.
//  Copyright © 2017 Harry. All rights reserved.
//

import UIKit

class CIGAboutViewController: UIViewController {

    @IBOutlet var lblVersion: UILabel!
    @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if (UIScreen.main.nativeBounds.height == 2436) {
            self.headerViewHeightConstraint.constant = self.headerViewHeightConstraint.constant + 20
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBackClicked(_ sender: Any) {
        
           self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnTermsOfUseClicked(_ sender: Any) {
        
//         UIApplication.shared.open( URL(string:  "https://docs.google.com/document/d/1CbRI8vGWGBeWO8IV_ud9_K8KM8TSMdZFhYcocLQ0QJw/edit?ts=59fc9b68")!, options: [:])
        
        let controller = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGTermsOfUseVC") as! CIGTermsOfUseVC
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    @IBAction func btnPrivacyPolicyClicked(_ sender: Any) {
        
//         UIApplication.shared.open( URL(string:  "https://docs.google.com/document/d/1L-riDZ4aW0Kc0Ta6LOhr-poMStbhhRb_YmP8fJmCOQ0/edit?ts=59fc9c73")!, options: [:])
        
        let controller = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGPrivacyPolicyVC") as! CIGPrivacyPolicyVC
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
