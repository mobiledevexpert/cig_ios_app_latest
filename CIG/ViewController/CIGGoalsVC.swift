//
//  CIGGoalsVC.swift
//  CIG
//
//  Created by Harry on 10/24/17.
//  Copyright © 2017 Harry. All rights reserved.
//

import UIKit
import SDWebImage
import SafariServices

class CIGGoalsVC: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,SFSafariViewControllerDelegate{

    @IBOutlet var btnCallNow: UIButton!
    @IBOutlet var btnHelpDeak: UIButton!
    @IBOutlet var imgLogo: UIImageView!
    @IBOutlet var btnContinue: UIButton!
    
    @IBOutlet var msgView: UIView!
    @IBOutlet var adsCollectionView: UICollectionView!
    
    var arrSelectedIndexpath =  NSMutableArray()
    
    var arrCarrier = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        msgView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        msgView.layer.shadowOffset = CGSize(width:0.0,height: 0.6)
        msgView.layer.shadowOpacity = 0.8
        msgView.layer.shadowRadius = 0.0
        msgView.layer.masksToBounds = false
        
        // Do any additional setup after loading the view.
        
//        let layout = adsCollectionView?.collectionViewLayout as! UICollectionViewFlowLayout
//        layout.itemSize = UICollectionViewFlowLayoutAutomaticSize
//        layout.estimatedItemSize = CGSize(width: self.view.frame.size.width/2, height: 70)
//
//        adsCollectionView = UICollectionView(frame: self.view.frame, collectionViewLayout: layout)
        
        adsCollectionView.delegate = self
        adsCollectionView.dataSource = self
        
        self.imgLogo.layoutIfNeeded()
        self.btnContinue.layoutIfNeeded()
        self.btnCallNow.layoutIfNeeded()
        self.btnHelpDeak.layoutIfNeeded()
        
        
        let redGradientLayerForHeader = CAGradientLayer()
        redGradientLayerForHeader.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.imgLogo.frame.size.height)
        redGradientLayerForHeader.colors = [UIColor(red: 162.0/255.0, green: 14.0/255.0, blue: 6.0/255.0, alpha: 0.1).cgColor,
                                            UIColor(red: 241.0/255.0, green: 73.0/255.0, blue: 64.0/255.0, alpha: 0.4).cgColor,
                                            UIColor(red: 162.0/255.0, green: 14.0/255.0, blue: 6.0/255.0, alpha: 0.5).cgColor]
        redGradientLayerForHeader.startPoint = CGPoint(x: -0.2, y: 0.2)
        //        redGradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        imgLogo.layer.insertSublayer(redGradientLayerForHeader, at: 0)
        
        let redGradientLayerForButton = CAGradientLayer()
        redGradientLayerForButton.frame = CGRect(x: 0, y: 0, width: self.btnContinue.frame.size.width, height: self.btnContinue.frame.size.height)
        redGradientLayerForButton.colors = [UIColor(red: 162.0/255.0, green: 14.0/255.0, blue: 6.0/255.0, alpha: 0.1).cgColor,
                                            UIColor(red: 241.0/255.0, green: 73.0/255.0, blue: 64.0/255.0, alpha: 0.4).cgColor,
                                            UIColor(red: 162.0/255.0, green: 14.0/255.0, blue: 6.0/255.0, alpha: 0.5).cgColor]
        redGradientLayerForButton.startPoint = CGPoint(x: -0.2, y: 0.2)
        //        redGradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
   
        btnContinue.layer.insertSublayer(redGradientLayerForButton, at: 0)
        
        let greenGradientLayerForButton = CAGradientLayer()
        greenGradientLayerForButton.frame = CGRect(x: 0, y: 0, width: self.btnHelpDeak.frame.size.width, height: self.btnHelpDeak.frame.size.height)
        greenGradientLayerForButton.colors = [UIColor(red: 76.0/255.0, green: 164.0/255.0, blue: 88.0/255.0, alpha: 1.0).cgColor,
                                              UIColor(red: 92.0/255.0, green: 203.0/255.0, blue: 108.0/255.0, alpha: 1.0).cgColor,
                                              UIColor(red: 17.0/255.0, green: 126.0/255.0, blue: 32.0/255.0, alpha: 1.0).cgColor]
        greenGradientLayerForButton.startPoint = CGPoint(x: -0.2, y: 0.2)
        //        redGradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
//        self.btnHelpDeak.layer.addSublayer(greenGradientLayerForButton)
          btnHelpDeak.layer.insertSublayer(greenGradientLayerForButton, at: 0)
        
        
        let blueGradientLayerForButton = CAGradientLayer()
        blueGradientLayerForButton.frame = CGRect(x: 0, y: 0, width: self.btnCallNow.frame.size.width, height: self.btnCallNow.frame.size.height)
        blueGradientLayerForButton.colors = [UIColor(red: 15.0/255.0, green: 77.0/255.0, blue: 199.0/255.0, alpha: 1.0).cgColor,
                                             UIColor(red: 42.0/255.0, green: 101.0/255.0, blue: 219.0/255.0, alpha: 1.0).cgColor,
                                             UIColor(red: 1.0/255.0, green: 51.0/255.0, blue: 148.0/255.0, alpha: 1.0).cgColor]
        blueGradientLayerForButton.startPoint = CGPoint(x: -0.2, y: 0.2)
        //        redGradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
//        self.btnCallNow.layer.addSublayer(blueGradientLayerForButton)
           btnCallNow.layer.insertSublayer(blueGradientLayerForButton, at: 0)
        
        self.get_carrierApiCall(param: NSDictionary() )
    }

    
    // MARK: - collectionView
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrCarrier.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CIGCollectionCell", for: indexPath) as! CIGCollectionCell
        
        let dic = self.arrCarrier[indexPath.item] as! NSDictionary
        
        cell.blurView.isHidden = true
        cell.imgCheckMark.isHidden = true
        
        if(arrSelectedIndexpath.contains(indexPath.item)){
            
            cell.blurView.isHidden = false
            cell.imgCheckMark.isHidden = false
        }
        
        cell.ImgAds.setIndicatorStyle(UIActivityIndicatorViewStyle.gray)
        cell.ImgAds.setShowActivityIndicator(true)
        
        cell.ImgAds.sd_setImage(with: URL(string: dic.value(forKey : "carrier_img") as! String))
    
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
        if(arrSelectedIndexpath.contains(indexPath.item)){
            
            arrSelectedIndexpath.remove(indexPath.item)

        }
        else {
            
            arrSelectedIndexpath.add(indexPath.item)
            
        }
        self.adsCollectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
            let width : CGFloat = self.view.frame.size.width/2.0
        
        if(UIDevice.current.userInterfaceIdiom == .pad) {
            return CGSize(width: width - 20 , height: 140)
        }
        else {
            return CGSize(width: width - 15 , height: 70)
        }
          
        
    }
    //MARK:-  API Calling
    
    func get_carrierApiCall(param : NSDictionary) {
        
        APIClient.showProgress()
        APIClient.sharedInstance.MakeAPICallWithEndURl(BASE_URL.appending(getCarrier) as NSString, parameters: param as NSDictionary!) { (response, error) in
            
            APIClient.dismissProgress()
            
            if (error == nil) {
                // code of responce...
                let resutls = response! as NSDictionary
                print(resutls)
                let status : Int = resutls.value(forKey: "status") as! Int
                if(status == 1) {
                    
                    // UserDefaults.standard.set(true, forKey: "isLogin")
                    
                    self.arrCarrier = NSMutableArray.init(array: resutls.value(forKey: "carrier") as! NSArray)
                    
                    for dic in self.arrCarrier{
                        
                        if (dic as! NSDictionary).value(forKey: "carrier_name") as! String == "Compliance"{
                            
                            self.arrCarrier.remove(dic)
                        }
                    }
                    
                    self.adsCollectionView.reloadData()
                    
                }else{
                    let error = resutls.value(forKey: "msg") as! String
                    
                    AppUtilites.showError(title: "Error",message: error, cancelButtonTitle:"OK")
                }
            }else{
                AppUtilites.showError(title: "Oops...",message: "Server timed out!", cancelButtonTitle:"OK")
                
            }
        }
    }
    
      // MARK: - Button Click
    
    @IBAction func btn_continues(_ sender: UIButton) {
        

        var carrierIDs = String()
        var arrSelectedCarrier = NSMutableArray()
        
        for var index in self.arrSelectedIndexpath{
            
            let dic = self.arrCarrier[index as! Int] as! NSDictionary
            arrSelectedCarrier.add(dic)
            
            carrierIDs = "\(carrierIDs),\(dic.value(forKey : "carrier_id")!)"
            
        }
        if carrierIDs.characters.count > 0 {
            let indexStr = carrierIDs.index(carrierIDs.startIndex, offsetBy: 1)
            
            carrierIDs = carrierIDs.substring(from: indexStr)
            
            let user = AppUtilites.sharedInstance.getUserData()
            let params = [
                "carrier_id": carrierIDs,
                "user_id": user.user_id
                ] as [String : Any]
            
            APIClient.showProgress()
            APIClient.sharedInstance.MakeAPICallWithEndURl(BASE_URL.appending(updateCarrier) as NSString, parameters: params as NSDictionary!) { (response, error) in
                
                APIClient.dismissProgress()
                
                if (error == nil) {
                    // code of responce...
                    let resutls = response! as NSDictionary
                    print(resutls)
                    let status : Int = resutls.value(forKey: "status") as! Int
                    if(status == 1) {
                        
                        // UserDefaults.standard.set(true, forKey: "isLogin")
                        let goalVC : CIGGoalsTwoVC = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGGoalsTwoVC") as! CIGGoalsTwoVC
                        goalVC.arrCarrier = arrSelectedCarrier.mutableCopy() as! NSMutableArray
                        
                        self.navigationController?.pushViewController(goalVC, animated: true)
                        
                        
                    }else{
                        let error = resutls.value(forKey: "msg") as! String
                        AppUtilites.showError(title: "Error",message: error, cancelButtonTitle:"OK")
                    }
                }else{
                    AppUtilites.showError(title: "Oops...",message: "Server timed out!", cancelButtonTitle:"OK")
                    
                }
            }
        }
        else {
            
              AppUtilites.showError(title: "Error",message: "Please Select Carrier.", cancelButtonTitle:"OK")
        }
        
    }
    
    @IBAction func btn_back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnHelpDeskClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCallNow(_ sender: Any) {
        
        if let url = URL(string: "tel://1-800-220-8899") {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func btnHelpDesk(_ sender: Any) {
        
        let link =  "https://cig.on.spiceworks.com/portal/tickets"
        
        if(link.characters.count > 0){
            
            let safariVC = SFSafariViewController(url: NSURL(string: link)! as URL)
            self.present(safariVC, animated: true, completion: nil)
            safariVC.delegate = self
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
