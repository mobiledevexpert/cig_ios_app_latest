//
//  CIGStratVC.swift
//  CIG
//
//  Created by Harry on 10/28/17.
//  Copyright © 2017 Harry. All rights reserved.
//

import UIKit

class CIGStratVC: UIViewController {

    @IBOutlet var btnCallNow: UIButton!
    @IBOutlet var btnHelpDesk: UIButton!
    @IBOutlet var btnStrat: UIButton!
    @IBOutlet var imgLogo: UIImageView!
    @IBOutlet var msgView: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnStrat.layoutIfNeeded()
        btnHelpDesk.layoutIfNeeded()
        btnCallNow.layoutIfNeeded()
        imgLogo.layoutIfNeeded()

        
        let redGradientLayerForHeader = CAGradientLayer()
        redGradientLayerForHeader.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.imgLogo.frame.size.height)
        redGradientLayerForHeader.colors = [UIColor(red: 162.0/255.0, green: 14.0/255.0, blue: 6.0/255.0, alpha: 0.1).cgColor,
                                            UIColor(red: 241.0/255.0, green: 73.0/255.0, blue: 64.0/255.0, alpha: 0.4).cgColor,
                                            UIColor(red: 162.0/255.0, green: 14.0/255.0, blue: 6.0/255.0, alpha: 0.5).cgColor]
        redGradientLayerForHeader.startPoint = CGPoint(x: -0.2, y: 0.2)
        //        redGradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
//        self.imgLogo.layer.addSublayer(redGradientLayerForHeader)
        self.imgLogo.layer.insertSublayer(redGradientLayerForHeader, at: 0)
        
        
        let redGradientLayerForButton = CAGradientLayer()
        redGradientLayerForButton.frame = CGRect(x: 0, y: 0, width: self.btnStrat.frame.size.width, height: self.btnStrat.frame.size.height)
        redGradientLayerForButton.colors = [UIColor(red: 162.0/255.0, green: 14.0/255.0, blue: 6.0/255.0, alpha: 0.1).cgColor,
                                            UIColor(red: 241.0/255.0, green: 73.0/255.0, blue: 64.0/255.0, alpha: 0.4).cgColor,
                                            UIColor(red: 162.0/255.0, green: 14.0/255.0, blue: 6.0/255.0, alpha: 0.5).cgColor]
        redGradientLayerForButton.startPoint = CGPoint(x: -0.2, y: 0.2)
        //        redGradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
//        self.btnStrat.layer.addSublayer(redGradientLayerForButton)
         self.btnStrat.layer.insertSublayer(redGradientLayerForButton, at: 0)
        
        let greenGradientLayerForButton = CAGradientLayer()
        greenGradientLayerForButton.frame = CGRect(x: 0, y: 0, width: self.btnHelpDesk.frame.size.width, height: self.btnHelpDesk.frame.size.height)
        greenGradientLayerForButton.colors = [UIColor(red: 76.0/255.0, green: 164.0/255.0, blue: 88.0/255.0, alpha: 1.0).cgColor,
                                              UIColor(red: 92.0/255.0, green: 203.0/255.0, blue: 108.0/255.0, alpha: 1.0).cgColor,
                                              UIColor(red: 17.0/255.0, green: 126.0/255.0, blue: 32.0/255.0, alpha: 1.0).cgColor]
        greenGradientLayerForButton.startPoint = CGPoint(x: -0.2, y: 0.2)
        //        redGradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
//        self.btnHelpDesk.layer.addSublayer(greenGradientLayerForButton)
          self.btnHelpDesk.layer.insertSublayer(greenGradientLayerForButton, at: 0)
        
        let blueGradientLayerForButton = CAGradientLayer()
        blueGradientLayerForButton.frame = CGRect(x: 0, y: 0, width: self.btnCallNow.frame.size.width, height: self.btnCallNow.frame.size.height)
        blueGradientLayerForButton.colors = [UIColor(red: 15.0/255.0, green: 77.0/255.0, blue: 199.0/255.0, alpha: 1.0).cgColor,
                                             UIColor(red: 42.0/255.0, green: 101.0/255.0, blue: 219.0/255.0, alpha: 1.0).cgColor,
                                             UIColor(red: 1.0/255.0, green: 51.0/255.0, blue: 148.0/255.0, alpha: 1.0).cgColor]
        blueGradientLayerForButton.startPoint = CGPoint(x: -0.2, y: 0.2)
        //        redGradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
//        self.btnCallNow.layer.addSublayer(blueGradientLayerForButton)
          self.btnCallNow.layer.insertSublayer(blueGradientLayerForButton, at: 0)
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnStartClick(_ sender: UIButton) {
       
        UserDefaults.standard.set(true, forKey: "isLogin")
        
        let homeVC  = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGHomeVC") as! CIGHomeVC
        
        let leftSideMenuVC  = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGLeftSideMenuVC") as? CIGLeftSideMenuVC
        
        let rightSideMenuVC  = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGRightSideMenuVC") as? CIGRightSideMenuVC
        
        let slideMenuController = SlideMenuController(mainViewController: homeVC, leftMenuViewController: leftSideMenuVC!, rightMenuViewController: rightSideMenuVC!)
        let nav = UINavigationController(rootViewController: slideMenuController)
        nav.navigationBar.isHidden = true
        let appDel : AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDel.window?.rootViewController = nav
        appDel.window?.makeKeyAndVisible()
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
