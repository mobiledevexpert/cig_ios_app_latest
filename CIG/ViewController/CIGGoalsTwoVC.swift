//
//  CIGGoalsTwoVC.swift
//  CIG
//
//  Created by Harry on 10/24/17.
//  Copyright © 2017 Harry. All rights reserved.
//

import UIKit
import SafariServices

class CIGGoalsTwoVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,SFSafariViewControllerDelegate {
    
    

    @IBOutlet var goalAmoutView: UIView!
    @IBOutlet var btnSubmit: UIButton!
    @IBOutlet var txtYearlyGoal: UITextField!
    @IBOutlet var txtCommision: UITextField!
    @IBOutlet var txtPlanName: UITextField!
    @IBOutlet var addGoalView: UIView!
    @IBOutlet var txtGoalAmount: UITextField!
    @IBOutlet var btnCallNow: UIButton!
    @IBOutlet var btnHelpDeak: UIButton!
    @IBOutlet var imgLogo: UIImageView!
    @IBOutlet var btnFinish: UIButton!
    
    @IBOutlet var blurView: UIView!
    @IBOutlet var tableview: UITableView!
    @IBOutlet var msgView: UILabel!
    
    var arrCarrier = NSMutableArray()
    var arrGoals = NSMutableArray()
    var selecteddict = NSDictionary()
    var IsEdit = Bool()
    
//    var txtExpectedGoalAmount = UITextField()
    @IBOutlet var txtExpectedGoalAmount: UILabel!
    var total = 0
    var amount = "0"
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        self.tableview.estimatedRowHeight = 190
//        self.tableview.rowHeight = UITableViewAutomaticDimension
//        self.tableview.tableHeaderView = self.goalAmoutView
        
        IsEdit = false
        
        self.imgLogo.layoutIfNeeded()
        self.btnCallNow.layoutIfNeeded()
        self.btnHelpDeak.layoutIfNeeded()
        
        msgView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        msgView.layer.shadowOffset = CGSize(width:0.0,height: 0.6)
        msgView.layer.shadowOpacity = 0.8
        msgView.layer.shadowRadius = 0.0
        msgView.layer.masksToBounds = false
        
        self.txtExpectedGoalAmount.text = "Your goal commission amount is $00 for this year."

        txtCommision.delegate = self
        txtYearlyGoal.delegate = self
        
        
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = blurView.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        blurView.addSubview(blurEffectView)
        
        let redGradientLayerForHeader = CAGradientLayer()
        redGradientLayerForHeader.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.imgLogo.frame.size.height)
        redGradientLayerForHeader.colors = [UIColor(red: 162.0/255.0, green: 14.0/255.0, blue: 6.0/255.0, alpha: 0.1).cgColor,
                                            UIColor(red: 241.0/255.0, green: 73.0/255.0, blue: 64.0/255.0, alpha: 0.4).cgColor,
                                            UIColor(red: 162.0/255.0, green: 14.0/255.0, blue: 6.0/255.0, alpha: 0.5).cgColor]
        redGradientLayerForHeader.startPoint = CGPoint(x: -0.2, y: 0.2)
        //        redGradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
//        self.imgLogo.layer.addSublayer(redGradientLayerForHeader)
          imgLogo.layer.insertSublayer(redGradientLayerForHeader, at: 0)
        
//        let redGradientLayerForButton = CAGradientLayer()
//        redGradientLayerForButton.frame = CGRect(x: 0, y: 0, width: self.btnFinish.frame.size.width, height: self.btnFinish.frame.size.height)
//        redGradientLayerForButton.colors = [UIColor(red: 162.0/255.0, green: 14.0/255.0, blue: 6.0/255.0, alpha: 0.1).cgColor,
//                                            UIColor(red: 241.0/255.0, green: 73.0/255.0, blue: 64.0/255.0, alpha: 0.4).cgColor,
//                                            UIColor(red: 162.0/255.0, green: 14.0/255.0, blue: 6.0/255.0, alpha: 0.5).cgColor]
//        redGradientLayerForButton.startPoint = CGPoint(x: -0.2, y: 0.2)
//        //        redGradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
//        self.btnFinish.layer.addSublayer(redGradientLayerForButton)
        
        let greenGradientLayerForButton = CAGradientLayer()
        greenGradientLayerForButton.frame = CGRect(x: 0, y: 0, width: self.btnHelpDeak.frame.size.width, height: self.btnHelpDeak.frame.size.height)
        greenGradientLayerForButton.colors = [UIColor(red: 76.0/255.0, green: 164.0/255.0, blue: 88.0/255.0, alpha: 1.0).cgColor,
                                              UIColor(red: 92.0/255.0, green: 203.0/255.0, blue: 108.0/255.0, alpha: 1.0).cgColor,
                                              UIColor(red: 17.0/255.0, green: 126.0/255.0, blue: 32.0/255.0, alpha: 1.0).cgColor]
        greenGradientLayerForButton.startPoint = CGPoint(x: -0.2, y: 0.2)
        //        redGradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
//        self.btnHelpDeak.layer.addSublayer(greenGradientLayerForButton)
         btnHelpDeak.layer.insertSublayer(greenGradientLayerForButton, at: 0)
        
        let blueGradientLayerForButton = CAGradientLayer()
        blueGradientLayerForButton.frame = CGRect(x: 0, y: 0, width: self.btnCallNow.frame.size.width, height: self.btnCallNow.frame.size.height)
        blueGradientLayerForButton.colors = [UIColor(red: 15.0/255.0, green: 77.0/255.0, blue: 199.0/255.0, alpha: 1.0).cgColor,
                                             UIColor(red: 42.0/255.0, green: 101.0/255.0, blue: 219.0/255.0, alpha: 1.0).cgColor,
                                             UIColor(red: 1.0/255.0, green: 51.0/255.0, blue: 148.0/255.0, alpha: 1.0).cgColor]
        blueGradientLayerForButton.startPoint = CGPoint(x: -0.2, y: 0.2)
        //        redGradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
//        self.btnCallNow.layer.addSublayer(blueGradientLayerForButton)
           btnCallNow.layer.insertSublayer(blueGradientLayerForButton, at: 0)
        // Do any additional setup after loading the view.
        
        self.tableview.delegate = self
        self.tableview.dataSource = self
        
        self.getGoalDetails()
    }

    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        let allowedCharacters = CharacterSet.decimalDigits
//         let DecimalPointSet = CharacterSet(charactersIn: ".")
//        let mainCharSet = allowedCharacters.union(DecimalPointSet)
        let characterSet = CharacterSet(charactersIn: string)
        return allowedCharacters.isSuperset(of: characterSet)
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if(self.amount == "0"){
            
            self.txtExpectedGoalAmount.text = ""
        }
        
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        self.amount = self.txtExpectedGoalAmount.text!
    }
    // MARK: - tableView Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return self.arrCarrier.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if arrGoals.count > 0{
            
            let  dic = self.arrCarrier[section] as! NSDictionary
            
            for goalDic in self.arrGoals{
                
                if (dic.value(forKey: "carrier_id")  as! String) == ((goalDic as! NSDictionary).value(forKey: "carrier_id")  as! String){
                    
                    return (((goalDic as! NSDictionary).value(forKey: "goal_detail")  as! NSArray).count + 3)
                }
            }
        }
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let  dic = self.arrCarrier[indexPath.section] as! NSDictionary
        print(dic)

        var goalDic = NSDictionary()
        var goalArr = NSArray()
        
        var NumberOfRows = 0
        
        print("cell\(indexPath.row)")
        
        for Dic in self.arrGoals{
            
            if (dic.value(forKey: "carrier_id")  as! String) == ((Dic as! NSDictionary).value(forKey: "carrier_id")  as! String){
                
                goalDic = Dic as! NSDictionary
                NumberOfRows = (((Dic as! NSDictionary).value(forKey: "goal_detail")  as! NSArray).count + 3)
                goalArr = ((Dic as! NSDictionary).value(forKey: "goal_detail") as? NSArray)!
            }
        }
    
        if (indexPath.row == 0){
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "CIGGoalProductTC", for: indexPath) as! CIGGoalProductTC
            let dic = arrCarrier[indexPath.section] as! NSDictionary
            cell.lblProductName.text = (dic.value(forKey: "carrier_name") as? String)?.uppercased()
            cell.lblProductName.font = UIFont.boldSystemFont(ofSize: 15.0)
            cell.lblPrice1.isHidden = true
            cell.lblPrice2.isHidden = true

            return cell

        }
        
        if NumberOfRows != 0 && indexPath.row == (NumberOfRows - 2){
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "CIGGoalCommissionTC", for: indexPath) as! CIGGoalCommissionTC
            let amount =  (goalDic.value(forKey: "carrier_total") as! NSNumber).stringValue
            cell.lblCommisionTotal.text = "$\(amount)"
            return cell
            
        }
        
        if (indexPath.row == 1 && NumberOfRows == 0) || (indexPath.row == NumberOfRows - 1){
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "CIGAddGoalTC", for: indexPath) as! CIGAddGoalTC
            cell.btnAdd.tag = indexPath.section
            return cell
        }
    
        let cell = tableView.dequeueReusableCell(withIdentifier: "CIGGoalProductTC", for: indexPath) as! CIGGoalProductTC
        cell.lblProductName.text = (goalArr[indexPath.row - 1] as! NSDictionary).value(forKey: "product_name") as? String
        cell.lblProductName.font = UIFont.systemFont(ofSize: 15.0)
        cell.lblPrice1.isHidden = false
        cell.lblPrice2.isHidden = false
        
        let amount = (goalArr[indexPath.row - 1] as! NSDictionary).value(forKey: "price1") as? String
        cell.lblPrice1.text = "$\(amount!)"
        cell.lblPrice2.text = (goalArr[indexPath.row - 1] as! NSDictionary).value(forKey: "price2") as? String
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        let  dic = self.arrCarrier[indexPath.section] as! NSDictionary
        print(dic)
        
        var NumberOfRows = 0
        
        print("cell\(indexPath.row)")
        
        for Dic in self.arrGoals{
            
            if (dic.value(forKey: "carrier_id")  as! String) == ((Dic as! NSDictionary).value(forKey: "carrier_id")  as! String){
                
                NumberOfRows = (((Dic as! NSDictionary).value(forKey: "goal_detail")  as! NSArray).count + 3)
            }
        }
        
        if (indexPath.row == 0){
            return false
        }
        
        if NumberOfRows != 0 && indexPath.row == (NumberOfRows - 2){
            return false
        }
        
        if (indexPath.row == 1 && NumberOfRows == 0) || (indexPath.row == NumberOfRows - 1){
            return false
        }
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let  dic = self.arrCarrier[indexPath.section] as! NSDictionary
            print(dic)
            
            var goalDic = NSDictionary()
            var goalArr = NSArray()
            
            var NumberOfRows = 0
            
            print("cell\(indexPath.row)")
            
            for Dic in self.arrGoals{
                
                if (dic.value(forKey: "carrier_id")  as! String) == ((Dic as! NSDictionary).value(forKey: "carrier_id")  as! String){
                    
                    goalDic = Dic as! NSDictionary
                    NumberOfRows = (((Dic as! NSDictionary).value(forKey: "goal_detail")  as! NSArray).count + 3)
                    goalArr = ((Dic as! NSDictionary).value(forKey: "goal_detail") as? NSArray)!
                }
            }
            
            var selectedDictionary = goalArr[indexPath.row - 1] as! NSDictionary
            
            let params = [
                "goal_id": selectedDictionary.value(forKey:"goal_id") as! String
                ] as [String : Any]
            
            APIClient.showProgress()
            APIClient.sharedInstance.MakeAPICallWithEndURl(BASE_URL.appending(k_delete_goal) as NSString, parameters: params as NSDictionary!) { (response, error) in
                
                APIClient.dismissProgress()
                
                if (error == nil) {
                    // code of responce...
                    let resutls = response! as NSDictionary
                    print(resutls)
                    let status : Int = resutls.value(forKey: "status") as! Int
                    if(status == 1) {
                        
                        //                    self.tableview.beginUpdates()
                        //                    (self.goalDic.value(forKey: "goal_detail") as! NSArray).removeObject(at: (sender as! UIButton).tag - 1])
                        //                    self.arrGolaData.removeObject(at: (sender as! UIButton).tag)
                        //                    self.tableview.deleteRows(at: [IndexPath(row: (sender as! UIButton).tag - 1, section: 1)], with: UITableViewRowAnimation.fade)
                        //                    self.tableview.endUpdates()
                        //                    self.tableview.reloadData()
                        
                        self.getGoalDetails()
                        
                        
                    }else{
                        
                        let error = resutls.value(forKey: "msg") as! String
                        AppUtilites.showError(title: "Error",message: error, cancelButtonTitle:"OK")
                    }
                }else{
                    AppUtilites.showError(title: "Oops...",message: "Server timed out!", cancelButtonTitle:"OK")
                    
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let  dic = self.arrCarrier[indexPath.section] as! NSDictionary
        print(dic)
        
        var goalDic = NSDictionary()
        var goalArr = NSArray()
        
        var NumberOfRows = 0
        
        print("cell\(indexPath.row)")
        
        for Dic in self.arrGoals{
            
            if (dic.value(forKey: "carrier_id")  as! String) == ((Dic as! NSDictionary).value(forKey: "carrier_id")  as! String){
                
                goalDic = Dic as! NSDictionary
                NumberOfRows = (((Dic as! NSDictionary).value(forKey: "goal_detail")  as! NSArray).count + 3)
                goalArr = ((Dic as! NSDictionary).value(forKey: "goal_detail") as? NSArray)!
            }
        }
        
        if (indexPath.row == 0){
            return;
        }
        
        if NumberOfRows != 0 && indexPath.row == (NumberOfRows - 2){
            return;
        }
        
        if (indexPath.row == 1 && NumberOfRows == 0) || (indexPath.row == NumberOfRows - 1){
            return
        }
        IsEdit = true
        selecteddict = goalArr[indexPath.row - 1] as! NSDictionary
        print(selecteddict)
        self.blurView.isHidden = false
        self.addGoalView.isHidden = false
        self.btnSubmit.tag = indexPath.section
        self.btnSubmit.setTitle("EDIT", for: UIControlState.normal)
        self.txtPlanName.text = selecteddict.value(forKey: "product_name") as? String
        self.txtCommision.text = "\(selecteddict.value(forKey: "price1")!)"
        self.txtYearlyGoal.text = "\(selecteddict.value(forKey: "price2")!)"
    }
    
    @IBAction func onAddGoalClicked(_ sender: Any) {
       
//        if(self.txtExpectedGoalAmount.text!.characters.count > 0){
        
            IsEdit = false
            self.blurView.isHidden = false
            self.addGoalView.isHidden = false
            self.btnSubmit.setTitle("SUBMIT", for: UIControlState.normal)
            self.btnSubmit.tag = (sender as! UIButton).tag
//        }
//        else{
//            AppUtilites.showError(title: "Error", message: "Please add goal amount.", cancelButtonTitle: "Ok")
//        }
    }
    @objc func btnAddClicked(sender: UIButton!) {
        
        self.blurView.isHidden = false
        self.addGoalView.isHidden = false
        self.btnSubmit.tag = sender.tag
        
    }
    
    @IBAction func CloseButtonClicked(_ sender: Any) {
        
        self.blurView.isHidden = true
        self.addGoalView.isHidden = true
        
        self.txtPlanName.text = ""
        self.txtCommision.text = ""
        self.txtYearlyGoal.text = ""
        
    }
    
   
    func getGoalDetails(){
        
        let user = AppUtilites.sharedInstance.getUserData()
        var amount = self.txtExpectedGoalAmount.text
        
        let params = [
            "user_id": user.user_id
            ] as [String : Any]
        
        APIClient.showProgress()
        APIClient.sharedInstance.MakeAPICallWithEndURl(BASE_URL.appending(getGoalDetail) as NSString, parameters: params as NSDictionary!) { (response, error) in
            
            APIClient.dismissProgress()
            
            if (error == nil) {
                // code of responce...
                let resutls = response! as NSDictionary
                print(resutls)
                let status : Int = resutls.value(forKey: "status") as! Int
                if(status == 1) {
                    
                    let arrGoalDetail =  resutls.value(forKey: "goal_all_detail") as! NSArray
                    self.arrGoals = NSMutableArray(array: arrGoalDetail)
                    
                    self.total = 0
                    for dic in self.arrGoals{
                        
                        var tempTotal = (dic as! NSDictionary).value(forKey: "carrier_total") as? Int
                        
                        if(tempTotal != nil)
                        {
                                 self.total += tempTotal!
                        }
                        
                       
                    }
                    
                    print(self.txtExpectedGoalAmount.text)
                    
                    self.txtExpectedGoalAmount.layoutIfNeeded()
                    if(self.txtExpectedGoalAmount.text?.characters.count == 0){
                        
                        self.txtExpectedGoalAmount.text = "0"
                    }
                    if(self.total > 0){

                        DispatchQueue.main.async {

                            self.txtExpectedGoalAmount.text = "Your goal commission amount is $\(self.total) for this year."
                            self.txtExpectedGoalAmount.setNeedsDisplay()
                            self.amount = "\(self.total)"
                        }
                    }
                    
                    self.tableview.reloadData()
                    
                }else{
                    let error = resutls.value(forKey: "msg") as! String
                    
                    AppUtilites.showError(title: "Error",message: error, cancelButtonTitle:"OK")
                }
            }else{
                AppUtilites.showError(title: "Oops...",message: "Server timed out!", cancelButtonTitle:"OK")
                
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
//        if(section == 0){
//            
//            return 115.0
//        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        if(section == self.arrCarrier.count - 1){
            
            return 50
        }
        return 0
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    

        let customView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 114))
        customView.backgroundColor = UIColor.white
        
       let imageView = UIImageView(frame: CGRect(x: 0, y: 10, width: 55, height: 55))
        imageView.image = UIImage(named: "$-1")
        
        let txtLable = UILabel(frame: CGRect(x: 50, y: 10, width: self.view.frame.size.width - 75, height: 45))
        txtLable.text = "How much money do you want to make this season ?"
        txtLable.numberOfLines = 2
        
        let imageViewSmall = UIImageView(frame: CGRect(x: 40, y: 60, width: 35, height: 35))
        imageViewSmall.image = UIImage(named: "$-1")
        
//        self.txtExpectedGoalAmount = UITextField(frame: CGRect(x: 75, y: 60, width: self.view.frame.size.width - 75, height: 30))
//        self.txtExpectedGoalAmount.keyboardType = .decimalPad
//        self.txtExpectedGoalAmount.delegate = self
////        self.txtExpectedGoalAmount.text = self.amount
//
//        self.txtExpectedGoalAmount.text = "Your goal commission amount is $\(self.amount) for this year."

        
        let lineView = UIView(frame: CGRect(x: 25, y: 90, width: self.view.frame.size.width - 65, height: 1))
        lineView.backgroundColor = UIColor.lightGray
        
        customView.addSubview(imageView)
        customView.addSubview(txtLable)
        customView.addSubview(imageViewSmall)
        customView.addSubview(txtExpectedGoalAmount)
        customView.addSubview(lineView)

        customView.layoutIfNeeded()
        customView.clipsToBounds = true

        return customView
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        

        let customView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50))
        customView.backgroundColor = UIColor.white
        let button = UIButton(frame: CGRect(x: 0, y: 10, width: customView.frame.size.width, height: 35))
        button.setTitle("Continue", for: .normal)
       
//        if(self.total >= Int(self.amount)! && (self.total != 0)){
//
//            button.alpha = 1.0
//              button.isEnabled = true
//        }
//        else{
//
//            button.alpha = 0.5
//             button.isEnabled = false
//        }
        button.backgroundColor = UIColor(red: 162.0/255.0, green: 14.0/255.0, blue: 6.0/255.0, alpha: 1.0)
        
        button.addTarget(self, action: #selector(self.finishButtonClicked), for: .touchUpInside)
        
        let redGradientLayerForButton = CAGradientLayer()
        redGradientLayerForButton.frame = CGRect(x: 0, y: 0, width: button.frame.size.width, height: button.frame.size.height)
        redGradientLayerForButton.colors = [UIColor(red: 162.0/255.0, green: 14.0/255.0, blue: 6.0/255.0, alpha: 0.1).cgColor,
                                            UIColor(red: 241.0/255.0, green: 73.0/255.0, blue: 64.0/255.0, alpha: 0.4).cgColor,
                                            UIColor(red: 162.0/255.0, green: 14.0/255.0, blue: 6.0/255.0, alpha: 0.5).cgColor]
        redGradientLayerForButton.startPoint = CGPoint(x: -0.2, y: 0.2)

        button.layer.addSublayer(redGradientLayerForButton)
        customView.addSubview(button)
        
        customView.layoutIfNeeded()
        customView.clipsToBounds = true
        return customView
    }
    
    @objc func finishButtonClicked() {
        
        
        let alertController = UIAlertController(title: "Info", message: self.txtExpectedGoalAmount.text, preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default){
            
            (result : UIAlertAction) -> Void in
            
            let profileVC : CIGUploadProfilePicVC = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGUploadProfilePicVC") as! CIGUploadProfilePicVC
            self.navigationController?.pushViewController(profileVC, animated: true)
            
            
        }
        alertController.addAction(okAction)
    
        let CancelAction = UIAlertAction(title: "Keep Editing", style: UIAlertActionStyle.default)
        {
            (result : UIAlertAction) -> Void in
            print("You pressed Cancel")
            
            
        }
        alertController.addAction(CancelAction)
        
        self.present(alertController, animated: true, completion: nil)
        
        
//        if(self.total >= Int(self.amount)! && (self.total != 0)){
//
//            let profileVC : CIGUploadProfilePicVC = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGUploadProfilePicVC") as! CIGUploadProfilePicVC
//            self.navigationController?.pushViewController(profileVC, animated: true)
//        }
//        else{
//
//            AppUtilites.showWarning(message: "Entered goals total amount should match the yearly goal amount")
//        }

    }
    @IBAction func btnCallNow(_ sender: Any) {
        
        if let url = URL(string: "tel://1-800-220-8899") {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func btnHelpDesk(_ sender: Any) {
        
        let link =  "https://cig.on.spiceworks.com/portal/tickets"
        if(link.characters.count > 0){
            
            let safariVC = SFSafariViewController(url: NSURL(string: link)! as URL)
            self.present(safariVC, animated: true, completion: nil)
            safariVC.delegate = self
        }
    }
    
    
    // MARK: - Button click
    
    
    @IBAction func btn_finish(_ sender: UIButton) {
        
        
        
        if(self.total >= Int(self.amount)! && (self.total != 0)){
            
            let profileVC : CIGUploadProfilePicVC = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGUploadProfilePicVC") as! CIGUploadProfilePicVC
            self.navigationController?.pushViewController(profileVC, animated: true)
        }
        else{
            
            AppUtilites.showWarning(message: "Entered goals total amount should match the yearly goal amount")
        }

    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnSubmitClicked(_ sender: Any) {
        
        
        
        if(self.txtPlanName.text!.characters.count == 0){
            
            AppUtilites.showError(title: "Error",message: "Enter plan name", cancelButtonTitle:"OK")
        }
        else if(self.txtCommision.text!.characters.count == 0){
            
            AppUtilites.showError(title: "Error",message: "Enter commission", cancelButtonTitle:"OK")
            
        }else if(self.txtYearlyGoal.text!.characters.count == 0){
            
            AppUtilites.showError(title: "Error",message: "Enter yearly goal", cancelButtonTitle:"OK")
        }
        else{
            
            
            let alertController = UIAlertController(title: "Info", message: "This change will affect your yearly Commission Goal. Would you like to Continue?", preferredStyle: UIAlertControllerStyle.alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
            {
                (result : UIAlertAction) -> Void in
                print("You pressed OK")
                var param : [String : Any]!
                
                print((sender as! UIButton).tag)
                
                let dic = self.arrCarrier[(sender as! UIButton).tag] as! NSDictionary
                let user = AppUtilites.sharedInstance.getUserData()
                var apiURL = ""
                if(self.IsEdit == true){
                    apiURL = k_edit_goal
                    param = ["carrier_id": dic.value(forKey: "carrier_id") as! String,
                             "user_id": user.user_id,
                             "product_name":self.txtPlanName.text as! String,
                             "price1":self.txtCommision.text as! String,
                             "price2":self.txtYearlyGoal.text as! String,
                             "goal_id":self.selecteddict.value(forKey: "goal_id") as! String
                        ] as [String : Any]
                }else{
                    apiURL = k_update_user_goal
                    param = ["carrier_id":dic.value(forKey: "carrier_id")  as! String,
                             "user_id": user.user_id,
                             "product_name":self.txtPlanName.text as Any,
                             "price1":self.txtCommision.text as Any,
                             "price2":self.txtYearlyGoal.text as Any
                        ] as [String : Any]
                }
                
                
                
                APIClient.showProgress()
                APIClient.sharedInstance.MakeAPICallWithEndURl(BASE_URL.appending(apiURL) as NSString, parameters: param as NSDictionary!) { (response, error) in
                    
                    APIClient.dismissProgress()
                    
                    if (error == nil) {
                        // code of responce...
                        let resutls = response! as NSDictionary
                        print(resutls)
                        let status : Int = resutls.value(forKey: "status") as! Int
                        if(status == 1) {
                            
                            self.txtPlanName.text = ""
                            self.txtCommision.text = ""
                            self.txtYearlyGoal.text = ""
                            
                            self.blurView.isHidden = true
                            self.addGoalView.isHidden = true
                            self.getGoalDetails()
                            
                        }else{
                            let error = resutls.value(forKey: "msg") as! String
                            
                            AppUtilites.showError(title: "Error",message: error, cancelButtonTitle:"OK")
                        }
                    }else{
                        AppUtilites.showError(title: "Oops...",message: "Server timed out!", cancelButtonTitle:"OK")
                        
                    }
                }
            }
            alertController.addAction(okAction)
            
            let CancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default)
            {
                (result : UIAlertAction) -> Void in
                print("You pressed Cancel")
                
                
            }
            alertController.addAction(CancelAction)
            
            self.present(alertController, animated: true, completion: nil)
            
        }
    }

}
