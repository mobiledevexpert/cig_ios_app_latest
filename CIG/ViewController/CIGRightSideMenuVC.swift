//
//  CIGRightSideMenuVC.swift
//  CIG
//
//  Created by Harry on 10/27/17.
//  Copyright © 2017 Harry. All rights reserved.
//

import UIKit
import SafariServices
class CIGRightSideMenuVC: UIViewController,SFSafariViewControllerDelegate {

    @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        if (UIScreen.main.nativeBounds.height == 2436) {
            self.headerViewHeightConstraint.constant = self.headerViewHeightConstraint.constant + 25
        }

        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onHelpDeskClicked(_ sender: Any) {
        
        let link =  "https://cig.on.spiceworks.com/portal/tickets"
        if(link.characters.count > 0){
            
            let safariVC = SFSafariViewController(url: NSURL(string: link)! as URL)
            self.present(safariVC, animated: true, completion: nil)
            safariVC.delegate = self
        }
    }
    @IBAction func onCloseClicked(_ sender: Any) {
        
        self.closeRight()
    }
    
    @IBAction func myProfileClicked(_ sender: Any) {
        
        slideMenuController()?.closeRight()
        
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGMyProfileVC") as! CIGMyProfileVC
        self.navigationController?.pushViewController(controller, animated: true)

    }
    
    @IBAction func signOutClicked(_ sender: Any) {
        
        UserDefaults.standard.set(false, forKey: "isLogin")
        
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: "user_dis")
        
        let loginVC  = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGLoginVC") as! CIGLoginVC
        let nav = UINavigationController(rootViewController: loginVC)
        nav.navigationBar.isHidden = true
        let appDel : AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDel.window = UIWindow(frame: UIScreen.main.bounds)
        appDel.window?.rootViewController = nav
        appDel.window?.makeKeyAndVisible()
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
