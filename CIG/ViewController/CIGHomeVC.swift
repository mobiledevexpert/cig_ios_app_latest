//
//  CIGHomeVC.swift
//  CIG
//
//  Created by Harry on 10/24/17.
//  Copyright © 2017 Harry. All rights reserved.
//

import UIKit
import SafariServices
import MessageUI

class CIGHomeVC: UIViewController,UITableViewDelegate,UITableViewDataSource,SFSafariViewControllerDelegate,MFMailComposeViewControllerDelegate,updateGoalStatusAfterUpdatingAchievementDelegate {
    

    @IBOutlet var lblGoalPercentage: UILabel!
    @IBOutlet weak var fullProgressView: UIView!
    @IBOutlet weak var progressView: UIView!
    @IBOutlet weak var progressViewCon: NSLayoutConstraint!
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var btnHelpDeak: UIButton!
    @IBOutlet weak var btnCallNow: UIButton!
    @IBOutlet var lblStatus: UILabel!
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!

    
    var gradeientForHeader : CAGradientLayer!
    var gradeientForProgressView : CAGradientLayer!
    var gradeientForHelpDeskButton : CAGradientLayer!
    var gradeientForbtnCallNow : CAGradientLayer!
    
    var titleArray = NSMutableArray()
    var descriptionArray = NSMutableArray()
    var imgArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (UIScreen.main.nativeBounds.height == 2436) {
            self.headerViewHeightConstraint.constant = self.headerViewHeightConstraint.constant + 20
        }
        
        let user = AppUtilites.sharedInstance.getUserData()
        print(user.display_name)
        print(user.avatar_image)
        
        progressView.layoutIfNeeded()
        headerView.layoutIfNeeded()
        btnHelpDeak.layoutIfNeeded()
        btnCallNow.layoutIfNeeded()
        
        
        self.lblUserName.text = user.display_name
        self.imgProfile.setIndicatorStyle(UIActivityIndicatorViewStyle.gray)
        self.imgProfile.setShowActivityIndicator(true)
        self.lblStatus.text = AppUtilites.sharedInstance.getCurrentstatus()
         self.imgProfile.sd_setImage(with: URL(string: user.avatar_image))
        if(user.goal_status == "0" || user.goal_status.characters.count == 0){
            
            self.progressViewCon.constant = 0
            self.lblGoalPercentage.text = "0%"
        }
        else{
            
            
            let goalamount : NSString  = (user.goal_status as NSString).substring(to: 3) as NSString
            let goalamountInt = goalamount.floatValue
            
            if(goalamount == "100"){
                
                self.progressViewCon.constant =  CGFloat(self.view.frame.size.width-150)
                
            }
            else{
                
                self.progressViewCon.constant =  CGFloat((Float(self.fullProgressView.frame.size.width)*goalamountInt)/100)
            }
            
//            self.progressViewCon.constant =  CGFloat((Float(self.fullProgressView.frame.size.width)*goalamountInt)/100)
            self.lblGoalPercentage.text = "\(user.goal_status)%"
        }
        
        self.progressView.setNeedsLayout()
        self.progressView.setNeedsDisplay()
        
        view.setNeedsLayout()
        view.setNeedsDisplay()
        
        self.slideMenuController()?.removeLeftGestures()
        self.slideMenuController()?.removeRightGestures()
        self.slideMenuController()?.addLeftGestures()
        self.slideMenuController()?.addRightGestures()
        let screenSize: CGRect = UIScreen.main.bounds
        self.slideMenuController()?.changeLeftViewWidth(screenSize.width - 20);
        self.slideMenuController()?.changeRightViewWidth(screenSize.width - 20)
        
        
        AppUtilites.sharedInstance.setCornerRadius(view: imgProfile, cornerRadius: imgProfile.frame.width/2, borderColor: UIColor.white, borderWidth: 2)
        
        gradeientForHeader = AppUtilites.sharedInstance.GetRedGradient(ScreenSize.SCREEN_WIDTH, headerView.frame.size.height)
        headerView.layer.insertSublayer(gradeientForHeader, at: 0)
        
        gradeientForProgressView = AppUtilites.sharedInstance.GetBlueGradient(progressViewCon.constant, progressView.frame.size.height)
        progressView.layer.insertSublayer(gradeientForProgressView, at: 0)
        
        gradeientForHelpDeskButton = AppUtilites.sharedInstance.GetGreenGradient(self.view.frame.size.width/2, self.btnHelpDeak.frame.size.height)
        btnHelpDeak.layer.insertSublayer(gradeientForHelpDeskButton, at: 0)
        
       
         gradeientForbtnCallNow = AppUtilites.sharedInstance.GetBlueGradient(self.view.frame.size.width/2, self.btnCallNow.frame.size.height)
        btnCallNow.layer.insertSublayer(gradeientForbtnCallNow, at: 0)
        
        titleArray = ["Online Quoting","Carrier Alerts","Events","Copeland Portal","Copeland Contacts","Leads","My Appointments"]
        descriptionArray = ["Quoting in Real Time","Alerts you need by Carriers you care about","Helping you grow your business","All the tools you need in one place","Need help? Call us now!","Exceeding goals! Request Leads!",""]
        imgArray = ["online-quoting","carrier-alerts","events-training","copeland-portal","copeland-contacts","leads - new","appointments"]
        
      
      
        
 
        let param = ["team_id": user.team_id]
        
        self.get_teamApiCall(param: param as NSDictionary)
    }

    override func viewWillAppear(_ animated: Bool) {
        
        let user = AppUtilites.sharedInstance.getUserData()
        self.imgProfile.sd_setImage(with: URL(string: user.avatar_image))
        
        if (AppUtilites.sharedInstance.isgoalUpdated == true){
            
            let param = ["user_id": user.user_id]
            self.get_UserData(param: param as NSDictionary)
        }
    }
    
    func updateGoalProgressBar() {
        let user = AppUtilites.sharedInstance.getUserData()
        //
        
        let param = ["user_id": user.user_id]
        self.get_UserData(param: param as NSDictionary)

    }
    // MARK: - API call
    
    func get_teamApiCall(param : NSDictionary) {
        
        APIClient.showProgress()
        APIClient.sharedInstance.MakeAPICallWithEndURl(BASE_URL.appending(k_get_team_contact_detail) as NSString, parameters: param as NSDictionary!) { (response, error) in
            
            APIClient.dismissProgress()
            
            if (error == nil) {
                // code of responce...
                let resutls = response! as NSDictionary
                print(resutls)
                let status : Int = resutls.value(forKey: "status") as! Int
                if(status == 1) {
                    
                    if resutls.value(forKey: "team_contacts") != nil {
                        
                        let team = resutls.value(forKey: "team_contacts") as! NSDictionary
                        AppUtilites.sharedInstance.teamLeadEmail = team.value(forKey: "email") as! String
                    }
                
                }else{
                    let error = resutls.value(forKey: "msg") as! String
                    
                    AppUtilites.showError(title: "Error",message: error, cancelButtonTitle:"OK")
                }
            }else{
                AppUtilites.showError(title: "Oops...",message: "Server timed out!", cancelButtonTitle:"OK")
                
            }
        }
    }
    
    func get_UserData(param : NSDictionary) {
        
        APIClient.showProgress()
        APIClient.sharedInstance.MakeAPICallWithEndURl(BASE_URL.appending(k_get_profile) as NSString, parameters: param as NSDictionary!) { (response, error) in
            
           
            
            if (error == nil) {
                // code of responce...
                let resutls = response! as NSDictionary
                print(resutls)
                let status : Int = resutls.value(forKey: "status") as! Int
                if(status == 1) {
                    
                    let user = resutls.value(forKeyPath: "user") as! NSDictionary
                    print(user)
                    AppUtilites.sharedInstance.saveUserData(data: user)
                    let user2 = AppUtilites.sharedInstance.getUserData()
                    print(user2.display_name)
                    print(user2.avatar_image)
                    print(user2.goal_status)
                    
                    self.lblUserName.text = user2.display_name
                    self.imgProfile.setIndicatorStyle(UIActivityIndicatorViewStyle.gray)
                    self.imgProfile.setShowActivityIndicator(true)
                    
                    print(user2.goal_status)
                    if(user2.goal_status == "0" || user2.goal_status.characters.count == 0){
                        
                        self.progressViewCon.constant = 0
                        self.lblGoalPercentage.text = "0%"
                        
                         APIClient.dismissProgress()
                    }
                    else{
                        
                      
                        let goalamount : NSString  = (user2.goal_status as NSString).substring(to: 3) as NSString
                        let goalamountInt = goalamount.floatValue
                        DispatchQueue.main.async {
                            self.progressView.layoutIfNeeded()
                            
//                            self.progressView.setNeedsLayout()
//                            self.progressView.setNeedsDisplay()
                            
                            if(goalamount == "100"){
                                
                                self.progressViewCon.constant =  CGFloat(self.view.frame.size.width-150)
                            }
                            else{
                                
                                self.progressViewCon.constant =  CGFloat((Float(self.fullProgressView.frame.size.width)*goalamountInt)/100)
                            }

                             self.gradeientForProgressView = AppUtilites.sharedInstance.GetBlueGradient(self.progressViewCon.constant, self.progressView.frame.size.height)
                            
                            self.progressView.layer.insertSublayer(self.gradeientForProgressView, at: 0)
                            self.lblGoalPercentage.text = "\(user2.goal_status)%"
                              APIClient.dismissProgress()
                        }
                       
                    }
                    
                }else{
                    let error = resutls.value(forKey: "msg") as! String
                    
                    AppUtilites.showError(title: "Error",message: error, cancelButtonTitle:"OK")
                }
            }else{
                AppUtilites.showError(title: "Oops...",message: "Server timed out!", cancelButtonTitle:"OK")
                
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func leftMenuButtonClicked(_ sender: Any) {
        self.slideMenuController()?.openLeft()
    }
    @IBAction func rightMenuButtonClicked(_ sender: Any) {
        self.slideMenuController()?.openRight()
    }
    
    @IBAction func onLogoutClicked(_ sender: Any) {
        
        let loginVC : CIGLoginVC = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGLoginVC") as! CIGLoginVC
        self.navigationController?.pushViewController(loginVC, animated: true)
    }
    // MARK: - Tableview
 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CIGHomeTC", for: indexPath) as! CIGHomeTC
        cell.imgType.image = UIImage(named: imgArray[indexPath.row] as! String)
        cell.lblTitle.text = titleArray[indexPath.row] as? String
        cell.lblDescription.text = descriptionArray[indexPath.row] as? String
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if(indexPath.row == 0) {
            
               var initialViewController = UIViewController()
            
            let onlineQuotingVC : CIGOnlineQuotingVC = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGOnlineQuotingVC") as! CIGOnlineQuotingVC
                        
            let leftSideMenuVC  = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGLeftSideMenuVC") as? CIGLeftSideMenuVC
            
            let rightSideMenuVC  = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGRightSideMenuVC") as? CIGRightSideMenuVC
            
           initialViewController = SlideMenuController(mainViewController: onlineQuotingVC, leftMenuViewController: leftSideMenuVC!, rightMenuViewController: rightSideMenuVC!)
            let nav = UINavigationController(rootViewController: initialViewController)
            nav.navigationBar.isHidden = true
            self.navigationController?.pushViewController(initialViewController, animated: true)
        }
        else if(indexPath.row == 1) {
            let carrierAlertVC : CIGCarrierAlertVC = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGCarrierAlertVC") as! CIGCarrierAlertVC
            
            let leftSideMenuVC  = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGLeftSideMenuVC") as? CIGLeftSideMenuVC
            
            let rightSideMenuVC  = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGRightSideMenuVC") as? CIGRightSideMenuVC
            
            let slideMenuController = SlideMenuController(mainViewController: carrierAlertVC, leftMenuViewController: leftSideMenuVC!, rightMenuViewController: rightSideMenuVC!)
            let nav = UINavigationController(rootViewController: slideMenuController)
            nav.navigationBar.isHidden = true
            self.navigationController?.pushViewController(slideMenuController, animated: true)
        }
        else if(indexPath.row == 2) {
            
            let eventTraingVC : CIGEventTraingVC = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGEventTraingVC") as! CIGEventTraingVC
            let leftSideMenuVC  = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGLeftSideMenuVC") as? CIGLeftSideMenuVC
            
            let rightSideMenuVC  = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGRightSideMenuVC") as? CIGRightSideMenuVC
            
            let slideMenuController = SlideMenuController(mainViewController: eventTraingVC, leftMenuViewController: leftSideMenuVC!, rightMenuViewController: rightSideMenuVC!)
            let nav = UINavigationController(rootViewController: slideMenuController)
            nav.navigationBar.isHidden = true
            self.navigationController?.pushViewController(slideMenuController, animated: true)

        }else if(indexPath.row == 3) {
            
            let safariVC = SFSafariViewController(url: NSURL(string: "https://www.copelandgroupusa.com/login")! as URL)
            self.present(safariVC, animated: true, completion: nil)
            safariVC.delegate = self
        }
        else if(indexPath.row == 4) {
            
            let eventTraingVC : CIGCopelandContactsVC = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGCopelandContactsVC") as! CIGCopelandContactsVC
            let leftSideMenuVC  = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGLeftSideMenuVC") as? CIGLeftSideMenuVC
            
            let rightSideMenuVC  = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGRightSideMenuVC") as? CIGRightSideMenuVC
            
            let slideMenuController = SlideMenuController(mainViewController: eventTraingVC, leftMenuViewController: leftSideMenuVC!, rightMenuViewController: rightSideMenuVC!)
            let nav = UINavigationController(rootViewController: slideMenuController)
            nav.navigationBar.isHidden = true
            self.navigationController?.pushViewController(slideMenuController, animated: true)
            
        }
        else if(indexPath.row == 5) {
            
            let eventTraingVC : CIGLeadsVC = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGLeadsVC") as! CIGLeadsVC
            let leftSideMenuVC  = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGLeftSideMenuVC") as? CIGLeftSideMenuVC
            
            let rightSideMenuVC  = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGRightSideMenuVC") as? CIGRightSideMenuVC
            
            let slideMenuController = SlideMenuController(mainViewController: eventTraingVC, leftMenuViewController: leftSideMenuVC!, rightMenuViewController: rightSideMenuVC!)
            let nav = UINavigationController(rootViewController: slideMenuController)
            nav.navigationBar.isHidden = true
            self.navigationController?.pushViewController(slideMenuController, animated: true)
            
        }else if(indexPath.row == 6){
            UIApplication.shared.openURL(NSURL(string: "calshow://")! as URL)
        }
    }
        
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients([AppUtilites.sharedInstance.teamLeadEmail])
        mailComposerVC.setSubject("CIG")
        mailComposerVC.setMessageBody(k_email_declaimer, isHTML: true)
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        
        let alert = AppUtilites.SimpleAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.")
                self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: MFMailComposeViewControllerDelegate
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
      controller.dismiss(animated: true, completion: nil)
    }
  
    
    @IBAction func btnEmailUs(_ sender: Any) {
        
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
        
    }
    
    
    @IBAction func btnHelpDesk(_ sender: Any) {
        
        let link =  "https://cig.on.spiceworks.com/portal/tickets"
        if(link.characters.count > 0){
            
            let safariVC = SFSafariViewController(url: NSURL(string: link)! as URL)
            self.present(safariVC, animated: true, completion: nil)
            safariVC.delegate = self
        }
    }
    
    
    @IBAction func btnAchievementClicked(_ sender: Any) {
        let achievementsVC  = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGAchievementsVC") as? CIGAchievementsVC
        achievementsVC?.updateGoalDelegateOnHome = self
        
        let navController : UINavigationController = UINavigationController(rootViewController: achievementsVC!)
        navController.isNavigationBarHidden = true
        
        self.present(navController, animated: true, completion: nil)
    }
}
