//
//  CIGCopelandContactsVC.swift
//  CIG
//
//  Created by Harry on 10/30/17.
//  Copyright © 2017 Harry. All rights reserved.
//

import UIKit
import SafariServices
import MessageUI


class CIGCopelandContactsVC: UIViewController,UITableViewDelegate,UITableViewDataSource ,SFSafariViewControllerDelegate,MFMailComposeViewControllerDelegate{

    
    @IBOutlet var lblGoalPercentage: UILabel!
    @IBOutlet weak var fullProgressView: UIView!
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblGood: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var progressView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var btnHelpDesk: UIButton!
    @IBOutlet weak var btnEmailUs: UIButton!
    @IBOutlet weak var progressViewCon: NSLayoutConstraint!
    @IBOutlet weak var tblContact: UITableView!
    @IBOutlet var contactTableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!
    
    var gradeientForHeader : CAGradientLayer!
    var gradeientForBtnEdit : CAGradientLayer!
    var gradeientForProgressView : CAGradientLayer!
    var gradeientForHelpDeskButton : CAGradientLayer!
    var gradeientForbtnCallNow : CAGradientLayer!
      var titleArray = NSMutableArray()
     var params = NSDictionary()
    var teams : NSArray = NSArray()
    
    var user: AMUserInfo = AMUserInfo()
    var estimatedHeight = CGFloat()

    override func viewDidLoad() {
        super.viewDidLoad()

        if (UIScreen.main.nativeBounds.height == 2436) {
            
            self.headerViewHeightConstraint.constant = self.headerViewHeightConstraint.constant + 20
            estimatedHeight = self.view.frame.size.height - 400
        }
        else{
            
            estimatedHeight = self.view.frame.size.height - 352
        }
        
        imgProfile.SetRoundImageWithBorder(image: imgProfile)
        btnHelpDesk.layoutIfNeeded()
        btnEmailUs.layoutIfNeeded()
        headerView.layoutIfNeeded()
        progressView.layoutIfNeeded()
//        progressViewCon.constant = AppUtilites.sharedInstance.getWidthFromPercentage(percentage: 75)
        
        self.slideMenuController()?.removeLeftGestures()
        self.slideMenuController()?.removeRightGestures()
        self.slideMenuController()?.addLeftGestures()
        self.slideMenuController()?.addRightGestures()
        let screenSize: CGRect = UIScreen.main.bounds
        self.slideMenuController()?.changeLeftViewWidth(screenSize.width - 20);
        self.slideMenuController()?.changeRightViewWidth(screenSize.width - 20)
        
        progressView.layoutIfNeeded()
        btnHelpDesk.layoutIfNeeded()
        btnEmailUs.layoutIfNeeded()
        
//        titleArray = ["REGIONAL DIRECTOR: John Smith","RECRUITER: John Doe","TEAM LEADER: Jennifer Jones"]
         user = AppUtilites.sharedInstance.getUserData()
      
        params = [
            "team_id":user.team_id
        ]
        
        get_teamApiCall(param: params);
        
        
        
//        self.tblContact.reloadData()
        
       
        
//        let user = AppUtilites.sharedInstance.getUserData()
        print(user.display_name)
        print(user.avatar_image)
        
         self.lblGood.text = AppUtilites.sharedInstance.getCurrentstatus()
        self.lblName.text = user.display_name
        self.imgProfile.setIndicatorStyle(UIActivityIndicatorViewStyle.gray)
        self.imgProfile.setShowActivityIndicator(true)
        
        if(user.goal_status == "0" || user.goal_status.characters.count == 0){
            
            self.progressViewCon.constant = 0
            self.lblGoalPercentage.text = "0%"
        }
        else{
            
            let goalamount : NSString  = (user.goal_status as NSString).substring(to: 3) as NSString
            let goalamountInt = goalamount.floatValue
            
            if(goalamount == "100"){
                
                self.progressViewCon.constant =  CGFloat(self.view.frame.size.width-170)
            }
            else{
                
                self.progressViewCon.constant =  CGFloat((Float(self.fullProgressView.frame.size.width)*goalamountInt)/100)
            }
            
//            self.progressViewCon.constant =  CGFloat((Float(self.fullProgressView.frame.size.width)*goalamountInt)/100)
            self.lblGoalPercentage.text = "\(user.goal_status)%"
        }
        
        self.imgProfile.sd_setImage(with: URL(string: user.avatar_image))
        
        gradeientForHeader = AppUtilites.sharedInstance.GetRedGradient(ScreenSize.SCREEN_WIDTH, headerView.frame.size.height)
        headerView.layer.insertSublayer(gradeientForHeader, at: 0)
        
        gradeientForProgressView = AppUtilites.sharedInstance.GetBlueGradient(progressViewCon.constant, progressView.frame.size.height)
        progressView.layer.insertSublayer(gradeientForProgressView, at: 0)
        
        gradeientForHelpDeskButton = AppUtilites.sharedInstance.GetGreenGradient(self.btnHelpDesk.frame.size.width, self.btnHelpDesk.frame.size.height)
        btnHelpDesk.layer.insertSublayer(gradeientForHelpDeskButton, at: 0)
        
        gradeientForbtnCallNow = AppUtilites.sharedInstance.GetBlueGradient(self.btnEmailUs.frame.size.width, self.btnEmailUs.frame.size.height)
        btnEmailUs.layer.insertSublayer(gradeientForbtnCallNow, at: 0)
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if (AppUtilites.sharedInstance.selectedTeam.count != 0){
            self.setTableHeight()
        }
        self.tblContact.reloadData()
        
        let user = AppUtilites.sharedInstance.getUserData()
        self.imgProfile.sd_setImage(with: URL(string: user.avatar_image))
        
    }
    
    func SetTableFooter() {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: 70))
        footerView.backgroundColor = UIColor.clear
        let btnEdit = UIButton(frame: CGRect(x: (ScreenSize.SCREEN_WIDTH - 250)/2, y: 10, width: 250, height: 50))
        btnEdit.setTitle("Edit Teams", for: .normal)
        gradeientForBtnEdit = AppUtilites.sharedInstance.GetBlueGradient(250,50)
        btnEdit.layer.insertSublayer(gradeientForBtnEdit, at: 0)
        footerView.addSubview(btnEdit)
        btnEdit.addTarget(self, action: #selector(self.editButtonClicked), for: .touchUpInside)

        tblContact.tableFooterView = footerView
    }
    
    func editButtonClicked(_ sender: Any)
    {
        
        let eventTraingVC : CIGEditCopelandContactsVC = (AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGEditCopelandContactsVC") as? CIGEditCopelandContactsVC)!
        let leftSideMenuVC  = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGLeftSideMenuVC") as? CIGLeftSideMenuVC
        let rightSideMenuVC  = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGRightSideMenuVC") as? CIGRightSideMenuVC
        
        let slideMenuController = SlideMenuController(mainViewController: eventTraingVC, leftMenuViewController: leftSideMenuVC!, rightMenuViewController: rightSideMenuVC!)
        let nav = UINavigationController(rootViewController: slideMenuController)
        nav.navigationBar.isHidden = true
        self.navigationController?.pushViewController(slideMenuController, animated: true)
        //      let editCopelandContactsVC  = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGEditCopelandContactsVC") as? CIGEditCopelandContactsVC
        //        self.navigationController?.pushViewController(editCopelandContactsVC!, animated: true)
    }

    // MARK: - Button Delegate
    @IBAction func leftMenuClicked(_ sender: Any) {
          self.slideMenuController()?.openLeft()
    }
    
    @IBAction func rightMenuClicked(_ sender: Any) {
         self.slideMenuController()?.openRight()
    }
   
    @IBAction func clkBtnBackEventTraining(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    // MARK: - Tableview
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(AppUtilites.sharedInstance.selectedTeam.count != 0)
        {
        return (AppUtilites.sharedInstance.selectedTeam.value(forKey: "team_contacts") as! NSArray).count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CIGCopelandContactsTC", for: indexPath) as! CIGCopelandContactsTC
        
        teams = AppUtilites.sharedInstance.selectedTeam.value(forKey: "team_contacts") as! NSArray
        let tempTitle = "\((teams[indexPath.row] as! NSDictionary).value(forKey: "position") as! String): \((teams[indexPath.row] as! NSDictionary).value(forKey: "name") as! String)"
        
        var nameArray = (tempTitle).components(separatedBy: ":")
        let myMutableString = NSMutableAttributedString(string: (tempTitle),attributes: [NSFontAttributeName:UIFont(name: "HelveticaNeue", size: 16.0)!])
        myMutableString.addAttribute(NSFontAttributeName,value:  UIFont(name: "HelveticaNeue-Bold", size: 15.0)!,range: NSRange(location: 0,length: nameArray[0].count + 1))
        
        cell.lblTitle.attributedText = myMutableString
        cell.lblEmail.text = ((teams[indexPath.row] as! NSDictionary).value(forKey: "email") as! String)
        cell.lblPhone.text = (teams[indexPath.row] as! NSDictionary).value(forKey: "phone_number") as? String
        cell.lblAddress.text = ""
        
         cell.btnCall.tag = indexPath.row
        cell.btnSendMail.tag = indexPath.row
        cell.btnGetDirection.tag = indexPath.row
        
         cell.btnCall.addTarget(self, action: #selector(self.btnCallClicked), for: .touchUpInside)
          cell.btnSendMail.addTarget(self, action: #selector(self.btnSendMailClicked), for: .touchUpInside)
//         cell.btnGetDirection.addTarget(self, action: #selector(self.btnDirectionClicked), for: .touchUpInside)
        cell.selectionStyle = .none
        dropShadow(mainView: cell.mainView, color: UIColor.gray, opacity: 0.8, offSet: CGSize.zero, radius: 3, scale: true)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 90
        
    }
    
    func dropShadow(mainView:UIView, color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        mainView.layer.masksToBounds = false
        mainView.layer.shadowColor = color.cgColor
        mainView.layer.shadowOpacity = opacity
        mainView.layer.shadowOffset = offSet
        mainView.layer.shadowRadius = radius
        
        mainView.layer.shadowPath = UIBezierPath(rect: mainView.bounds).cgPath
        mainView.layer.shouldRasterize = true
        mainView.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    func btnCallClicked(_ sender: Any)
    {
        let phone = (teams[(sender as! UIButton).tag] as! NSDictionary).value(forKey: "phone_number") as? String
        
        if( UIApplication.shared.canOpenURL(URL(string: "tel://\(phone!)")!))
        {
            UIApplication.shared.openURL(URL(string: "tel://\(phone!)")!)
        }
        else
        {
            AppUtilites.showError(title: "Oops", message: "invalid contact number", cancelButtonTitle: "OK")
        }
    }
    
    func btnSendMailClicked(_ sender: Any)
    {
        let emailID = (teams[(sender as! UIButton).tag] as! NSDictionary).value(forKey: "email") as? String
        let mailComposeViewController = configuredMailComposeViewController(mailID: emailID!)
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
    }
    
    func btnDirectionClicked(_ sender: Any)
    {
        
    }
    
    func configuredMailComposeViewController(mailID: String) -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
       mailComposerVC.setToRecipients([mailID])
        mailComposerVC.setSubject("CIG")
      mailComposerVC.setMessageBody(k_email_declaimer, isHTML: true)
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        
        let alert = AppUtilites.SimpleAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.")
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: MFMailComposeViewControllerDelegate
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func btnEmailUs(_ sender: Any) {
        
        let mailComposeViewController = configuredMailComposeViewController(mailID: AppUtilites.sharedInstance.teamLeadEmail)
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
        
    }
    
    
    @IBAction func clkBtnBackCopelandContact(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnHelpDesk(_ sender: Any) {
        
        let link =  "https://cig.on.spiceworks.com/portal/tickets"
        if(link.characters.count > 0){
            
            let safariVC = SFSafariViewController(url: NSURL(string: link)! as URL)
            self.present(safariVC, animated: true, completion: nil)
            safariVC.delegate = self
        }
    }
    
    func setTableHeight(){
        
        if(UIDevice.current.userInterfaceIdiom != .pad && AppUtilites.sharedInstance.selectedTeam.count > 0) {
            
            if ((AppUtilites.sharedInstance.selectedTeam.value(forKey: "team_contacts") as! NSArray).count == 0){
                
                self.contactTableViewHeightConstraint.constant = estimatedHeight
            }
            else {
                
                self.contactTableViewHeightConstraint.constant = 0
                for array in AppUtilites.sharedInstance.selectedTeam {
                    
                    self.contactTableViewHeightConstraint.constant += CGFloat((AppUtilites.sharedInstance.selectedTeam.value(forKey: "team_contacts") as! NSArray).count * 42)
                }
                
                if( self.contactTableViewHeightConstraint.constant < estimatedHeight){
                    
                    self.contactTableViewHeightConstraint.constant = estimatedHeight
                }
            }
        }
    }
    
    // MARK: - API call
    
    func get_teamApiCall(param : NSDictionary) {
        
        APIClient.showProgress()
        APIClient.sharedInstance.MakeAPICallWithEndURl(BASE_URL.appending(k_get_team) as NSString, parameters: param as NSDictionary!) { (response, error) in
            
            APIClient.dismissProgress()
            
            if (error == nil) {
                // code of responce...
                let resutls = response! as NSDictionary
                print(resutls)
                let status : Int = resutls.value(forKey: "status") as! Int
                if(status == 1) {
                    let team = resutls.value(forKey: "team") as! NSArray
                    AppUtilites.sharedInstance.allTeam = team
                    for i in 0...team.count - 1 {
                        if(self.user.team_id == (team[i] as! NSDictionary).value(forKey: "team_id") as! String )// (team[i] as! NSDictionary).value(forKey: "team_id") as! String
                       {
                        AppUtilites.sharedInstance.selectedTeam = team[i] as! NSDictionary
                        }
                    }
                    
                    self.setTableHeight()
                    self.tblContact.reloadData()
                    self.SetTableFooter()
                    
                }else{
                    let error = resutls.value(forKey: "msg") as! String
                    
                    AppUtilites.showError(title: "Error",message: error, cancelButtonTitle:"OK")
                }
            }else{
                AppUtilites.showError(title: "Oops...",message: "Server timed out!", cancelButtonTitle:"OK")
                
            }
        }
    }
    
}
