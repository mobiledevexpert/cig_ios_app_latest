//
//  CIGAchievementsVC.swift
//  CIG
//
//  Created by Harry on 10/31/17.
//  Copyright © 2017 Harry. All rights reserved.
//

import UIKit

protocol updateGoalStatusAfterUpdatingAchievementDelegate: class
    
{
    func updateGoalProgressBar()
}

class CIGAchievementsVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tblAchievement: UITableView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var upperView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!

     var goal_detailArray : NSArray = NSArray()
    weak var updateGoalDelegateOnHome: updateGoalStatusAfterUpdatingAchievementDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if (UIScreen.main.nativeBounds.height == 2436) {
            self.headerViewHeightConstraint.constant = self.headerViewHeightConstraint.constant + 20
        }
        

//        let tap = UITapGestureRecognizer(target: self, action: Selector("handleTap:"))
//        mainView.addGestureRecognizer(tap)
//        dropShadow(mainView: upperView, color: UIColor.black, opacity: 1.0, offSet: CGSize(width: 0.0 , height: 2.0), radius: 0, scale: true)

        dropShadow(mainView: upperView, color: UIColor.gray, opacity: 0.8, offSet: CGSize(width: 0.0 , height: 1.0), radius: 3, scale: true)
        dropShadow(mainView: bottomView, color: UIColor.gray, opacity: 0.8, offSet: CGSize(width: 0 , height: -1.0), radius: 3, scale: true)
        
        getGoalDetails()
    }
    
    func handleTap(sender: UITapGestureRecognizer? = nil) {
        self.dismiss(animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
     // MARK: - Button Clicked
    
    @IBAction func btnDoneClicked(_ sender: Any) {
        
        updateGoalDelegateOnHome?.updateGoalProgressBar()
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Tableview
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return goal_detailArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return ((goal_detailArray[section] as! NSDictionary).value(forKey: "goal_detail") as! NSArray).count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CIGAchievementsTC", for: indexPath) as! CIGAchievementsTC
        cell.lblTitle.text = (((goal_detailArray[indexPath.section] as! NSDictionary).value(forKey: "goal_detail") as! NSArray)[indexPath.row] as! NSDictionary).value(forKey: "product_name") as! String
        cell.selectionStyle = .none
        cell.btnOnePlus.addTarget(self, action: #selector(self.increseButtonClicked), for: .touchUpInside)
        cell.btnOnePlus.tag = Int((((goal_detailArray[indexPath.section] as! NSDictionary).value(forKey: "goal_detail") as! NSArray)[indexPath.row] as! NSDictionary).value(forKey: "goal_id") as! String)!

        return cell
    }
    
    @objc func increseButtonClicked(_ sender: Any) {

        let goalId = (sender as! UIButton).tag
        
        let params = [
            "goal_id": goalId
            ] as [String : Any]
        
        APIClient.showProgress()
        APIClient.sharedInstance.MakeAPICallWithEndURl(BASE_URL.appending(k_increase_goal) as NSString, parameters: params as NSDictionary!) { (response, error) in
            
            APIClient.dismissProgress()
            
            if (error == nil) {
                // code of responce...
                let resutls = response! as NSDictionary
                print(resutls)
                let status : Int = resutls.value(forKey: "status") as! Int
                if(status == 1) {
                   
                    AppUtilites.sharedInstance.isgoalUpdated = true
//                    AppUtilites.showError(title: "Message",message: "Goal Sucessfully Incremented.", cancelButtonTitle:"OK")
                    
                    AppUtilites.showSucess(message: "Goal Sucessfully Incremented.")
//                    self.getGoalDetails()
                }else{
                    let error = resutls.value(forKey: "msg") as! String
                    
                    AppUtilites.showError(title: "Error",message: error, cancelButtonTitle:"OK")
                }
            }else{
                AppUtilites.showError(title: "Oops...",message: "Server timed out!", cancelButtonTitle:"OK")
                
            }
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if(UIDevice.current.userInterfaceIdiom == .pad) {
            
            return 70
        }
        
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return  SetHeaderView(title: (((goal_detailArray[section] as! NSDictionary).value(forKey: "carrier_name") as! String)))
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func SetHeaderView(title: String)->UIView {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: 50))
        headerView.backgroundColor = UIColor(red: 243.0/255.0, green: 243.0/255.0, blue: 243.0/255.0, alpha: 1.0)
        let lblHeader = UILabel(frame: CGRect(x: 15, y: 0, width: ScreenSize.SCREEN_WIDTH, height: 50))
        lblHeader.text = title
        lblHeader.font = UIFont(name: "Trebuchet MS", size: 19.0)
        lblHeader.textColor = UIColor.black
        headerView.addSubview(lblHeader)
        return headerView
    }
    
    func dropShadow(mainView:UIView, color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        mainView.layer.masksToBounds = false
        mainView.layer.shadowColor = color.cgColor
        mainView.layer.shadowOpacity = opacity
        mainView.layer.shadowOffset = offSet
        mainView.layer.shadowRadius = radius
        
        mainView.layer.shadowPath = UIBezierPath(rect: mainView.bounds).cgPath
        mainView.layer.shouldRasterize = true
        mainView.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    @IBAction func editCarrierClicked(_ sender: Any) {
        
        let controller = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGPersonalGoalsVC") as! CIGPersonalGoalsVC
        self.navigationController?.pushViewController(controller, animated: true)
    }
    // MARK: - API call
    
    func getGoalDetails(){
        
        let user = AppUtilites.sharedInstance.getUserData()
        let params = [
            "user_id": user.user_id
            ] as [String : Any]
        
        APIClient.showProgress()
        APIClient.sharedInstance.MakeAPICallWithEndURl(BASE_URL.appending(getGoalDetail) as NSString, parameters: params as NSDictionary!) { (response, error) in
            
            APIClient.dismissProgress()
            
            if (error == nil) {
                // code of responce...
                let resutls = response! as NSDictionary
                print(resutls)
                let status : Int = resutls.value(forKey: "status") as! Int
                if(status == 1) {
                    self.goal_detailArray = resutls.value(forKey: "goal_all_detail") as! NSArray
                    self.tblAchievement.reloadData()
                    
                }else{
                    let error = resutls.value(forKey: "msg") as! String
                    
                    AppUtilites.showError(title: "Error",message: error, cancelButtonTitle:"OK")
                }
            }else{
                AppUtilites.showError(title: "Oops...",message: "Server timed out!", cancelButtonTitle:"OK")
                
            }
        }
    }
}

