//
//  CIGCopelandPerksVC.swift
//  CIG
//
//  Created by Harry on 10/31/17.
//  Copyright © 2017 Harry. All rights reserved.
//

import UIKit


class CIGCopelandPerksVC: UIViewController {

     @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var btnReturn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        if (UIScreen.main.nativeBounds.height == 2436) {
            self.headerViewHeightConstraint.constant = self.headerViewHeightConstraint.constant + 20
        }
        
        let width = self.view.frame.size.width - 100
        if(UIDevice.current.userInterfaceIdiom == .pad) {
            let  gradeientForbtnCallNow = AppUtilites.sharedInstance.GetGreenGradient(width, self.btnReturn.frame.size.height)
            btnReturn.layer.insertSublayer(gradeientForbtnCallNow, at: 0)
            
        }else
        {
            let  gradeientForbtnCallNow = AppUtilites.sharedInstance.GetGreenGradient(width, self.btnReturn.frame.size.height)
            btnReturn.layer.insertSublayer(gradeientForbtnCallNow, at: 0)
            
        }
      
        
      
      
        btnReturn.layoutIfNeeded()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnreturnClicked(_ sender: Any) {
         self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func btnbackClicked(_ sender: Any) {
        
         self.navigationController?.popViewController(animated: true)
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
