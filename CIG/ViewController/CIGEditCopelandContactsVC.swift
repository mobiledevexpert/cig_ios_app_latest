//
//  CIGEditCopelandContactsVC.swift
//  CIG
//
//  Created by Harry on 10/31/17.
//  Copyright © 2017 Harry. All rights reserved.
//

import UIKit
import SafariServices
import MessageUI

//import DropDown

class CIGEditCopelandContactsVC: UIViewController,UITableViewDelegate,UITableViewDataSource ,SFSafariViewControllerDelegate,MFMailComposeViewControllerDelegate{

    @IBOutlet var lblGoalPercentage: UILabel!
    @IBOutlet weak var fullProgressView: UIView!
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblGood: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var progressView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var btnHelpDesk: UIButton!
    @IBOutlet weak var btnEmailUs: UIButton!
    @IBOutlet weak var progressViewCon: NSLayoutConstraint!
    @IBOutlet weak var tblContact: UITableView!
    @IBOutlet weak var btnChangeState: UIButton!
    @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!
//     let dropDown = DropDown()
    
    var gradeientForHeader : CAGradientLayer!
    var gradeientForBtnEdit : CAGradientLayer!
    var gradeientForProgressView : CAGradientLayer!
    var gradeientForHelpDeskButton : CAGradientLayer!
    var gradeientForbtnCallNow : CAGradientLayer!
    var selectedButton:UIButton = UIButton()
    var selectedTeam : NSDictionary = NSDictionary()
    var teams : NSArray = NSArray()
    var user : AMUserInfo = AMUserInfo()
    
    @IBOutlet var contactTableViewHeightConstraint: NSLayoutConstraint!
    var estimatedHeight = CGFloat()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if (UIScreen.main.nativeBounds.height == 2436) {
            self.headerViewHeightConstraint.constant = self.headerViewHeightConstraint.constant + 20
        }

        
        estimatedHeight = self.view.frame.size.height - 400

        imgProfile.SetRoundImageWithBorder(image: imgProfile)
        btnHelpDesk.layoutIfNeeded()
        btnEmailUs.layoutIfNeeded()
        headerView.layoutIfNeeded()
        progressView.layoutIfNeeded()
//        progressViewCon.constant = AppUtilites.sharedInstance.getWidthFromPercentage(percentage: 75)
        
        self.slideMenuController()?.removeLeftGestures()
        self.slideMenuController()?.removeRightGestures()
        self.slideMenuController()?.addLeftGestures()
        self.slideMenuController()?.addRightGestures()
        let screenSize: CGRect = UIScreen.main.bounds
        self.slideMenuController()?.changeLeftViewWidth(screenSize.width - 20);
        self.slideMenuController()?.changeRightViewWidth(screenSize.width - 20)
        
        progressView.layoutIfNeeded()
        btnHelpDesk.layoutIfNeeded()
        btnEmailUs.layoutIfNeeded()
        selectedTeam = AppUtilites.sharedInstance.selectedTeam
        tblContact.reloadData()
//        SetTableFooter()
//        DropDown.startListeningToKeyboard()
       
        
        // The view to which the drop down will appear on
//        dropDown.anchorView = btnChangeState // UIView or UIBarButtonItem
        
        // The list of items to display. Can be changed dynamically
//        dropDown.dataSource = ["Car", "Motorcycle", "Truck"]
        
//        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
//            print("Selected item: \(item) at index: \(index)")
//            self.dropDown.hide()
//        }
        
//        dropDown.bottomOffset = CGPoint(x: 0, y: btnChangeState.bounds.height)
//
//        // Will set a custom width instead of the anchor view width
//        dropDown.width = 200
        
        
        
        
        self.setTableHeight()
        
        user = AppUtilites.sharedInstance.getUserData()
        print(user.display_name)
        print(user.avatar_image)
         self.lblGood.text = AppUtilites.sharedInstance.getCurrentstatus()
        self.lblName.text = user.display_name
        self.imgProfile.setIndicatorStyle(UIActivityIndicatorViewStyle.gray)
        self.imgProfile.setShowActivityIndicator(true)
        
        if(user.goal_status == "0" || user.goal_status.characters.count == 0){
            
            self.progressViewCon.constant = 0
            self.lblGoalPercentage.text = "0%"
        }
        else{
            
            let goalamount : NSString  = (user.goal_status as NSString).substring(to: 3) as NSString
            let goalamountInt = goalamount.floatValue
//            self.progressViewCon.constant =  CGFloat((Float(self.fullProgressView.frame.size.width)*goalamountInt)/100)
            
            if(goalamount == "100"){
                
                self.progressViewCon.constant =  CGFloat(self.view.frame.size.width-150)
            }
            else{
                
                self.progressViewCon.constant =  CGFloat((Float(self.fullProgressView.frame.size.width)*goalamountInt)/100)
            }
            
            self.lblGoalPercentage.text = "\(user.goal_status)%"
        }
        
        self.imgProfile.sd_setImage(with: URL(string: user.avatar_image))

        gradeientForHeader = AppUtilites.sharedInstance.GetRedGradient(ScreenSize.SCREEN_WIDTH, headerView.frame.size.height)
        headerView.layer.insertSublayer(gradeientForHeader, at: 0)
        
        gradeientForProgressView = AppUtilites.sharedInstance.GetBlueGradient(progressViewCon.constant, progressView.frame.size.height)
        progressView.layer.insertSublayer(gradeientForProgressView, at: 0)
        
        gradeientForHelpDeskButton = AppUtilites.sharedInstance.GetGreenGradient(self.btnHelpDesk.frame.size.width, self.btnHelpDesk.frame.size.height)
        btnHelpDesk.layer.insertSublayer(gradeientForHelpDeskButton, at: 0)
        
        gradeientForbtnCallNow = AppUtilites.sharedInstance.GetBlueGradient(self.btnEmailUs.frame.size.width, self.btnEmailUs.frame.size.height)
        btnEmailUs.layer.insertSublayer(gradeientForbtnCallNow, at: 0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
        let user = AppUtilites.sharedInstance.getUserData()
        self.imgProfile.sd_setImage(with: URL(string: user.avatar_image))
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients([AppUtilites.sharedInstance.teamLeadEmail])
        mailComposerVC.setSubject("CIG")
      mailComposerVC.setMessageBody(k_email_declaimer, isHTML: true)
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        
        let alert = AppUtilites.SimpleAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.")
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: MFMailComposeViewControllerDelegate
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    // MARK: - Button Delegate
    @IBAction func leftMenuClicked(_ sender: Any) {
      self.slideMenuController()?.openRight()
    }
    
    @IBAction func rightMenuClicked(_ sender: Any) {
          self.slideMenuController()?.openLeft()
        
    }
    
    @IBAction func btnHelpClicked(_ sender: Any) {
        let link =  "https://cig.on.spiceworks.com/portal/tickets"
        if(link.characters.count > 0){
            
            let safariVC = SFSafariViewController(url: NSURL(string: link)! as URL)
            self.present(safariVC, animated: true, completion: nil)
            safariVC.delegate = self
        }
    }
    
    @IBAction func btnEmailClicked(_ sender: Any) {
        
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
        
    }
    
    @IBAction func btnBackClicked(_ sender: Any) {
         self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnChangeState(_ sender: Any) {
//        dropDown.show()
    }
    
    @IBAction func btnsaveClicked(_ sender: Any) {
        
       var params = [
            "user_id":user.user_id,
            "team_id":selectedTeam.value(forKey: "team_id")
        ]
        change_teamApiCall(param: params as NSDictionary)
        
    }
    func SetTableFooter() {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: 70))
        footerView.backgroundColor = UIColor.clear
        let btnSave = UIButton(frame: CGRect(x: (ScreenSize.SCREEN_WIDTH - 250)/2, y: 10, width: 250, height: 50))
        btnSave.setTitle("Save", for: .normal)
        gradeientForBtnEdit = AppUtilites.sharedInstance.GetGreenGradient(250,50)
        btnSave.layer.insertSublayer(gradeientForBtnEdit, at: 0)
        footerView.addSubview(btnSave)
        btnSave.addTarget(self, action: #selector(self.btnsaveClicked), for: .touchUpInside)
        
        tblContact.tableFooterView = footerView
    }
    
   
    
    // MARK: - Tableview
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if(AppUtilites.sharedInstance.allTeam.count != 0)
        {
            return AppUtilites.sharedInstance.allTeam.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if((AppUtilites.sharedInstance.allTeam[section] as! NSDictionary).count != 0)
        {
            return ((AppUtilites.sharedInstance.allTeam[section] as! NSDictionary).value(forKey: "team_contacts") as! NSArray).count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CIGCopelandContactsTC", for: indexPath) as! CIGCopelandContactsTC
        
        teams = (AppUtilites.sharedInstance.allTeam[indexPath.section] as! NSDictionary).value(forKey: "team_contacts") as! NSArray
        
        let tempTitle = "\((teams[indexPath.row] as! NSDictionary).value(forKey: "position") as! String): \((teams[indexPath.row] as! NSDictionary).value(forKey: "name") as! String)"
        
        var nameArray = (tempTitle).components(separatedBy: ":")
        let myMutableString = NSMutableAttributedString(string: (tempTitle),attributes: [NSFontAttributeName:UIFont(name: "Trebuchet MS", size: 16.0)!])
        myMutableString.addAttribute(NSFontAttributeName,value:  UIFont(name: "TrebuchetMS-Bold", size: 15.0)!,range: NSRange(location: 0,length: nameArray[0].count + 1))
        
        cell.lblTitle.attributedText = myMutableString
        cell.lblEmail.text = ((teams[indexPath.row] as! NSDictionary).value(forKey: "email") as! String)
        cell.lblPhone.text = (teams[indexPath.row] as! NSDictionary).value(forKey: "phone_number") as? String
        cell.lblAddress.text = ""
        
//        (indexPath.section * 1000) + indexPath.row
        
        cell.btnCall.myRow = indexPath.row
        cell.btnCall.mySection = indexPath.section
        
        cell.btnSendMail.myRow = indexPath.row
        cell.btnSendMail.mySection = indexPath.section
        
        cell.btnGetDirection.myRow = indexPath.row
        cell.btnGetDirection.mySection = indexPath.section
        
        cell.btnCall.addTarget(self, action: #selector(self.btnCallClicked), for: .touchUpInside)
        cell.btnSendMail.addTarget(self, action: #selector(self.btnSendMailClicked), for: .touchUpInside)
//        cell.btnGetDirection.addTarget(self, action: #selector(self.btnDirectionClicked), for: .touchUpInside)
//        cell.selectionStyle = .none
        dropShadow(mainView: cell.mainView, color: UIColor.gray, opacity: 0.8, offSet: CGSize.zero, radius: 3, scale: true)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.tblContact.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return  SetHeaderView(title:  (AppUtilites.sharedInstance.allTeam[section] as! NSDictionary).value(forKey: "team_name") as! String,tag: section)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func SetHeaderView(title: String,tag: Int)->UIView {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: 50))
        headerView.backgroundColor = UIColor(red: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha: 1.0)
        let lblHeader = UILabel(frame: CGRect(x: 15, y: 0, width: ScreenSize.SCREEN_WIDTH - 50, height: 50))
        lblHeader.text = title
        lblHeader.textAlignment = .center
        lblHeader.font = UIFont(name: "TrebuchetMS-Bold", size: 19.0)
        lblHeader.textColor = UIColor.black
        
        let btnSelect = UIButton(frame: CGRect(x: (ScreenSize.SCREEN_WIDTH - 30), y: 15, width: 20, height: 20))
        btnSelect.setImage(UIImage.init(named: "ic_unselected"), for: .normal)
        btnSelect.setImage(UIImage.init(named: "ic_selected"), for: .selected)
        btnSelect.tag = tag
        btnSelect.addTarget(self, action: #selector(self.btnSelectClicked), for: .touchUpInside)
        
        if((selectedTeam.count != 0 && selectedTeam.value(forKey: "team_name")as! String == title))
        {
            selectedButton = btnSelect
             btnSelect.isSelected = true
        }
        else
        {
            btnSelect.isSelected = false
        }
        
        headerView.addSubview(lblHeader)
        headerView.addSubview(btnSelect)
        return headerView
    }

    func btnSelectClicked(_ sender: Any)
    {
        if(sender as! UIButton).isSelected
        {
//            (sender as! UIButton).isSelected = false
//             AppUtilites.sharedInstance.selectedTeam =  NSDictionary ()
        }
        else
        {
            selectedTeam = AppUtilites.sharedInstance.allTeam[(sender as! UIButton).tag] as! NSDictionary
            (sender as! UIButton).isSelected = true
            selectedButton.isSelected = false
            selectedButton = (sender as! UIButton)
        }
    }
    
    func dropShadow(mainView:UIView, color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        mainView.layer.masksToBounds = false
        mainView.layer.shadowColor = color.cgColor
        mainView.layer.shadowOpacity = opacity
        mainView.layer.shadowOffset = offSet
        mainView.layer.shadowRadius = radius
        
        mainView.layer.shadowPath = UIBezierPath(rect: mainView.bounds).cgPath
        mainView.layer.shouldRasterize = true
        mainView.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    func btnCallClicked(_ sender: Any)
    {
        
        let teamSection = (AppUtilites.sharedInstance.allTeam[(sender as! MyButton).mySection!] as! NSDictionary).value(forKey: "team_contacts") as! NSArray
        
        let phone = (teamSection[(sender as! MyButton).myRow!] as! NSDictionary).value(forKey: "phone_number") as? String
        
        print(phone)
        
        if( UIApplication.shared.canOpenURL(URL(string: "tel://\(phone!)")!))
        {
            UIApplication.shared.openURL(URL(string: "tel://\(phone!)")!)
        }
        else
        {
            AppUtilites.showError(title: "Oops", message: "invalid contact number", cancelButtonTitle: "OK")
        }
    }
    
    func btnSendMailClicked(_ sender: Any){
        
        let teamSection = (AppUtilites.sharedInstance.allTeam[(sender as! MyButton).mySection!] as! NSDictionary).value(forKey: "team_contacts") as! NSArray
        
        let emailID = (teamSection[(sender as! MyButton).myRow!] as! NSDictionary).value(forKey: "email") as? String
        
         print(emailID)
        
        let mailComposeViewController = configuredMailComposeViewController(mailID: emailID!)
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
    }
    
    func configuredMailComposeViewController(mailID: String) -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients([mailID])
        mailComposerVC.setSubject("CIG")
      mailComposerVC.setMessageBody(k_email_declaimer, isHTML: true)
        
        return mailComposerVC
    }
    
    
    func btnDirectionClicked(_ sender: Any)
    {
        
    }
    
    func change_teamApiCall(param : NSDictionary) {
        
        APIClient.showProgress()
        APIClient.sharedInstance.MakeAPICallWithEndURl(BASE_URL.appending(k_change_team) as NSString, parameters: param as NSDictionary!) { (response, error) in
            
            APIClient.dismissProgress()
            
            if (error == nil) {
                // code of responce...
                let resutls = response! as NSDictionary
                //                print(resutls)
                let status : Int = resutls.value(forKey: "status") as! Int
                if(status == 1) {
                    var tempUser = [ "avatar" : self.user.avatar_image,"created":self.user.created_at,"display_name":self.user.display_name,"email":self.user.email,"updated":self.user.updated_at,"user_id":self.user.user_id,"team_id":self.selectedTeam.value(forKey: "team_id"),"goal_status":self.user.goal_status
                    ]
                    AppUtilites.sharedInstance.saveUserData(data: tempUser as NSDictionary)
                    AppUtilites.sharedInstance.selectedTeam = self.selectedTeam
                    self.navigationController?.popViewController(animated: false)
                    
                    let param = ["team_id": self.selectedTeam.value(forKey: "team_id")]
                    
                    self.get_teamApiCall(param: param as NSDictionary)
                    
                }else{
                    let error = resutls.value(forKey: "msg") as! String
                    
                    AppUtilites.showError(title: "Error",message: error, cancelButtonTitle:"OK")
                }
            }else{
                AppUtilites.showError(title: "Oops...",message: "Server timed out!", cancelButtonTitle:"OK")
                
            }
        }
    }

    func setTableHeight(){
        
        if(UIDevice.current.userInterfaceIdiom != .pad) {
            
            if (AppUtilites.sharedInstance.allTeam.count == 0){
                
                self.contactTableViewHeightConstraint.constant = estimatedHeight
            }
            else {
                
                self.contactTableViewHeightConstraint.constant = 0
                
                for dic in AppUtilites.sharedInstance.allTeam {
                    
                    self.contactTableViewHeightConstraint.constant += CGFloat(((dic as! NSDictionary).value(forKey: "team_contacts") as! NSArray).count * 105)
                }
                
                if( self.contactTableViewHeightConstraint.constant < estimatedHeight){
                    
                    self.contactTableViewHeightConstraint.constant = estimatedHeight
                }
            }
        }
    }
    // MARK: - API call
    
    
    func get_teamApiCall(param : NSDictionary) {
        
        APIClient.showProgress()
        APIClient.sharedInstance.MakeAPICallWithEndURl(BASE_URL.appending(k_get_team_contact_detail) as NSString, parameters: param as NSDictionary!) { (response, error) in
            
            APIClient.dismissProgress()
            
            if (error == nil) {
                // code of responce...
                let resutls = response! as NSDictionary
                print(resutls)
                let status : Int = resutls.value(forKey: "status") as! Int
                if(status == 1) {
                    
                    if resutls.value(forKey: "team_contacts") != nil {
                        
                        let team = resutls.value(forKey: "team_contacts") as! NSDictionary
                         AppUtilites.sharedInstance.teamLeadEmail = team.value(forKey: "email") as! String
                    }
                    
                }else{
                    let error = resutls.value(forKey: "msg") as! String
                    
                    AppUtilites.showError(title: "Error",message: error, cancelButtonTitle:"OK")
                }
            }else{
                AppUtilites.showError(title: "Oops...",message: "Server timed out!", cancelButtonTitle:"OK")
                
            }
        }
    }
}

