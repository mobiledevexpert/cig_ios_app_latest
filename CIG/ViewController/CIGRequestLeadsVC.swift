//
//  CIGRequestLeadsVC.swift
//  CIG
//
//  Created by Harry on 11/1/17.
//  Copyright © 2017 Harry. All rights reserved.
//

import UIKit
import SafariServices
import MessageUI
import EventKit
import Photos
import OpinionzAlertView

class CIGRequestLeadsVC: UIViewController,SFSafariViewControllerDelegate,MFMailComposeViewControllerDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    
    
    @IBOutlet var lblGoalPercentage: UILabel!
    @IBOutlet weak var fullProgressView: UIView!

    @IBOutlet weak var lblGood: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    
    @IBOutlet var txtTypeOfListRequest: UITextField!
    @IBOutlet var txtViewNotes: UITextView!
      let imagePicker = UIImagePickerController()
    @IBOutlet var lblfileName: UILabel!
    @IBOutlet var txtRegionalManagerTeam: UITextField!
    @IBOutlet var txtState: UITextField!
    @IBOutlet var txtCity: UITextField!
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtName: UITextField!
    @IBOutlet var imgProfileView: UIImageView!
    
     @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var btnHelpDeak: UIButton!
    @IBOutlet weak var btnCallNow: UIButton!
     @IBOutlet weak var progressView: UIView!
     @IBOutlet weak var progressViewCon: NSLayoutConstraint!
    @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!

    var gradeientForHeader : CAGradientLayer!
    var gradeientForProgressView : CAGradientLayer!
    var gradeientForHelpDeskButton : CAGradientLayer!
    var gradeientForbtnCallNow : CAGradientLayer!
    
    var imageselected = UIImage()
    var isImageSelected = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if (UIScreen.main.nativeBounds.height == 2436) {
            self.headerViewHeightConstraint.constant = self.headerViewHeightConstraint.constant + 20
        }

        
        imgProfile.SetRoundImageWithBorder(image: imgProfile)
        progressView.layoutIfNeeded()
        headerView.layoutIfNeeded()
        btnHelpDeak.layoutIfNeeded()
        btnCallNow.layoutIfNeeded()
        
        imagePicker.delegate = self
        self.isImageSelected = false
        
        let user = AppUtilites.sharedInstance.getUserData()
        print(user.display_name)
        print(user.avatar_image)
        
        self.lblName.text = user.display_name
        self.imgProfile.setIndicatorStyle(UIActivityIndicatorViewStyle.gray)
        self.imgProfile.setShowActivityIndicator(true)
        
        if(user.goal_status == "0" || user.goal_status.characters.count == 0){
            
            self.progressViewCon.constant = 0
            self.lblGoalPercentage.text = "0%"
        }
        else{
            
            let goalamount : NSString  = (user.goal_status as NSString).substring(to: 3) as NSString
            let goalamountInt = goalamount.floatValue
//            self.progressViewCon.constant =  CGFloat((Float(self.fullProgressView.frame.size.width)*goalamountInt)/100)
            
            if(goalamount == "100"){
                
                self.progressViewCon.constant =  CGFloat(self.view.frame.size.width-170)
            }
            else{
                
                self.progressViewCon.constant =  CGFloat((Float(self.fullProgressView.frame.size.width)*goalamountInt)/100)
            }
            
            self.lblGoalPercentage.text = "\(user.goal_status)%"
        }
        
        self.imgProfile.sd_setImage(with: URL(string: user.avatar_image))
        
        gradeientForHeader = AppUtilites.sharedInstance.GetRedGradient(ScreenSize.SCREEN_WIDTH, headerView.frame.size.height)
        headerView.layer.insertSublayer(gradeientForHeader, at: 0)
        
        gradeientForProgressView = AppUtilites.sharedInstance.GetBlueGradient(progressViewCon.constant, progressView.frame.size.height)
        progressView.layer.insertSublayer(gradeientForProgressView, at: 0)
        
        gradeientForHelpDeskButton = AppUtilites.sharedInstance.GetGreenGradient(self.view.frame.size.width/2, self.btnHelpDeak.frame.size.height)
        btnHelpDeak.layer.insertSublayer(gradeientForHelpDeskButton, at: 0)
        
         gradeientForbtnCallNow = AppUtilites.sharedInstance.GetBlueGradient(self.view.frame.size.width/2, self.btnCallNow.frame.size.height)
        btnCallNow.layer.insertSublayer(gradeientForbtnCallNow, at: 0)
        
        
        // Do any additional setup after loading the view.
    }

    @IBAction func btnBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        let user = AppUtilites.sharedInstance.getUserData()
        print(user.display_name)
        print(user.avatar_image)
        
        self.lblName.text = user.display_name
        self.imgProfile.setIndicatorStyle(UIActivityIndicatorViewStyle.gray)
        self.imgProfile.setShowActivityIndicator(true)
        
        if(user.goal_status == "0" || user.goal_status.characters.count == 0){
            
            self.progressViewCon.constant = 0
            self.lblGoalPercentage.text = "0%"
        }
        else{
            
            let goalamount : NSString  = (user.goal_status as NSString).substring(to: 3) as NSString
            let goalamountInt = goalamount.floatValue
//            self.progressViewCon.constant =  CGFloat((Float(self.fullProgressView.frame.size.width)*goalamountInt)/100)
            if(goalamount == "100"){
                
                self.progressViewCon.constant =  CGFloat(self.view.frame.size.width-150)
            }
            else{
                
                self.progressViewCon.constant =  CGFloat((Float(self.fullProgressView.frame.size.width)*goalamountInt)/100)
            }
            
            self.lblGoalPercentage.text = "\(user.goal_status)%"
        }
        
        self.imgProfile.sd_setImage(with: URL(string: user.avatar_image))
        
        gradeientForHeader = AppUtilites.sharedInstance.GetRedGradient(ScreenSize.SCREEN_WIDTH, headerView.frame.size.height)
        headerView.layer.insertSublayer(gradeientForHeader, at: 0)
        
        gradeientForProgressView = AppUtilites.sharedInstance.GetBlueGradient(progressViewCon.constant, progressView.frame.size.height)
        progressView.layer.insertSublayer(gradeientForProgressView, at: 0)
        
        gradeientForHelpDeskButton = AppUtilites.sharedInstance.GetGreenGradient(self.view.frame.size.width/2, self.btnHelpDeak.frame.size.height)
        btnHelpDeak.layer.insertSublayer(gradeientForHelpDeskButton, at: 0)
        
         gradeientForbtnCallNow = AppUtilites.sharedInstance.GetBlueGradient(self.view.frame.size.width/2, self.btnCallNow.frame.size.height)
        btnCallNow.layer.insertSublayer(gradeientForbtnCallNow, at: 0)

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnLeftMenuClicked(_ sender: Any) {
          self.slideMenuController()?.openLeft()
    }
    @IBAction func btnRightMenuClicked(_ sender: Any) {
         self.slideMenuController()?.openRight()
    }
    
    @IBAction func btnChooseFileClicked(_ sender: Any) {
        
        let alertController = UIAlertController(title: "Select Image", message: "", preferredStyle: .actionSheet)
        
        let sendButton = UIAlertAction(title: "Take Photo", style: .default, handler: { (action) -> Void in
            self.OpenImagePicker(openWithCamera: true)
        })
        
        let  deleteButton = UIAlertAction(title: "Choose Photo", style: .default, handler: { (action) -> Void in
            self.OpenImagePicker(openWithCamera: false)
        })
        
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in
            
        })
        
        alertController.addAction(sendButton)
        alertController.addAction(deleteButton)
        alertController.addAction(cancelButton)
        if let popoverController = alertController.popoverPresentationController {
            popoverController.sourceView = sender as! UIView
            popoverController.sourceRect = (sender as AnyObject).bounds
        }
        //        self.present(alertController, animated: true, completion: nil)
        
        self.navigationController!.present(alertController, animated: true, completion: nil)
        
    }
    
    func showSucessWithAction(message: String) {
        //        let alert = OpinionzAlertView(title: "Congratulations", message: message, cancelButtonTitle: "OK", otherButtonTitles: nil)
        let alert = OpinionzAlertView(title: "Congratulations", message: message, cancelButtonTitle: "OK", otherButtonTitles: nil) { (alertView, buttonIndex) in
            self.navigationController?.popToRootViewController(animated: false)
        }
        alert?.iconType = OpinionzAlertIconSuccess
        alert?.color = UIColor(red: 0.15, green: 0.68, blue: 0.38, alpha: 1)
        alert?.show()
    }
    
  
    
    @IBAction func btnSubmitClicked(_ sender: Any) {
        
        
        if (AppUtilites.sharedInstance.isValidEmail(string: self.txtEmail.text!) == true){
            
            let user = AppUtilites.sharedInstance.getUserData()
            
            let params = [
                "user_id": user.user_id,
                "name": self.txtName.text,
                "email": txtEmail.text,
                "city": txtCity.text,
                "state": txtState.text,
                "rigional_manange_team": txtRegionalManagerTeam.text,
                "notes": txtViewNotes.text,
                "types_of_list_request": txtTypeOfListRequest.text
                ] as [String : Any]
            
            APIClient.showProgress()
            
            if(self.isImageSelected == true) {
                
                APIClient.sharedInstance.postImageToServerForLead(BASE_URL.appending(k_lead_Request) as NSString, image: self.imageselected, parameters: params as NSDictionary!) { (response, error) in
                    
                    //        APIClient.sharedInstance.MakeAPICallWithEndURl(BASE_URL.appending(k_lead_Request) as NSString, parameters: params as NSDictionary!) { (response, error) in
                    
                    APIClient.dismissProgress()
                    
                    if (error == nil) {
                        // code of responce...
                        let resutls = response! as NSDictionary
                        print(resutls)
                        let status : Int = resutls.value(forKey: "status") as! Int
                        if(status == 1) {
                            
                            let successMsg = resutls.value(forKey: "msg") as! String
                            
                            
//                            AppUtilites.showSucess(message: successMsg)
                             self.showSucessWithAction(message: "Leads request submitted")
                            
                        }else{
                            let error = resutls.value(forKey: "msg") as! String
                            AppUtilites.showError(title: "Error",message: error, cancelButtonTitle:"OK")
                        }
                    }else{
                        AppUtilites.showError(title: "Oops...",message: "Server timed out!", cancelButtonTitle:"OK")
                        
                    }
                }
            }
            else{
                
                APIClient.sharedInstance.MakeAPICallWithEndURl(BASE_URL.appending(k_lead_Request) as NSString, parameters: params as NSDictionary!) { (response, error) in
                    
                    APIClient.dismissProgress()
                    
                    if (error == nil) {
                        // code of responce...
                        let resutls = response! as NSDictionary
                        print(resutls)
                        let status : Int = resutls.value(forKey: "status") as! Int
                        if(status == 1) {
                            
                            let successMsg = resutls.value(forKey: "msg") as! String
                            
                            self.showSucessWithAction(message: "Leads request submitted")
//                            AppUtilites.showSucess(message: successMsg)
                            
                        }else{
                            
                            let error = resutls.value(forKey: "msg") as! String
                            AppUtilites.showError(title: "Error",message: error, cancelButtonTitle:"OK")
                        }
                    }else{
                        AppUtilites.showError(title: "Oops...",message: "Server timed out!", cancelButtonTitle:"OK")
                        
                    }
                }
            }
        }
        else{
            
            AppUtilites.showError(title: "Error", message: "Please enter valid emailid.", cancelButtonTitle: "Ok")
        }
    }
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients([AppUtilites.sharedInstance.teamLeadEmail])
        mailComposerVC.setSubject("CIG")
      mailComposerVC.setMessageBody(k_email_declaimer, isHTML: true)
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        
        let alert = AppUtilites.SimpleAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.")
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: MFMailComposeViewControllerDelegate
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
  
    @IBAction func btnEmailClicked(_ sender: Any) {
        
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
        
    }
    
    @IBAction func btnHelpClicked(_ sender: Any) {
        
        let link =  "https://cig.on.spiceworks.com/portal/tickets"
        if(link.characters.count > 0){
            
            let safariVC = SFSafariViewController(url: NSURL(string: link)! as URL)
            self.present(safariVC, animated: true, completion: nil)
            safariVC.delegate = self
        }
    }
    
    func OpenImagePicker(openWithCamera: Bool) {
        
        imagePicker.allowsEditing = false
        
        if(openWithCamera)
        {
            if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
                
                imagePicker.sourceType = .camera
            }
            else{
                
                AppUtilites.showInfo(message: "Camera not available")
                return;
            }
        }
        else{
            
            imagePicker.sourceType = .photoLibrary
        }
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
           
           imageselected = pickedImage
            self.isImageSelected = true
            if let imageURL = info[UIImagePickerControllerReferenceURL] as? URL {
                let result = PHAsset.fetchAssets(withALAssetURLs: [imageURL], options: nil)
                let asset = result.firstObject
             //   print(asset?.value(forKey: "filename"))
                self.lblfileName.text = asset?.value(forKey: "filename") as? String
                
            }
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        self.isImageSelected = false
        dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
