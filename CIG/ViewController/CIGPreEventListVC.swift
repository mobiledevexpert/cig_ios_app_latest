//
//  CIGPreEventListVC.swift
//  CIG
//
//  Created by Harry on 4/6/18.
//  Copyright © 2018 Harry. All rights reserved.
//

import UIKit
import SafariServices
import MessageUI
import EventKit
import EventKitUI

class CIGPreEventListVC: UIViewController,UITableViewDelegate,UITableViewDataSource,SFSafariViewControllerDelegate,MFMailComposeViewControllerDelegate {
    
    
    @IBOutlet var eventTableViewHeightConstraint: NSLayoutConstraint!
    
    
    @IBOutlet var tableview: UITableView!
    
    @IBOutlet weak var btnHelpDeak: UIButton!
    @IBOutlet weak var btnCallNow: UIButton!
    @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!
    
    var arrEvents = NSMutableArray()
    var gradeientForHeader : CAGradientLayer!
    var gradeientForProgressView : CAGradientLayer!
    var gradeientForHelpDeskButton : CAGradientLayer!
    var gradeientForbtnCallNow : CAGradientLayer!
    
    var viewMoreIndex = Int()
    let store = EKEventStore()
    var estimatedHeight = CGFloat()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (UIScreen.main.nativeBounds.height == 2436) {
            self.headerViewHeightConstraint.constant = self.headerViewHeightConstraint.constant + 20
            estimatedHeight = self.view.frame.size.height - 405
        }
        else {
            estimatedHeight = self.view.frame.size.height - 352
        }
        
        self.tableview.delegate = self
        self.tableview.dataSource = self
        self.tableview.rowHeight = UITableViewAutomaticDimension
        self.tableview.estimatedRowHeight = 100
        
        
        //        self.imgProfile.setIndicatorStyle(UIActivityIndicatorViewStyle.gray)
        //        self.imgProfile.setShowActivityIndicator(true)
        //        self.imgProfile.sd_setImage(with: URL(string: user.avatar_image))
        
        //        self.imgProfile.SetRoundImageWithoutBorder(image: imgProfile)
        //        self.imgView.SetRoundView(view: imgView)
        //        self.imgView.SetViewShadow(view: imgView)
        //        btnHelpDeak.layoutIfNeeded()
        //        btnCallNow.layoutIfNeeded()
        //        headerView.layoutIfNeeded()
        //        progressView.layoutIfNeeded()
        //        progressViewCon.constant = AppUtilites.sharedInstance.getWidthFromPercentage(percentage: 75)
        
        
        
        self.viewMoreIndex = -1000
        self.slideMenuController()?.removeLeftGestures()
        self.slideMenuController()?.removeRightGestures()
        self.slideMenuController()?.addLeftGestures()
        self.slideMenuController()?.addRightGestures()
        let screenSize: CGRect = UIScreen.main.bounds
        self.slideMenuController()?.changeLeftViewWidth(screenSize.width - 20);
        self.slideMenuController()?.changeRightViewWidth(screenSize.width - 20)
        
        
        tableview.delegate = self
        
        
        self.getEventList()
        
        btnHelpDeak.layoutIfNeeded()
        btnCallNow.layoutIfNeeded()
        
        
        gradeientForHelpDeskButton = AppUtilites.sharedInstance.GetGreenGradient(self.view.frame.size.width/2, self.btnHelpDeak.frame.size.height)
        btnHelpDeak.layer.insertSublayer(gradeientForHelpDeskButton, at: 0)
        
        gradeientForbtnCallNow = AppUtilites.sharedInstance.GetBlueGradient(self.view.frame.size.width/2, self.btnCallNow.frame.size.height)
        btnCallNow.layer.insertSublayer(gradeientForbtnCallNow, at: 0)
        
        NotificationCenter.default.addObserver(self, selector: #selector(sortallrecordsbyzipcode), name: NSNotification.Name(rawValue: "UpdateLocationNotification"), object: nil)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return  self.arrEvents.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let dic = self.arrEvents[indexPath.row] as! NSDictionary
        
        if(indexPath.row == self.viewMoreIndex){
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "CIGEventDetailTC") as! CIGEventDetailTC
            cell.lblTitle.text = dic.value(forKey: "title") as? String
            
            let startDate = dic.value(forKey: "from_date") as! String
            let endDate = dic.value(forKey: "to_date") as! String
            
            let arrDateComponant = endDate.components(separatedBy: " ")
            if(arrDateComponant.count > 1){
                cell.lblDateTime.text = "\(startDate) - \(arrDateComponant[4] )"
            }
            else{
                cell.lblDateTime.text = "\(startDate)"
            }
            cell.btnViewMore.tag = indexPath.row
            
            cell.lblLocation.text = dic.value(forKey: "address") as? String
            cell.lblDescription.text = dic.value(forKey: "desc") as? String
            cell.btnViewMore.setTitle("View Less", for: UIControlState.normal)
            cell.btnSavetoCalender.tag = indexPath.row
            
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CIGEventTC") as! CIGEventTC
        cell.lblTitle.text = dic.value(forKey: "title") as? String
        
        let startDate = dic.value(forKey: "from_date") as! String
        let endDate = dic.value(forKey: "to_date") as! String
        cell.btnViewMore.tag = indexPath.row
        
        let arrDateComponant = endDate.components(separatedBy: " ")
        if(arrDateComponant.count > 1){
            cell.lblDateTime.text = "\(startDate) - \(arrDateComponant[4] )"
        }
        else{
            cell.lblDateTime.text = "\(startDate)"
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if(indexPath.row == self.viewMoreIndex){
            
            let dic = self.arrEvents[indexPath.row] as! NSDictionary
            let font = UIFont(name: "Verdana", size: 15)
            
            if(UIDevice.current.userInterfaceIdiom == .pad) {
                
                return (100 + heightForLabel(text:  (dic.value(forKey: "desc") as? String)!, font: font!, width: self.view.frame.size.width - 30))
                
            }
            else{
                return (85 + heightForLabel(text:  (dic.value(forKey: "desc") as? String)!, font: font!, width: self.view.frame.size.width - 30))
            }
            
        }
        return 65
        
    }
    
    func heightForLabel(text:String, font:UIFont, width:CGFloat) -> CGFloat
    {
        let label:UILabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        //UILabel(frame: rect (0, 0, width, CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        
        label.sizeToFit()
        return label.frame.height
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients(["help@copelandgroupusa.com"])
        mailComposerVC.setSubject("CIG")
        mailComposerVC.setMessageBody(k_email_declaimer, isHTML: true)
        
        return mailComposerVC
    }
    
    @IBAction func LoginClick(_ sender: Any) {
        var loginViewController =  AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGLoginVC") as! CIGLoginVC
        self.navigationController?.pushViewController(loginViewController, animated: true)
    }
    
    @IBAction func onSavetoCalenderClick(_ sender: Any) {
        
        //        DispatchQueue.main.async {
        
        self.store.requestAccess(to: EKEntityType.event) { (granted,error ) in
            
            let dic = self.arrEvents[(sender as! UIButton).tag] as! NSDictionary
            
            if (granted == true){
                
                
                
                let formatter : DateFormatter = DateFormatter()
                formatter.dateFormat = "MMMM dd, yyyy hh:mma"
                
                print(dic.value(forKey: "from_date") as! String)
                
                let startDate = formatter.date(from: dic.value(forKey: "from_date") as! String)!
                
                print(startDate)
                var endDate = Date()
                if (dic.value(forKey: "to_date") as! String).characters.count > 0 {
                    
                    endDate = formatter.date(from: dic.value(forKey: "to_date") as! String)!
                }
                else{
                    
                    endDate = startDate.addingTimeInterval(60*60)
                }
                
                
                
                self.addEventToCalendar(title:(dic.value(forKey: "title") as? String)! ,
                                        description: "\(dic.value(forKey: "location") as! String) \(dic.value(forKey: "address") as! String)",
                    startDate: startDate,
                    endDate:  endDate)
            }
        }
        //        }
    }
    
    
    func addEventToCalendar(title: String, description: String?, startDate: Date, endDate: Date, completion: ((_ success: Bool, _ error: NSError?) -> Void)? = nil) {
        let eventStore = EKEventStore()
        
        eventStore.requestAccess(to: .event, completion: { (granted, error) in
            if (granted) && (error == nil) {
                let event = EKEvent(eventStore: eventStore)
                event.title = title
                event.startDate = startDate
                event.endDate = endDate
                event.notes = description
                event.calendar = eventStore.defaultCalendarForNewEvents
                do {
                    try eventStore.save(event, span: .thisEvent)
                } catch let e as NSError {
                    completion?(false, e)
                    return
                }
                
                DispatchQueue.main.async {
                    AppUtilites.showSucess(message: "Event Saved Successfully.")
                }
                completion?(true, nil)
            } else {
                completion?(false, error as NSError?)
            }
        })
    }
    func showSendMailErrorAlert() {
        
        let alert = AppUtilites.SimpleAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.")
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: MFMailComposeViewControllerDelegate
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func btnEmailUs(_ sender: Any) {
        
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
        
    }
    
    // MARK: - Button Delegate
    
    @IBAction func clkBtnBackEventTraining(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnHelpDesk(_ sender: Any) {
        
        if let url = URL(string: "tel://1-800-220-8899") {
            UIApplication.shared.open(url)
        }
        
//        let link =  "https://cig.on.spiceworks.com/portal/tickets"
//        if(link.characters.count > 0){
//
//            let safariVC = SFSafariViewController(url: NSURL(string: link)! as URL)
//            self.present(safariVC, animated: true, completion: nil)
//            safariVC.delegate = self
//        }
    }
    
    @IBAction func btnViewMoreClicked(_ sender: Any) {
        
        self.viewMoreIndex = (sender as! UIButton).tag
        self.setTableHeight()
        self.tableview.reloadData()
    }
    
    @IBAction func btnViewLessClicked(_ sender: Any) {
        
        self.viewMoreIndex = -1000
        self.setTableHeight()
        self.tableview.reloadData()
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    func setTableHeight(){
        
        if(UIDevice.current.userInterfaceIdiom != .pad) {
            
            if (self.arrEvents.count == 0){
                
                self.eventTableViewHeightConstraint.constant = estimatedHeight
            }
            else {
                
                if (self.viewMoreIndex != -1000){
                    
                    self.eventTableViewHeightConstraint.constant = CGFloat(self.arrEvents.count * 65) + 70.0
                    
                }
                else {
                    self.eventTableViewHeightConstraint.constant = CGFloat(self.arrEvents.count * 65)
                }
                
                if( self.eventTableViewHeightConstraint.constant < estimatedHeight){
                    
                    self.eventTableViewHeightConstraint.constant = estimatedHeight
                }
            }
        }
    }
    func getEventList(){
        
        APIClient.showProgress()
        
        APIClient.sharedInstance.MakeAPICallWithEndURl(BASE_URL.appending(k_event_list) as NSString, parameters: nil ) { (response, error) in
            
            APIClient.dismissProgress()
            
            if (error == nil) {
                // code of responce...
                let resutls = response! as NSDictionary
                print(resutls)
                
                let array = resutls.value(forKey: "event_list") as! NSArray
                self.arrEvents = NSMutableArray(array: resutls.value(forKey: "event_list") as! NSArray)
                //self.arrEvents = NSMutableArray(array: self.arrEvents.reverseObjectEnumerator().allObjects).mutableCopy() as! NSMutableArray
                if (UserDefaults.standard.value(forKey: "currentCity") != nil){
                    
                    let zipcode = UserDefaults.standard.value(forKey: "currentCity") as! String
                    
                    var temparray = NSMutableArray()
                    for dict in self.arrEvents{
                        if (((dict as! NSDictionary).value(forKey: "city")) != nil){
                            let otherzipcode = ((dict as! NSDictionary).value(forKey: "city")) as! String
                            if zipcode.caseInsensitiveCompare(otherzipcode) == ComparisonResult.orderedSame{
                                temparray.add(dict)
                            }
                        }
                    }
                    if temparray.count > 0{
                        self.arrEvents = temparray
                    }
                    self.setTableHeight()
                    self.tableview.reloadData()
                    
                    //self.sortallrecordsbyzipcode()
                }else{
                    self.setTableHeight()
                    self.tableview.reloadData()
                }
                
//                self.setTableHeight()
//                self.tableview.reloadData()
                
                
            }else{
                AppUtilites.showError(title: "Oops...",message: "Server timed out!", cancelButtonTitle:"OK")
                
            }
        }
    }
    
    func sortallrecordsbyzipcode(){
        
        let zipcode = UserDefaults.standard.value(forKey: "currentCity") as! String
        
        var temparray = NSMutableArray()
        for dict in self.arrEvents{
            if (((dict as! NSDictionary).value(forKey: "city")) != nil){
                let otherzipcode = ((dict as! NSDictionary).value(forKey: "city")) as! String
                if zipcode.caseInsensitiveCompare(otherzipcode) == ComparisonResult.orderedSame{
                    temparray.add(dict)
                }
            }
        }
        if temparray.count > 0{
            self.arrEvents = temparray
        }
        self.setTableHeight()
        self.tableview.reloadData()
        
//        let latitude = UserDefaults.standard.double(forKey: "savedlatitude")
//        print(latitude)
//
//        let longitude = UserDefaults.standard.double(forKey: "savedlongitude")
//        print(longitude)
//        //var currentlocation = CLLocation()
//
//        let currentlocation = CLLocation(latitude: latitude, longitude: longitude)
//        CLGeocoder().reverseGeocodeLocation(currentlocation) { (placemarks, error) in
//            if((error) != nil){
//                self.setTableHeight()
//                self.tableview.reloadData()
//                return;
//            }
//            if let placemarkcount = placemarks?.count
//            {
//                if placemarkcount > 0{
//                    let pm = placemarks![0] as! CLPlacemark
//                    if ((pm.postalCode) != nil){
//                        let zipcode = pm.postalCode
//
//                        var temparray = NSMutableArray()
//                        for dict in self.arrEvents{
//                            if (((dict as! NSDictionary).value(forKey: "zip")) != nil){
//                                let otherzipcode = ((dict as! NSDictionary).value(forKey: "zip")) as! String
//                                if zipcode == otherzipcode{
//                                    temparray.add(dict)
//                                }
//                            }
//                        }
//                        if temparray.count > 0{
//                            self.arrEvents = temparray
//                        }
//                        self.setTableHeight()
//                        self.tableview.reloadData()
//
//                        //print(zipcode)
//                    }else{
//                        self.setTableHeight()
//                        self.tableview.reloadData()
//                    }
//                }else{
//                    self.setTableHeight()
//                    self.tableview.reloadData()
//                }
//            }
//        }
        
    }
}
