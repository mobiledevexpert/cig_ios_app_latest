//
//  CIGLeftSideMenuVC.swift
//  CIG
//
//  Created by Harry on 10/27/17.
//  Copyright © 2017 Harry. All rights reserved.
//

import UIKit
import SafariServices

class CIGLeftSideMenuVC: UIViewController,UITableViewDelegate,UITableViewDataSource,SFSafariViewControllerDelegate {

    var titleArray = NSMutableArray()
    var imgArray = NSMutableArray()
    
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var imgProfile: UIImageView!
    @IBOutlet var bottomView: UIView!
    @IBOutlet var topView: UIView!
    @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if (UIScreen.main.nativeBounds.height == 2436) {
            self.headerViewHeightConstraint.constant = self.headerViewHeightConstraint.constant + 25
        }

        
        let user = AppUtilites.sharedInstance.getUserData()
        print(user.display_name)
        print(user.avatar_image)
        
        self.lblUserName.text = user.display_name
        self.imgProfile.setIndicatorStyle(UIActivityIndicatorViewStyle.gray)
        self.imgProfile.setShowActivityIndicator(true)
        self.imgProfile.sd_setImage(with: URL(string: user.avatar_image))
        self.imgProfile.SetRoundImageWithoutBorder(image: imgProfile)
        
         self.topView.layer.shadowOffset = CGSize(width: 0, height: 3)
        
        titleArray = ["Home","Online Quoting","Carrier Alerts","Events & Training","Copeland Portal","Copeland Contacts","Leads"]
        imgArray = ["home-button-square","online-quoting-button-square","carrier-alerts-button-square","events-training-button-square","copeland-portal-button-square","copeland-contacts-button-square","leads-button-square"]
        
        // Do any additional setup after loading the view.
    }

    
    
    @IBAction func btnMyProfile(_ sender: UIButton) {
        
//        slideMenuController()?.closeLeft()
//        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let newViewController = storyBoard.instantiateViewController(withIdentifier: "CIGMyProfileVC") as! CIGMyProfileVC
//        self.present(newViewController, animated: true, completion: nil)
        
        slideMenuController()?.closeLeft()
        
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGMyProfileVC") as! CIGMyProfileVC
        self.navigationController?.pushViewController(controller, animated: true)
        
        
    }
    @IBAction func onCloseClicked(_ sender: Any) {
        
        self.closeLeft()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func btnSettingsClicked(_ sender: Any) {
        
        let settingsVC : CIGSettingsViewController = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGSettingsViewController") as! CIGSettingsViewController
        
        let leftSideMenuVC  = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGLeftSideMenuVC") as? CIGLeftSideMenuVC
        
        let rightSideMenuVC  = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGRightSideMenuVC") as? CIGRightSideMenuVC
        
        let slideMenuController = SlideMenuController(mainViewController: settingsVC, leftMenuViewController: leftSideMenuVC!, rightMenuViewController: rightSideMenuVC!)
        let nav = UINavigationController(rootViewController: slideMenuController)
        nav.navigationBar.isHidden = true
        self.navigationController?.pushViewController(slideMenuController, animated: true)
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CIGHomeTC", for: indexPath) as! CIGHomeTC
        
        cell.imgType.image = UIImage(named: imgArray[indexPath.row] as! String)
        cell.lblTitle.text = titleArray[indexPath.row] as? String
    
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.closeLeft()
        
        if(indexPath.row == 0) {
            
            let homeVC : CIGHomeVC = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGHomeVC") as! CIGHomeVC
            
            let leftSideMenuVC  = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGLeftSideMenuVC") as? CIGLeftSideMenuVC
            
            let rightSideMenuVC  = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGRightSideMenuVC") as? CIGRightSideMenuVC
            
            let slideMenuController = SlideMenuController(mainViewController: homeVC, leftMenuViewController: leftSideMenuVC!, rightMenuViewController: rightSideMenuVC!)
            let nav = UINavigationController(rootViewController: slideMenuController)
            nav.navigationBar.isHidden = true
            self.navigationController?.pushViewController(slideMenuController, animated: true)
        }
        else if(indexPath.row == 1){
            
            let quoteVC : CIGOnlineQuotingVC = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGOnlineQuotingVC") as! CIGOnlineQuotingVC
            
            let leftSideMenuVC  = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGLeftSideMenuVC") as? CIGLeftSideMenuVC
            
            let rightSideMenuVC  = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGRightSideMenuVC") as? CIGRightSideMenuVC
            
            let slideMenuController = SlideMenuController(mainViewController: quoteVC, leftMenuViewController: leftSideMenuVC!, rightMenuViewController: rightSideMenuVC!)
            let nav = UINavigationController(rootViewController: slideMenuController)
            nav.navigationBar.isHidden = true
            self.navigationController?.pushViewController(slideMenuController, animated: true)
            
        }
        else if(indexPath.row == 2) {
            
            let carrierAlertVC : CIGCarrierAlertVC = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGCarrierAlertVC") as! CIGCarrierAlertVC
            
            let leftSideMenuVC  = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGLeftSideMenuVC") as? CIGLeftSideMenuVC
            
            let rightSideMenuVC  = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGRightSideMenuVC") as? CIGRightSideMenuVC
            
            let slideMenuController = SlideMenuController(mainViewController: carrierAlertVC, leftMenuViewController: leftSideMenuVC!, rightMenuViewController: rightSideMenuVC!)
            let nav = UINavigationController(rootViewController: slideMenuController)
            nav.navigationBar.isHidden = true
            self.navigationController?.pushViewController(slideMenuController, animated: true)
        }
        else if(indexPath.row == 3) {
            
            let eventTraingVC : CIGEventTraingVC = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGEventTraingVC") as! CIGEventTraingVC
            let leftSideMenuVC  = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGLeftSideMenuVC") as? CIGLeftSideMenuVC
            
            let rightSideMenuVC  = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGRightSideMenuVC") as? CIGRightSideMenuVC
            
            let slideMenuController = SlideMenuController(mainViewController: eventTraingVC, leftMenuViewController: leftSideMenuVC!, rightMenuViewController: rightSideMenuVC!)
            let nav = UINavigationController(rootViewController: slideMenuController)
            nav.navigationBar.isHidden = true
            self.navigationController?.pushViewController(slideMenuController, animated: true)
            
        }
        else if(indexPath.row == 4) {
            let safariVC = SFSafariViewController(url: NSURL(string: "https://www.copelandgroupusa.com/login")! as URL)
            self.present(safariVC, animated: true, completion: nil)
            safariVC.delegate = self
        }
        else if(indexPath.row == 5) {
            
            let eventTraingVC : CIGCopelandContactsVC = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGCopelandContactsVC") as! CIGCopelandContactsVC
            let leftSideMenuVC  = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGLeftSideMenuVC") as? CIGLeftSideMenuVC
            
            let rightSideMenuVC  = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGRightSideMenuVC") as? CIGRightSideMenuVC
            
            let slideMenuController = SlideMenuController(mainViewController: eventTraingVC, leftMenuViewController: leftSideMenuVC!, rightMenuViewController: rightSideMenuVC!)
            let nav = UINavigationController(rootViewController: slideMenuController)
            nav.navigationBar.isHidden = true
            self.navigationController?.pushViewController(slideMenuController, animated: true)
            
        }
        else if(indexPath.row == 6) {
            
            let eventTraingVC : CIGLeadsVC = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGLeadsVC") as! CIGLeadsVC
            let leftSideMenuVC  = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGLeftSideMenuVC") as? CIGLeftSideMenuVC
            
            let rightSideMenuVC  = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGRightSideMenuVC") as? CIGRightSideMenuVC
            
            let slideMenuController = SlideMenuController(mainViewController: eventTraingVC, leftMenuViewController: leftSideMenuVC!, rightMenuViewController: rightSideMenuVC!)
            let nav = UINavigationController(rootViewController: slideMenuController)
            nav.navigationBar.isHidden = true
            self.navigationController?.pushViewController(slideMenuController, animated: true)
            
        }
}

    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
}
