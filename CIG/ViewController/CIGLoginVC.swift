//
//  CIGLoginVC.swift
//  CIG
//
//  Created by Harry on 10/24/17.
//  Copyright © 2017 Harry. All rights reserved.
//

import UIKit
import SafariServices
import MessageUI
import EventKit
import EventKitUI

class CIGLoginVC: UIViewController,UITextFieldDelegate,SFSafariViewControllerDelegate,UITableViewDelegate,UITableViewDataSource,MFMailComposeViewControllerDelegate  {
    
    
    @IBOutlet weak var blurView: UIView!
    @IBOutlet weak var PasswordResetView3: UIView!
    @IBOutlet weak var btn_OkPasswordReset: UIButton!
    @IBOutlet weak var PasswordResetView1: UIView!
    @IBOutlet weak var PasswordResetView2: UIView!
    @IBOutlet weak var txtNewPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    @IBOutlet weak var txtVerifyCodeForgotPassword: UITextField!
    @IBOutlet weak var btnSendForgotPassword: UIButton!
    @IBOutlet weak var txtEmailForgotPassword: UITextField!
    @IBOutlet weak var forgotPasswordView: UIView!
    @IBOutlet var btnForgot: UIButton!
    @IBOutlet var btnCallNow: UIButton!
    @IBOutlet var btnHelpDeak: UIButton!
    @IBOutlet var imgLogo: UIImageView!
    @IBOutlet var headerView: UIView!
    @IBOutlet var txt_Password: UITextField!
    @IBOutlet var txt_Email: UITextField!
    @IBOutlet var imgTriangleEvents: UIImageView!
    @IBOutlet var imgTriangleSign: UIImageView!
    @IBOutlet var ImgTriangle: UIImageView!
    @IBOutlet var btnSignin: UIButton!
    @IBOutlet var btnContinue: UIButton!
    @IBOutlet var LblMsg: UILabel!
    @IBOutlet var msgView: UIView!
    @IBOutlet var btncheck: UIButton!
    @IBOutlet var viewMiddle: UIView!
    
    @IBOutlet weak var btnGoPasswordReset: UIButton!
    @IBOutlet weak var btn_Submit: UIButton!
    
    var tapGesture = UITapGestureRecognizer()
    
    var greengradeientForSendButton : CAGradientLayer!
    var gradeientForSignIN : CAGradientLayer!
    var gradeientForHelpDeskButton : CAGradientLayer!
    var gradeientForheaderView : CAGradientLayer!
    var gradeientForbtnContinue : CAGradientLayer!
    var gradeientForbtnCallNow : CAGradientLayer!
    
    var gradeientForbtnOk : CAGradientLayer!
    var gradeientForbtnSubmit : CAGradientLayer!
    var gradeientForbtnSend : CAGradientLayer!
    var gradeientForbtnGo : CAGradientLayer!
    
    var user_id_ForgotPassword: String!
    
    var params = NSDictionary()
    
    //@IBOutlet var eventTableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var tableview: UITableView!
    
    var arrEvents = NSMutableArray()
    var arrNearby = NSMutableArray()
    var arrNotNearBy = NSMutableArray()
    var gradeientForHeader : CAGradientLayer!
    var gradeientForProgressView : CAGradientLayer!
    
    var viewMoreIndex = Int()
    var selectedSectionIndex = Int()
    let store = EKEventStore()
    var estimatedHeight = CGFloat()
    
    fileprivate func setupViewatStartUP() {
        LblMsg.text = "Providing the BEST Data & Technology in Healthcare"
        imgTriangleEvents.isHidden = false
        imgTriangleSign.isHidden = true
        ImgTriangle.isHidden = true
        btnSignin.isHidden = false
        btnContinue.isHidden = true
        msgView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        msgView.layer.shadowOffset = CGSize(width:0.0,height: 0.6)
        msgView.layer.shadowOpacity = 0.8
        msgView.layer.shadowRadius = 0.0
        msgView.layer.masksToBounds = false
        
//        self.blurView.backgroundColor = UIColor(red: 242, green: 242, blue: 242, alpha: 0.01)
        
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = blurView.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        blurView.addSubview(blurEffectView)
    }
    
    @IBAction func btnRememberMeClicked(_ sender: Any) {
        
        if self.btncheck.tag == 1 {
            
            self.btncheck.setBackgroundImage(UIImage(named: "blank-square.png"), for: UIControlState.normal)
            self.btncheck.tag = 0
            UserDefaults.standard.set("", forKey: "kUserEmail")
            UserDefaults.standard.set("", forKey: "kUserPassword")
            UserDefaults.standard.set("0", forKey: "kIsRememberMe")
        }
        else{
            
            self.btncheck.tag = 1
            self.btncheck.setBackgroundImage(UIImage(named: "tick.png"), for: UIControlState.normal)
            
            UserDefaults.standard.set(self.txt_Email.text, forKey: "kUserEmail")
            UserDefaults.standard.set(self.txt_Password.text, forKey: "kUserPassword")
            UserDefaults.standard.set("1", forKey: "kIsRememberMe")
            
        }
        
    }
    
    fileprivate func setGradient() {
        greengradeientForSendButton = AppUtilites.sharedInstance.GetGreenGradient(self.btnSendForgotPassword.frame.size.width, self.btnSendForgotPassword.frame.size.height)
        
        
        btnSendForgotPassword.layer.insertSublayer(greengradeientForSendButton, at: 0)
        
        gradeientForheaderView = AppUtilites.sharedInstance.GetRedGradient(self.headerView.frame.size.width, self.headerView.frame.size.height)
        headerView.layer.insertSublayer(gradeientForheaderView, at: 0)
        
        gradeientForbtnContinue = AppUtilites.sharedInstance.GetRedGradient(self.btnContinue.frame.size.width, self.btnContinue.frame.size.height)
        btnContinue.layer.insertSublayer(gradeientForbtnContinue, at: 0)
        
        gradeientForSignIN = AppUtilites.sharedInstance.GetRedGradient(self.btnSignin.frame.size.width, self.btnSignin.frame.size.height)
        btnSignin.layer.insertSublayer(gradeientForSignIN, at: 0)
        
        gradeientForHelpDeskButton = AppUtilites.sharedInstance.GetGreenGradient(self.view.frame.size.width/2, self.btnHelpDeak.frame.size.height)
        btnHelpDeak.layer.insertSublayer(gradeientForHelpDeskButton, at: 0)
        
         gradeientForbtnCallNow = AppUtilites.sharedInstance.GetBlueGradient(self.view.frame.size.width/2, self.btnCallNow.frame.size.height)
        btnCallNow.layer.insertSublayer(gradeientForbtnCallNow, at: 0)
        
        gradeientForbtnOk = AppUtilites.sharedInstance.GetGreenGradient(self.btn_OkPasswordReset.frame.size.width, self.btn_OkPasswordReset.frame.size.height)
        btn_OkPasswordReset.layer.insertSublayer(gradeientForbtnOk, at: 0)
        
        gradeientForbtnGo = AppUtilites.sharedInstance.GetGreenGradient(self.btnGoPasswordReset.frame.size.width, self.btnGoPasswordReset.frame.size.height)
        btnGoPasswordReset.layer.insertSublayer(gradeientForbtnGo, at: 0)
        
        gradeientForbtnSend = AppUtilites.sharedInstance.GetGreenGradient(self.btnSendForgotPassword.frame.size.width, self.btnSendForgotPassword.frame.size.height)
        btnSendForgotPassword.layer.insertSublayer(gradeientForbtnSend, at: 0)
        
        gradeientForbtnSubmit = AppUtilites.sharedInstance.GetGreenGradient(self.btn_Submit.frame.size.width, self.btn_Submit.frame.size.height)
        btn_Submit.layer.insertSublayer(gradeientForbtnSubmit, at: 0)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnHelpDeak.setTitle("Email Us", for: .normal)
        
        if (UIScreen.main.nativeBounds.height == 2436) {
            estimatedHeight = self.view.frame.size.height - 405
        }
        else {
            estimatedHeight = self.view.frame.size.height - 352
        }
        
        self.tableview.delegate = self
        self.tableview.dataSource = self
        self.tableview.rowHeight = UITableViewAutomaticDimension
        self.tableview.estimatedRowHeight = 100
        self.tableview.tableFooterView = UIView()
        
        self.viewMoreIndex = -1000
        self.selectedSectionIndex = -1000
        self.getEventList()
        
        viewMiddle.isHidden = true
        msgView.isHidden = true
        tableview.isHidden = false
        txt_Password.delegate = self
        txt_Email.delegate = self
        self.hideKeyboard()
        
        setupViewatStartUP()
        
        if let IsRememberMe = UserDefaults.standard.string(forKey: "kIsRememberMe"){
            
            if(IsRememberMe == "1"){
                
                self.txt_Email.text = UserDefaults.standard.value(forKey: "kUserEmail") as? String
                self.txt_Password.text = UserDefaults.standard.value(forKey: "kUserPassword") as? String
            }
            else {
                self.txt_Email.text = ""
                self.txt_Password.text = ""
            }
        }
       

        setGradient()
        
        #if (arch(i386) || arch(x86_64)) && os(iOS)
            // If Simulator
             txt_Email.text = "harry002@mailinator.com"
             txt_Password.text = "harry002"
        #endif
        
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.myviewTapped(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        blurView.addGestureRecognizer(tapGesture)
        blurView.isUserInteractionEnabled = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(sortallrecordsbyzipcode), name: NSNotification.Name(rawValue: "UpdateLocationNotification"), object: nil)
    }
    
    func myviewTapped(_ sender: UITapGestureRecognizer) {
        
        blurView.isHidden = true
        forgotPasswordView.isHidden = true
        PasswordResetView1.isHidden = true
        PasswordResetView2.isHidden = true
        PasswordResetView3.isHidden = true
    }
    
    
    override func viewDidLayoutSubviews() {
       
        self.greengradeientForSendButton.frame = self.btnSendForgotPassword.bounds;
        self.gradeientForbtnCallNow.frame = self.btnCallNow.bounds
        self.gradeientForHelpDeskButton.frame = self.btnHelpDeak.bounds
        self.gradeientForSignIN.frame = self.btnSignin.bounds
        self.gradeientForbtnContinue.frame = self.btnContinue.bounds
        self.gradeientForheaderView.frame = self.headerView.bounds
        
        self.gradeientForbtnSubmit.frame = self.btn_Submit.bounds
        self.gradeientForbtnSend.frame = self.btnSendForgotPassword.bounds
        self.gradeientForbtnOk.frame = self.btn_OkPasswordReset.bounds
        self.gradeientForbtnGo.frame = self.btnGoPasswordReset.bounds
    
    }
  
    
    
    
    //MARK:-  API Calling
    
    
    func ForgotPasswordAPICall(param : NSDictionary) {
        
        APIClient.showProgress()
        APIClient.sharedInstance.MakeAPICallWithEndURl(BASE_URL.appending(k_ForgotPassword) as NSString, parameters: param as NSDictionary!) { (response, error) in
            
            APIClient.dismissProgress()
            
            if (error == nil) {
                // code of responce...
                let resutls = response! as NSDictionary
                print(resutls)
                let status : Int = resutls.value(forKey: "status") as! Int
                if(status == 1) {
                    
                    DispatchQueue.main.async {
                        self.forgotPasswordView.isHidden = true
                        self.PasswordResetView1.isHidden = false
                        
                    }
                    
//                    UserDefaults.standard.set(true, forKey: "isLogin")
//                    UserDefaults.standard.set((resutls.value(forKey: "user") as! NSDictionary).value(forKey: "user_id"), forKey: "UserId")
//
//                    let homeVC  = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGHomeVC") as! CIGHomeVC
//                    self.navigationController?.pushViewController(homeVC, animated: true)
                    
                }else{
                    let error = resutls.value(forKey: "msg") as! String
                    
                    AppUtilites.showError(title: "Error",message: error, cancelButtonTitle:"OK")
                }
            }else{
                AppUtilites.showError(title: "Oops...",message: "Server timed out!", cancelButtonTitle:"OK")
                
            }
        }
    }
    
    
    func resetPasswordAPICall(param : NSDictionary) {
        
        APIClient.showProgress()
        APIClient.sharedInstance.MakeAPICallWithEndURl(BASE_URL.appending(k_reset_password) as NSString, parameters: param as NSDictionary!) { (response, error) in
            
            APIClient.dismissProgress()
            
            if (error == nil) {
                // code of responce...
                let resutls = response! as NSDictionary
                print(resutls)
                let status : Int = resutls.value(forKey: "status") as! Int
                if(status == 1) {
                    
                    
                    
                    DispatchQueue.main.async {
                        self.PasswordResetView2.isHidden = true
                        self.PasswordResetView3.isHidden = false
                        
                    }
                    
                }else{
                    let error = resutls.value(forKey: "msg") as! String
                    
                    AppUtilites.showError(title: "Error",message: error, cancelButtonTitle:"OK")
                }
            }else{
                AppUtilites.showError(title: "Oops...",message: "Server timed out!", cancelButtonTitle:"OK")
                
            }
        }
    }
    
    
    func VerifyCodeAPICall(param : NSDictionary) {
        
        APIClient.showProgress()
        APIClient.sharedInstance.MakeAPICallWithEndURl(BASE_URL.appending(k_verify_token) as NSString, parameters: param as NSDictionary!) { (response, error) in
            
            APIClient.dismissProgress()
            
            if (error == nil) {
                // code of responce...
                let resutls = response! as NSDictionary
                print(resutls)
                let status : Int = resutls.value(forKey: "status") as! Int
                if(status == 1) {
                    
                    let user_id : String = resutls.value(forKey: "user_id") as! String
                    self.user_id_ForgotPassword = user_id
                    DispatchQueue.main.async {
                        self.PasswordResetView1.isHidden = true
                        self.PasswordResetView2.isHidden = false
                        
                    }
              
                }else{
                    let error = resutls.value(forKey: "msg") as! String
                    
                    AppUtilites.showError(title: "Error",message: error, cancelButtonTitle:"OK")
                }
            }else{
                AppUtilites.showError(title: "Oops...",message: "Server timed out!", cancelButtonTitle:"OK")
                
            }
        }
    }
    
    
    func loginApiCall(param : NSDictionary) {
        
        APIClient.showProgress()
        APIClient.sharedInstance.MakeAPICallWithEndURl(BASE_URL.appending(loginUrl) as NSString, parameters: param as NSDictionary!) { (response, error) in
            
            APIClient.dismissProgress()
            
            if (error == nil) {
                // code of responce...
                let resutls = response! as NSDictionary
                print(resutls)
                let status : Int = resutls.value(forKey: "status") as! Int
                
                if(status == 1) {
                    
                    let user = resutls.value(forKeyPath: "user") as! NSDictionary
                    print(user)
                    AppUtilites.sharedInstance.saveUserData(data: user)
                    
                    if  ((resutls.value(forKey: "user") as! NSDictionary).value(forKey: "is_complete") as! String) == "1" {
                        
                        UserDefaults.standard.set(true, forKey: "isLogin")
                        
                        UserDefaults.standard.set((resutls.value(forKey: "user") as! NSDictionary).value(forKey: "user_id"), forKey: "UserId")
                        
                        if let token = UserDefaults.standard.value(forKey: "k_DeviceToken") as? String {
                            
                            self.addDeviceToken(token: token , userId: (resutls.value(forKey: "user") as! NSDictionary).value(forKey: "user_id") as! String)
                        }
                        let homeVC  = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGHomeVC") as! CIGHomeVC
                        
                        let leftSideMenuVC  = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGLeftSideMenuVC") as? CIGLeftSideMenuVC
                        
                        let rightSideMenuVC  = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGRightSideMenuVC") as? CIGRightSideMenuVC
                        
                        let slideMenuController = SlideMenuController(mainViewController: homeVC, leftMenuViewController: leftSideMenuVC!, rightMenuViewController: rightSideMenuVC!)
                        let nav = UINavigationController(rootViewController: slideMenuController)
                        nav.navigationBar.isHidden = true
                        let appDel : AppDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDel.window?.rootViewController = nav
                        appDel.window?.makeKeyAndVisible()
                    }
                    else {
                        
                        let goalVC : CIGGoalsVC = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGGoalsVC") as! CIGGoalsVC
                        self.navigationController?.pushViewController(goalVC, animated: true)

                    }
                    
                }else{
                    let error = resutls.value(forKey: "msg") as! String
                    
                    AppUtilites.showError(title: "Error",message: error, cancelButtonTitle:"OK")
                }
            }else{
                AppUtilites.showError(title: "Oops...",message: "Server timed out!", cancelButtonTitle:"OK")
                
            }
        }
    }
    
    func addDeviceToken(token : String ,userId : String){
        
        APIClient.showProgress()
        let param = ["user_id": userId, "device_token": token]
        APIClient.sharedInstance.MakeAPICallWithEndURl(BASE_URL.appending(k_add_token) as NSString, parameters: param as NSDictionary!) { (response, error) in
            
            APIClient.dismissProgress()
            
            if (error == nil) {
                // code of responce...
                let resutls = response! as NSDictionary
                print(resutls)
                
            }else{
                
//                AppUtilites.showError(title: "Oops...",message: "Server timed out!", cancelButtonTitle:"OK")
            }
        }
    }
    
    func signupApiCall(param : NSDictionary) {
        
            
        APIClient.showProgress()
        APIClient.sharedInstance.MakeAPICallWithEndURl(BASE_URL.appending(signupUrl) as NSString, parameters: param as NSDictionary!) { (response, error) in
            
            APIClient.dismissProgress()
            
            if (error == nil) {
                // code of responce...
                let resutls = response! as NSDictionary
                print(resutls)
                let status : Int = resutls.value(forKey: "status") as! Int
                let statuscode : Int = resutls.value(forKey: "statuscode") as! Int
                
                if(status == 1) {
                    
                    // UserDefaults.standard.set(true, forKey: "isLogin")
//                    UserDefaults.standard.set(resutls.value(forKey: "user_id") as! Int, forKey: "UserId")
                    UserDefaults.standard.set(5, forKey: "UserId")

                    let goalVC : CIGGoalsVC = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGGoalsVC") as! CIGGoalsVC
                    self.navigationController?.pushViewController(goalVC, animated: true)
                    
                    
                }else{
                    
                    if (statuscode == 409){
                        
                        self.loginApiCall(param: param)
                    }
                    else{
                        
                        if (statuscode == 403){
                            
                            AppUtilites.showError(title: "Error",message: "Email Id not registred please signup.", cancelButtonTitle:"OK")
                        }
                        else {
                            let error = resutls.value(forKey: "msg") as! String
                            AppUtilites.showError(title: "Error",message: error, cancelButtonTitle:"OK")
                        }
                    }
                }
            }else{
                AppUtilites.showError(title: "Oops...",message: "Server timed out!", cancelButtonTitle:"OK")
                
            }
        }
    }
    
    // MARK: - Button Click
    
    @IBAction func btn_submitPasswordReset(_ sender: Any) {
       
        
        if(txtNewPassword.text == txtConfirmPassword.text && txtConfirmPassword.text != nil)
        {
            let password = self.txtNewPassword.text
            
            params = [  "password":password!  ,"user_id": user_id_ForgotPassword ]
            resetPasswordAPICall(param: params)
        }else
        {
              AppUtilites.showError(title: "Hold On...",message: "Entered new password does not matches with confirm password.", cancelButtonTitle:"OK")
        }
        
        
        
        
    }
    @IBAction func btn_SendForgotPassword(_ sender: Any) {
       
        
        let email = self.txtEmailForgotPassword.text
        
        
        if(email != nil && AppUtilites.sharedInstance.isValidEmail(string: email!))
        {
            params = [  "email":email!   ]
        
        self.ForgotPasswordAPICall(param: params)
        }else{
             AppUtilites.showError(title: "Hold On...",message: "Enter Email before continuing!", cancelButtonTitle:"OK")
        }
        
    }
    
    
    
    @IBAction func btn_ForgotPassword(_ sender: Any) {
        
       self.blurView.isHidden = false
       self.forgotPasswordView.isHidden = false
        
    }
    
    @IBAction func btn_CallContinue(_ sender: UIButton) {
        print("btn_CallContinue")
        
        
//                let goalVC : CIGGoalsVC = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGGoalsVC") as! CIGGoalsVC
//                self.navigationController?.pushViewController(goalVC, animated: true)
        
        //
        
        //        view.endEditing(true)
        //        if(self.validation() == true) {
        //
        //            params = [
        //                "email":txt_Email.text!,
        //                "password":txt_Password.text!
        //            ]
        //
        //            signupApiCall(param: params)
        //        }
    }
    
    @IBAction func btn_Events(_ sender: UIButton) {
        ImgTriangle.isHidden = true
        imgTriangleEvents.isHidden = false
        viewMiddle.isHidden = true
        msgView.isHidden = true
        tableview.isHidden = false
    }
    
    @IBAction func btn_CallSignup(_ sender: UIButton) {
   
        view.endEditing(true)
        if(self.validation() == true) {

            if self.btncheck.tag == 1 {
                
                self.btncheck.tag = 0
                self.btncheck.setBackgroundImage(UIImage(named: "tick.png"), for: UIControlState.normal)
                
                UserDefaults.standard.set(self.txt_Email.text, forKey: "kUserEmail")
                UserDefaults.standard.set(self.txt_Password.text, forKey: "kUserPassword")
                UserDefaults.standard.set("1", forKey: "kIsRememberMe")
                
            }
            
            params = [
                "email":txt_Email.text!,
                "password":txt_Password.text!
            ]
//            signupApiCall(param: params)
            loginApiCall(param: params)
        }
    }
    
    @IBAction func btn_OkPasswordReset(_ sender: Any) {
        
        self.PasswordResetView3.isHidden = true
         self.blurView.isHidden = true
    }
    @IBAction func btn_signup(_ sender: UIButton) {
        
        let link =  "https://copelandgroupusa.com/registration"
        if(link.characters.count > 0){
            
            let safariVC = SFSafariViewController(url: NSURL(string: link)! as URL)
            self.present(safariVC, animated: true, completion: nil)
            safariVC.delegate = self
        }
    }
    
    @IBAction func btn_agent(_ sender: UIButton) {
        
        viewMiddle.isHidden = false
        msgView.isHidden = false
        tableview.isHidden = true
        imgTriangleSign.isHidden = true
        imgTriangleEvents.isHidden = true
        ImgTriangle.isHidden = false
        btnSignin.isHidden = false
        btnContinue.isHidden = true
        btnForgot.isHidden = false
        LblMsg.text = "Providing the BEST Data & Technology in Healthcare"
    }
    
    @IBAction func btn_GoPasswordReset(_ sender: Any) {
        
      
        let code = self.txtVerifyCodeForgotPassword.text
        
        if(code != nil && AppUtilites.sharedInstance.isValidatePresence(string: code!))
        {
        
            params = [  "token":code   ]
            VerifyCodeAPICall(param: params)
        }else{
              AppUtilites.showError(title: "Hold On...",message: "Enter code before continuing!", cancelButtonTitle:"OK")
        }
        
    }
    
    //MARK:-  Textfield Delegate Method...
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == txt_Email{
            txt_Password.becomeFirstResponder()
        }
        else if textField == txt_Password{
            
            txt_Password.resignFirstResponder()
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField){
        
        
    }
    
    
    //MARK:-  Textfield validation
    
    func validation() -> Bool {
        
        if (!AppUtilites.sharedInstance.isValidatePresence(string: self.txt_Email.text!)) {
            AppUtilites.showError(title: "Hold On...",message: "Enter a Email before continuing!", cancelButtonTitle:"OK")
            return false
        }
        else if (!AppUtilites.sharedInstance.isValidEmail(string: self.txt_Email.text!)) {
            AppUtilites.showError(title: "Hold On...",message: "Enter Email before continuing!", cancelButtonTitle:"OK")
            return false
        }
        else if (!AppUtilites.sharedInstance.isValidatePresence(string: self.txt_Password.text!)) {
            AppUtilites.showError(title: "Hold On...",message: "Enter password before continuing!", cancelButtonTitle:"OK")
            return false
        }
        else {
            return true
        }
    }
    
    @IBAction func btnCallNow(_ sender: Any) {
        
        if let url = URL(string: "tel://1-800-220-8899") {
            UIApplication.shared.open(url)
        }
    }
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients(["help@copelandgroupusa.com"])
        mailComposerVC.setSubject("CIG")
        mailComposerVC.setMessageBody(k_email_declaimer, isHTML: true)
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        
        let alert = AppUtilites.SimpleAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.")
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: MFMailComposeViewControllerDelegate
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnHelpDesk(_ sender: Any) {
        
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
        /*let link =  "https://cig.on.spiceworks.com/portal/tickets"
        if(link.characters.count > 0){

            let safariVC = SFSafariViewController(url: NSURL(string: link)! as URL)
            self.present(safariVC, animated: true, completion: nil)
            safariVC.delegate = self
        }*/
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (section == 0){
            return  self.arrNearby.count
        }else{
            return self.arrNotNearBy.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let dic : NSDictionary
        if (indexPath.section == 0){
            dic = self.arrNearby[indexPath.row] as! NSDictionary
        }else{
            dic = self.arrNotNearBy[indexPath.row] as! NSDictionary
        }
        
        
        if(indexPath.row == self.viewMoreIndex && indexPath.section == self.selectedSectionIndex){
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "CIGEventDetailTC") as! CIGEventDetailTC
            cell.lblTitle.text = dic.value(forKey: "title") as? String
            
            let startDate = dic.value(forKey: "from_date") as! String
            let endDate = dic.value(forKey: "to_date") as! String
            
            let arrDateComponant = endDate.components(separatedBy: " ")
            if(arrDateComponant.count > 1){
                cell.lblDateTime.text = "\(startDate) - \(arrDateComponant[4] )"
            }
            else{
                cell.lblDateTime.text = "\(startDate)"
            }
            cell.btnViewMore.tag = indexPath.row
            
            cell.lblLocation.text = dic.value(forKey: "address") as? String
            cell.lblDescription.text = dic.value(forKey: "desc") as? String
            cell.btnViewMore.setTitle("View Less", for: UIControlState.normal)
            cell.btnSavetoCalender.tag = indexPath.row
            
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CIGEventTC") as! CIGEventTC
        cell.lblTitle.text = dic.value(forKey: "title") as? String
        
        let startDate = dic.value(forKey: "from_date") as! String
        let endDate = dic.value(forKey: "to_date") as! String
        cell.btnViewMore.tag = indexPath.row
        
        let arrDateComponant = endDate.components(separatedBy: " ")
        if(arrDateComponant.count > 1){
            cell.lblDateTime.text = "\(startDate) - \(arrDateComponant[4] )"
        }
        else{
            cell.lblDateTime.text = "\(startDate)"
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if(indexPath.row == self.viewMoreIndex && indexPath.section == self.selectedSectionIndex){
            
            let dic : NSDictionary
            if (indexPath.section == 0){
                dic = self.arrNearby[indexPath.row] as! NSDictionary
            }else{
                dic = self.arrNotNearBy[indexPath.row] as! NSDictionary
            }
            let font = UIFont(name: "Verdana", size: 15)
            
            if(UIDevice.current.userInterfaceIdiom == .pad) {
                
                return (100 + heightForLabel(text:  (dic.value(forKey: "desc") as? String)!, font: font!, width: self.view.frame.size.width - 30))
                
            }
            else{
                return (85 + heightForLabel(text:  (dic.value(forKey: "desc") as? String)!, font: font!, width: self.view.frame.size.width - 30))
            }
            
        }
        return 65
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if(section == 0 && self.arrNearby.count == 0){
            return UIView()
        }
        if(section == 1 && self.arrNotNearBy.count == 0){
            return UIView()
        }
        let headerview = UIView()
        headerview.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 60)
        let lblTitle = UILabel()
        lblTitle.frame = headerview.frame
        if (section == 0) {
            lblTitle.text = "Events Near Me"
        }else{
            lblTitle.text = "All Events"
        }
        lblTitle.textAlignment = .center
        lblTitle.font = UIFont.systemFont(ofSize: 20)
        lblTitle.textColor = .red
        headerview.addSubview(lblTitle)
        return headerview
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0 && self.arrNearby.count == 0){
            return 0.01
        }
        if(section == 1 && self.arrNotNearBy.count == 0){
            return 0.01
        }
        return 60
    }
    
    func heightForLabel(text:String, font:UIFont, width:CGFloat) -> CGFloat
    {
        let label:UILabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        //UILabel(frame: rect (0, 0, width, CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        
        label.sizeToFit()
        return label.frame.height
        
    }
    
    @IBAction func onSavetoCalenderClick(_ sender: Any) {
        
        //        DispatchQueue.main.async {
        var selectedsection = -1000
        
        if let cell = (sender as! UIButton).superview?.superview?.superview as? CIGEventDetailTC {
            let indexPath = tableview.indexPath(for: cell)
            selectedsection = (indexPath?.section)!
        }
        
        self.store.requestAccess(to: EKEntityType.event) { (granted,error ) in
            var dic = NSDictionary()
            if (selectedsection == 0){
                dic = self.arrNearby[(sender as! UIButton).tag] as! NSDictionary
            }else{
                dic = self.arrNotNearBy[(sender as! UIButton).tag] as! NSDictionary
            }
            //let dic = self.arrEvents[(sender as! UIButton).tag] as! NSDictionary
            
            if (granted == true){
                
                
                
                let formatter : DateFormatter = DateFormatter()
                formatter.dateFormat = "MMMM dd, yyyy hh:mma"
                
                print(dic.value(forKey: "from_date") as! String)
                
                let startDate = formatter.date(from: dic.value(forKey: "from_date") as! String)!
                
                print(startDate)
                var endDate = Date()
                if (dic.value(forKey: "to_date") as! String).characters.count > 0 {
                    
                    endDate = formatter.date(from: dic.value(forKey: "to_date") as! String)!
                }
                else{
                    
                    endDate = startDate.addingTimeInterval(60*60)
                }
                
                
                
                self.addEventToCalendar(title:(dic.value(forKey: "title") as? String)! ,
                                        description: "\(dic.value(forKey: "location") as! String) \(dic.value(forKey: "address") as! String)",
                    startDate: startDate,
                    endDate:  endDate)
            }
        }
        //        }
    }
    
    
    func addEventToCalendar(title: String, description: String?, startDate: Date, endDate: Date, completion: ((_ success: Bool, _ error: NSError?) -> Void)? = nil) {
        let eventStore = EKEventStore()
        
        eventStore.requestAccess(to: .event, completion: { (granted, error) in
            if (granted) && (error == nil) {
                let event = EKEvent(eventStore: eventStore)
                event.title = title
                event.startDate = startDate
                event.endDate = endDate
                event.notes = description
                event.calendar = eventStore.defaultCalendarForNewEvents
                do {
                    try eventStore.save(event, span: .thisEvent)
                } catch let e as NSError {
                    completion?(false, e)
                    return
                }
                
                DispatchQueue.main.async {
                    AppUtilites.showSucess(message: "Event Saved Successfully.")
                }
                completion?(true, nil)
            } else {
                completion?(false, error as NSError?)
            }
        })
    }
    
    @IBAction func btnViewMoreClicked(_ sender: Any) {
        
        if let cell = (sender as! UIButton).superview?.superview?.superview as? CIGEventTC {
            let indexPath = tableview.indexPath(for: cell)
            self.selectedSectionIndex = (indexPath?.section)!
        }
        
        self.viewMoreIndex = (sender as! UIButton).tag
        self.setTableHeight()
        self.tableview.reloadData()
    }
    
    @IBAction func btnViewLessClicked(_ sender: Any) {
        
        self.selectedSectionIndex = -1000
        self.viewMoreIndex = -1000
        self.setTableHeight()
        self.tableview.reloadData()
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    func setTableHeight(){
        
//        if(UIDevice.current.userInterfaceIdiom != .pad) {
//
//            if (self.arrEvents.count == 0){
//
//                self.eventTableViewHeightConstraint.constant = estimatedHeight
//            }
//            else {
//
//                if (self.viewMoreIndex != -1000){
//
//                    //self.eventTableViewHeightConstraint.constant = CGFloat(self.arrEvents.count * 65) + 70.0
//
//                }
//                else {
//                    self.eventTableViewHeightConstraint.constant = CGFloat(self.arrEvents.count * 65)
//                }
//
//                if( self.eventTableViewHeightConstraint.constant < estimatedHeight){
//
//                    self.eventTableViewHeightConstraint.constant = estimatedHeight
//                }
//            }
//        }
    }
    func getEventList(){
        
        APIClient.showProgress()
        
        APIClient.sharedInstance.MakeAPICallWithEndURl(BASE_URL.appending(k_event_list) as NSString, parameters: nil ) { (response, error) in
            
            APIClient.dismissProgress()
            
            if (error == nil) {
                // code of responce...
                let resutls = response! as NSDictionary
                print(resutls)
                
                let array = resutls.value(forKey: "event_list") as! NSArray
                self.arrEvents = NSMutableArray(array: resutls.value(forKey: "event_list") as! NSArray)
                //self.arrEvents = NSMutableArray(array: self.arrEvents.reverseObjectEnumerator().allObjects).mutableCopy() as! NSMutableArray
                //UserDefaults.standard.set("bgbgnn", forKey: "currentCity")
                if (UserDefaults.standard.value(forKey: "currentCity") != nil){
                    
                    let zipcode = UserDefaults.standard.value(forKey: "currentCity") as! String
                    
                    var temparray1 = NSMutableArray()
                    var temparray2 = NSMutableArray()
                    for dict in self.arrEvents{
                        if (((dict as! NSDictionary).value(forKey: "city")) != nil){
                            let otherzipcode = ((dict as! NSDictionary).value(forKey: "city")) as! String
                            if zipcode.caseInsensitiveCompare(otherzipcode) == ComparisonResult.orderedSame{
                                temparray1.add(dict)
                            }else{
                                temparray2.add(dict)
                            }
                        }
                    }
                    //if temparray1.count > 0{
                        self.arrNearby = temparray1
                        self.arrNotNearBy = temparray2
                        
                    //}else{
                    //    self.arrNearby = NSMutableArray()
                    //    self.arrNotNearBy = self.arrEvents
                    //}
                    self.setTableHeight()
                    self.tableview.reloadData()
                    
                    //self.sortallrecordsbyzipcode()
                }else{
                    self.arrNotNearBy = self.arrEvents
                    self.setTableHeight()
                    self.tableview.reloadData()
                }
                
                //                self.setTableHeight()
                //                self.tableview.reloadData()
                
                
            }else{
                AppUtilites.showError(title: "Oops...",message: "Server timed out!", cancelButtonTitle:"OK")
                
            }
        }
    }
    
    func sortallrecordsbyzipcode(){
        
        let zipcode = UserDefaults.standard.value(forKey: "currentCity") as! String
        
        var temparray1 = NSMutableArray()
        var temparray2 = NSMutableArray()
        for dict in self.arrEvents{
            if (((dict as! NSDictionary).value(forKey: "city")) != nil){
                let otherzipcode = ((dict as! NSDictionary).value(forKey: "city")) as! String
                if zipcode.caseInsensitiveCompare(otherzipcode) == ComparisonResult.orderedSame{
                    temparray1.add(dict)
                }else{
                    temparray2.add(dict)
                }
            }
        }
        self.arrNearby = temparray1
        self.arrNotNearBy = temparray2
        /*if temparray.count > 0{
            self.arrEvents = temparray
        }*/
        self.setTableHeight()
        self.tableview.reloadData()
        
    }
}
