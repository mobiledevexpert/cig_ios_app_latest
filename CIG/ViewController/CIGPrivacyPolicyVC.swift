//
//  CIGPrivacyPolicyVC.swift
//  CIG
//
//  Created by Harry on 11/6/17.
//  Copyright © 2017 Harry. All rights reserved.
//

import UIKit

class CIGPrivacyPolicyVC: UIViewController {

    @IBOutlet weak var webViewPrivacy: UIWebView!
      @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (UIScreen.main.nativeBounds.height == 2436) {
            self.headerViewHeightConstraint.constant = self.headerViewHeightConstraint.constant + 20
        }

        
        var path: String? = Bundle.main.path(forResource: "Privacy Policy", ofType: "docx")
        var targetURL = URL(fileURLWithPath: path ?? "")
        var request = URLRequest(url: targetURL)
        webViewPrivacy.loadRequest(request)
        
//        webViewPrivacy.loadRequest(URLRequest(url: URL(string: "https://docs.google.com/document/d/1L-riDZ4aW0Kc0Ta6LOhr-poMStbhhRb_YmP8fJmCOQ0/edit?ts=59fc9c73")!))
        
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func btnBackPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
