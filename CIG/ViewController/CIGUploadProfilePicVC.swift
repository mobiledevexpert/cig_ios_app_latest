//
//  CIGUploadProfilePicVC.swift
//  CIG
//
//  Created by Harry on 11/2/17.
//  Copyright © 2017 Harry. All rights reserved.
//

import UIKit
import SafariServices

class CIGUploadProfilePicVC: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,SFSafariViewControllerDelegate {

    @IBOutlet var profilePIcImgView: UIImageView!
    @IBOutlet var btnCallNow: UIButton!
    @IBOutlet var btnHelpDeak: UIButton!
    @IBOutlet var btnContinue: UIButton!

     let imagePicker = UIImagePickerController()
    var user = AMUserInfo()
    
    var gradeientForHelpDeskButton : CAGradientLayer!
    var gradeientForbtnCallNow : CAGradientLayer!
    
    var isImageSelected = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        btnHelpDeak.layoutIfNeeded()
        btnCallNow.layoutIfNeeded()
        
        user = AppUtilites.sharedInstance.getUserData()
        isImageSelected = false
        self.imagePicker.delegate = self
        
        self.profilePIcImgView.setIndicatorStyle(UIActivityIndicatorViewStyle.gray)
        self.profilePIcImgView.setShowActivityIndicator(true)
        self.profilePIcImgView.sd_setImage(with: URL(string: user.avatar_image))
        self.profilePIcImgView.SetRoundImageWithShadow(image: profilePIcImgView)
        
        
        gradeientForHelpDeskButton = AppUtilites.sharedInstance.GetGreenGradient(self.view.frame.size.width/2, self.btnHelpDeak.frame.size.height)
        btnHelpDeak.layer.insertSublayer(gradeientForHelpDeskButton, at: 0)
        
        
        gradeientForbtnCallNow = AppUtilites.sharedInstance.GetBlueGradient(self.view.frame.size.width/2, self.btnCallNow.frame.size.height)
        btnCallNow.layer.insertSublayer(gradeientForbtnCallNow, at: 0)
        
        // Do any additional setup after loading the view.
    }

    @IBAction func onSelectPicClicked(_ sender: Any) {
        
        let alertController = UIAlertController(title: "Update Image", message: "", preferredStyle: .actionSheet)
        
        let sendButton = UIAlertAction(title: "Take Photo", style: .default, handler: { (action) -> Void in
            self.OpenImagePicker(openWithCamera: true)
        })
        
        let  deleteButton = UIAlertAction(title: "Choose Photo", style: .default, handler: { (action) -> Void in
            self.OpenImagePicker(openWithCamera: false)
        })
        
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in
            
        })
        
        alertController.addAction(sendButton)
        alertController.addAction(deleteButton)
        alertController.addAction(cancelButton)
        if let popoverController = alertController.popoverPresentationController {
            popoverController.sourceView = sender as! UIView
            popoverController.sourceRect = (sender as AnyObject).bounds
        }
        //        self.present(alertController, animated: true, completion: nil)
        
        self.navigationController!.present(alertController, animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func OpenImagePicker(openWithCamera: Bool) {
        
        imagePicker.allowsEditing = false
        
        if(openWithCamera)
        {
            if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
                
                imagePicker.sourceType = .camera
            }
            else{
                
                AppUtilites.showInfo(message: "Camera not available")
                return;
            }
        }
        else
        {
            imagePicker.sourceType = .photoLibrary
        }
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            self.profilePIcImgView.contentMode = .scaleToFill
            self.profilePIcImgView.image = pickedImage
            self.isImageSelected = true
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func btnCallNow(_ sender: Any) {
        
        if let url = URL(string: "tel://1-800-220-8899") {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func btnHelpDesk(_ sender: Any) {
        
        let link =  "https://cig.on.spiceworks.com/portal/tickets"
        if(link.characters.count > 0){
            
            let safariVC = SFSafariViewController(url: NSURL(string: link)! as URL)
            self.present(safariVC, animated: true, completion: nil)
            safariVC.delegate = self
        }
    }
    
     @IBAction func btn_continues(_ sender: UIButton) {
        
        
        if(isImageSelected == false){
            
            AppUtilites.showError(title: "Message",message: "Please Select Image.", cancelButtonTitle:"OK")
        }
        else {
            
            if(self.isImageSelected == true){
                
                let params = [
                    "user_id": user.user_id,
                    "display_name": user.display_name,
                    "team_id": user.team_id,
                    "state": "",
                    "region": ""
                    
                    ] as [String : Any]
                
                APIClient.showProgress()
                APIClient.sharedInstance.postImageToServer(BASE_URL.appending(k_update_profile) as NSString, image: self.profilePIcImgView.image, parameters: params as NSDictionary , completionHandler: { (response, error ) in
                    
                    
                    APIClient.dismissProgress()
                    
                    if (error == nil) {
                        // code of responce...
                        let resutls = response! as NSDictionary
                        print(resutls)
                        let status : Int = resutls.value(forKey: "status") as! Int
                        if(status == 1) {
                            
                            let user = resutls.value(forKeyPath: "user") as! NSDictionary
                            print(user)
                            AppUtilites.sharedInstance.saveUserData(data: user)
                            
                            let startVC : CIGStratVC = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGStratVC") as! CIGStratVC
                            self.navigationController?.pushViewController(startVC, animated: true)
                            
                        }else{
                            let error = resutls.value(forKey: "msg") as! String
                            AppUtilites.showError(title: "Error",message: error, cancelButtonTitle:"OK")
                        }
                    }else{
                        AppUtilites.showError(title: "Oops...",message: "Server timed out!", cancelButtonTitle:"OK")
                        
                    }
                })
            }
        }
    }
    
    @IBAction func btn_back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
