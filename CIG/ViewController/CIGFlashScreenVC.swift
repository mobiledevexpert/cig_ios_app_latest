//
//  CIGFlashScreenVC.swift
//  CIG
//
//  Created by Harry on 10/24/17.
//  Copyright © 2017 Harry. All rights reserved.
//

import UIKit
import SwiftyGif
import LocalAuthentication


class CIGFlashScreenVC: UIViewController,SwiftyGifDelegate {

    @IBOutlet var btnBack: UIButton!
    
    @IBOutlet weak var imgBack: UIImageView!
    @IBOutlet var lblTemp: UILabel!
    
    var isLoadedFromSettings : Bool = false
    var context = LAContext()

    let kMsgShowFinger = "Show me your finger 👍"
    let kMsgShowReason = "Unlock CIG."
    let kMsgFingerOK = "Login successful! ✅"
    
    deinit {
        
        Utils.removeObserverForNotifications(observer: self)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if(isLoadedFromSettings){
            
            btnBack.isHidden = false
        }
        else{
             setupController()
        }
        
        let temperatre = UserDefaults.standard.value(forKey: "CurrentTemp")
        
        if(temperatre == nil)
        {
            self.lblTemp.text = "Current Temperature : \(UserDefaults.standard.value(forKey: "CurrentTemp")) °F"
        }else{
            
            let temp : String = "\(UserDefaults.standard.value(forKey: "CurrentTemp")!)"
            
            if(temp.characters.count >= 6)
            {
                self.lblTemp.text = "Current Temperature : \((temp as NSString).substring(to: 5)) °F"
            } else
            {
                self.lblTemp.text = "Current Temperature : \(temp) °F"
            }
        }
    }

    override func viewWillAppear(_ animated: Bool){
        
        super.viewWillAppear(animated)
        if (UserDefaults.standard.value(forKey: "Isloaded") == nil) {
            UserDefaults.standard.set("1", forKey: "Isloaded")
            self.startAnimationAndTimer()
        }else{
           updateUI()
        }
        //updateUI()
    }
    
    private func setupController(){
        
        Utils.registerNotificationWillEnterForeground(observer: self, selector: #selector(CIGFlashScreenVC.updateUI))
    }
    
    @objc func updateUI()
    {
        self.startAnimationAndTimer()
//        var policy: LAPolicy?
//
//        if #available(iOS 9.0, *)
//        {
//
//            policy = .deviceOwnerAuthentication
//        }
//        else{
//
//            context.localizedFallbackTitle = "Fuu!"
//            policy = .deviceOwnerAuthenticationWithBiometrics
//        }
//
//        var err: NSError?
//        guard context.canEvaluatePolicy(policy!, error: &err) else {
//
//            AppUtilites.showError(title: "CIG Agent", message: "Touch Id and Passcode not set.", cancelButtonTitle: "Ok")
//            self.startAnimationAndTimer()
//            return
//        }
//
//        loginProcess(policy: policy!)
    }
    
    private func loginProcess(policy: LAPolicy)
    {
        
        context.evaluatePolicy(policy, localizedReason: kMsgShowReason, reply: { (success, error) in
            DispatchQueue.main.async {

                if success {
                    
                  self.startAnimationAndTimer()
                    
                } else {
                    guard let error = error else {
                        self.showUnexpectedErrorMessage()
                        return
                    }
                    switch(error) {
                        
                        
                    case LAError.authenticationFailed:
                       print("There was a problem verifying your identity.")
                        
                    case LAError.userCancel:
                        
                       print("Authentication was canceled by user.")
                        //self.startAnimationAndTimer()
                       self.updateUI()
                        
                    case LAError.userFallback:
                       print("The user tapped the fallback button (Fuu!)")
                    case LAError.systemCancel:
                       print("Authentication was canceled by system.")
                        
                    case LAError.passcodeNotSet:
                       print("Passcode is not set on the device.")
                    case LAError.touchIDNotAvailable:
                        print("Touch ID is not available on the device.")
                    case LAError.touchIDNotEnrolled:
                        print("Touch ID has no enrolled fingers.")
                        
                    case LAError.touchIDLockout:
                       print("There were too many failed Touch ID attempts and Touch ID is now locked.")
                    case LAError.appCancel:
                       print("Authentication was canceled by application.")
                    case LAError.invalidContext:
                        print("LAContext passed to this call has been previously invalidated.")
                        
                    default:
                        print("Touch ID may not be configured")
                        break
                    }
                    return
                }
                
                
            }
        })
    }
    
    func startAnimationAndTimer(){
        
        self.imgBack.delegate = self
        let gifManager = SwiftyGifManager(memoryLimit:20)
        let gif = UIImage(gifName: "cloud")
        self.imgBack.setGifImage(gif, manager: gifManager, loopCount:-1)
        self.imgBack.startAnimatingGif()
        
        let timer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector:#selector(self.moveToNextView), userInfo: nil, repeats: false)
    }
    private func showUnexpectedErrorMessage()
    {
       
    }
    
    @IBAction func btnBackClicked(_ sender: Any) {
        
          self.navigationController?.popViewController(animated: false)
    }
    
    @objc func moveToNextView() {

        var initialViewController = UIViewController()


        if(UserDefaults.standard.bool(forKey: "isLogin")){

            let homeVC  =  AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGHomeVC") as! CIGHomeVC

            let leftSideMenuVC  =  AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGLeftSideMenuVC") as? CIGLeftSideMenuVC

            let rightSideMenuVC  =  AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGRightSideMenuVC") as? CIGRightSideMenuVC

            initialViewController = SlideMenuController(mainViewController: homeVC, leftMenuViewController: leftSideMenuVC!, rightMenuViewController: rightSideMenuVC!)

        }else{
            initialViewController =  AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGLoginVC") as! CIGLoginVC
            //initialViewController =  AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGPreEventListVC") as! CIGPreEventListVC
        }
        
        let navigation = UINavigationController(rootViewController: initialViewController)
        navigation.navigationBar.isHidden = true
        let appDel : AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDel.window = UIWindow(frame: UIScreen.main.bounds)
        appDel.window?.rootViewController = navigation
        appDel.window?.makeKeyAndVisible()
        
//        let loginVC : CIGLoginVC = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGLoginVC") as! CIGLoginVC
//        self.navigationController?.pushViewController(loginVC, animated: true)
    }
    

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  

}
