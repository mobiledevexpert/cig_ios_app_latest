//
//  CIGCarrierAlertVC.swift
//  CIG
//
//  Created by Harry on 10/28/17.
//  Copyright © 2017 Harry. All rights reserved.
//

import UIKit
import SafariServices
import MessageUI
import TTTAttributedLabel

class CIGCarrierAlertVC: UIViewController,SFSafariViewControllerDelegate,MFMailComposeViewControllerDelegate,UITableViewDelegate,UITableViewDataSource,TTTAttributedLabelDelegate {

    
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var lblGoalPercentage: UILabel!
    @IBOutlet weak var fullProgressView: UIView!
    @IBOutlet weak var progressView: UIView!
    @IBOutlet weak var progressViewCon: NSLayoutConstraint!
    @IBOutlet weak var tableViewHeightCon: NSLayoutConstraint!

    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var btnHelpDeak: UIButton!
    @IBOutlet weak var btnCallNow: UIButton!
    @IBOutlet weak var lblGood: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!

    
    var gradeientForHeader : CAGradientLayer!
    var gradeientForProgressView : CAGradientLayer!
    var gradeientForHelpDeskButton : CAGradientLayer!
    var gradeientForbtnCallNow : CAGradientLayer!
    
    var arrCarrirAlers = NSMutableArray()
    var estimatedHeight = CGFloat()

    
    override func viewDidLoad() {
        super.viewDidLoad()

        if (UIScreen.main.nativeBounds.height == 2436) {
            self.headerViewHeightConstraint.constant = self.headerViewHeightConstraint.constant + 20
        }
        
        // Do any additional setup after loading the view.
        
        imgProfile.SetRoundImageWithBorder(image: imgProfile)
        
        let user = AppUtilites.sharedInstance.getUserData()
        print(user.display_name)
        print(user.avatar_image)
        
        btnHelpDeak.layoutIfNeeded()
        btnCallNow.layoutIfNeeded()
        headerView.layoutIfNeeded()
        progressView.layoutIfNeeded()
        
        estimatedHeight = self.view.frame.size.height - 352

        self.lblUserName.text = user.display_name
        self.imgProfile.setIndicatorStyle(UIActivityIndicatorViewStyle.gray)
        self.imgProfile.setShowActivityIndicator(true)
         self.lblGood.text = AppUtilites.sharedInstance.getCurrentstatus()
        
        if(user.goal_status == "0" || user.goal_status.characters.count == 0){
            
            self.progressViewCon.constant = 0
            self.lblGoalPercentage.text = "0%"
        }
        else{
            
            let goalamount : NSString  = (user.goal_status as NSString).substring(to: 3) as NSString
            let goalamountInt = goalamount.floatValue
            
            if(goalamount == "100"){
                
                self.progressViewCon.constant =  CGFloat(self.view.frame.size.width-170)
            }
            else{
                
                self.progressViewCon.constant =  CGFloat((Float(self.fullProgressView.frame.size.width)*goalamountInt)/100)
            }
//            self.progressViewCon.constant =  CGFloat((Float(self.fullProgressView.frame.size.width)*goalamountInt)/100)
            self.lblGoalPercentage.text = "\(user.goal_status)%"
        }

        
     
//        progressViewCon.constant = AppUtilites.sharedInstance.getWidthFromPercentage(percentage: 75)
        
        self.slideMenuController()?.removeLeftGestures()
        self.slideMenuController()?.removeRightGestures()
        self.slideMenuController()?.addLeftGestures()
        self.slideMenuController()?.addRightGestures()
        let screenSize: CGRect = UIScreen.main.bounds
        self.slideMenuController()?.changeLeftViewWidth(screenSize.width - 20);
        self.slideMenuController()?.changeRightViewWidth(screenSize.width - 20)
        
        progressView.layoutIfNeeded()
        btnHelpDeak.layoutIfNeeded()
        btnCallNow.layoutIfNeeded()
        
        
        gradeientForHeader = AppUtilites.sharedInstance.GetRedGradient(ScreenSize.SCREEN_WIDTH, headerView.frame.size.height)
        headerView.layer.insertSublayer(gradeientForHeader, at: 0)
        
        gradeientForProgressView = AppUtilites.sharedInstance.GetBlueGradient(progressViewCon.constant, progressView.frame.size.height)
        progressView.layer.insertSublayer(gradeientForProgressView, at: 0)
        
        gradeientForHelpDeskButton = AppUtilites.sharedInstance.GetGreenGradient(self.view.frame.size.width/2, self.btnHelpDeak.frame.size.height)
        btnHelpDeak.layer.insertSublayer(gradeientForHelpDeskButton, at: 0)
        
         gradeientForbtnCallNow = AppUtilites.sharedInstance.GetBlueGradient(self.view.frame.size.width/2, self.btnCallNow.frame.size.height)
        btnCallNow.layer.insertSublayer(gradeientForbtnCallNow, at: 0)
        
//        let user = AppUtilites.sharedInstance.getUserData()
        self.imgProfile.sd_setImage(with: URL(string: user.avatar_image))
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 100
        
        self.getCarrierAlertList()
    
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
      
        let user = AppUtilites.sharedInstance.getUserData()
        self.imgProfile.sd_setImage(with: URL(string: user.avatar_image))
        
    }
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients([AppUtilites.sharedInstance.teamLeadEmail])
        mailComposerVC.setSubject("CIG")
      mailComposerVC.setMessageBody(k_email_declaimer, isHTML: true)
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        
        let alert = AppUtilites.SimpleAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.")
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: MFMailComposeViewControllerDelegate
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func btnEmailUs(_ sender: Any) {
        
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return  self.arrCarrirAlers.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {


        let dic = self.arrCarrirAlers[indexPath.row] as! NSDictionary

        let cell = tableView.dequeueReusableCell(withIdentifier: "CIGCarrierAlertTC") as! CIGCarrierAlertTC
        
        cell.headerImageView.sd_setImage(with: URL(string: (dic.value(forKey: "iphone_logo") as? String)!))
        
        
        let dateFormatter = DateFormatter()
        let tempLocale = dateFormatter.locale // save locale temporarily
        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: dic.value(forKey: "created") as! String )

        cell.lblDateTime.text = AppUtilites.timeAgoStringFromDate(date:date!)!
        
        cell.lblDisc.text = "\(dic.value(forKey: "message") as! String)"
     
//        let tap = UITapGestureRecognizer(target: self, action: #selector(tapLabel(tap:)))
//        cell.lblDisc.addGestureRecognizer(tap)
//        cell.lblDisc.isUserInteractionEnabled = true

        
//        let attributedString = NSMutableAttributedString(string:"\(dic.value(forKey: "message") as! String)")
//
//        if let hyperlinks = dic.value(forKey: "hyperlink"){
//
//            for link in hyperlinks as! NSArray{
//
//                let linkWasSet = attributedString.setAsLink(textToFind: (link as! NSDictionary).value(forKey: "text") as! String, linkURL: (link as! NSDictionary).value(forKey: "link") as! String)
//
//
//                if linkWasSet {
//                    // adjust more attributedString properties
//                }
//            }
//        }
        
//        cell.lblDisc.attributedText = attributedString
        
        if let message = dic.value(forKey: "message-two"){

             cell.lblDisc.text = "\(dic.value(forKey: "message") as! String) \n\n \(message)"
        }

        if let url = dic.value(forKey: "url"){

            cell.lblDisc.text = "\(cell.lblDisc.text as! String) \n\n \(url)"
        }

        if let url2 = dic.value(forKey: "url-two"){

            cell.lblDisc.text = "\(cell.lblDisc.text as! String) \n\n \(url2)"
        }
        cell.lblDisc.delegate = self
        //cell.lblDisc.enabledTextCheckingTypes = NSTextCheckingResult.CheckingType.link.rawValue
        let arraylinks : NSArray = dic.object(forKey: "hyperlinks") as! NSArray
        if let hyperlinks = dic.value(forKey: "hyperlinks"){
            for dicthyper in arraylinks {
                let dicthyper1 : NSDictionary = dicthyper as! NSDictionary
                print(dicthyper1.value(forKey: "text") as! String)
                //let range1 = cell.lblDisc.text?.range(of: dicthyper1.value(forKey: "link") as! String)
                let range2 = cell.lblDisc.text?.range(of: dicthyper1.value(forKey: "text") as! String)
                
                //cell.lblDisc.addLink(to: URL(string: dicthyper1.value(forKey: "link") as! String), with:
                 //   (cell.lblDisc.text?.nsRange(from: range1!))!)
                
                let url : NSString = dicthyper1.value(forKey: "link") as! NSString
                let urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
                var searchURL = URL(string: url as String)
                
                //print(searchURL)
                print(url)
                print(urlStr)
                //let urlstr = dicthyper1.value(forKey: "link") as! String
                //let urlllll = URL(string: urlstr)
                //print(urlstr)
                //print(urlllll)
                //print("this is search : \(searchURL!)")
                if searchURL == nil{
                    searchURL = URL(string: urlStr as String)
                }
                //print("this is search : \(searchURL)")
                cell.lblDisc.addLink(to: searchURL!, with:
                    (cell.lblDisc.text?.nsRange(from: range2!))!)
                //cell.lblDisc.addLink(to: NSURL(string: "gfg")! as URL, with: cell.lblDisc.text?.range(of: dicthyper1.s(forKey: "link")))
            }
        }
        if arraylinks.count == 0 {
            cell.lblDisc.text = "\(dic.value(forKey: "message") as! String)"
        }
        // Attach a block to be called when the user taps a URL
//        cell.lblDisc.urlLinkTapHandler = { label, url, range in
//
//            var strUrl : String = url
//            if strUrl.lowercased().hasPrefix("http://")==false{
//                strUrl = "http://\(strUrl)"
//            }
//
//            let safariVC = SFSafariViewController(url: NSURL(string: strUrl)! as URL)
//            safariVC.delegate = self
//            self.present(safariVC, animated: true, completion: nil)
//        }
        
        let colorStr = dic.value(forKey: "bg_color") as? String
        
        
        var RGBStr = colorStr!.split(separator: ",")
        
        
        let Red = Double (RGBStr[0])
        let Green = Double (RGBStr[1])
        let Blue = Double (RGBStr[2])
        
        cell.headerView.backgroundColor = UIColor(red: CGFloat(Red!/255.0), green: CGFloat(Green!/255.0), blue: CGFloat(Blue!/255.0), alpha: 1.0)
     
        return cell
    }
    
//    @objc func tapLabel(tap: UITapGestureRecognizer) {
//
//        guard let range = self.yourLabel.text?.range(of: "Substring to detect")?.nsRange else {
//            return
//        }
//        if tap.didTapAttributedTextInLabel(label: self.yourLabel, inRange: range) {
//            // Substring tapped
//        }
//    }
    func attributedLabel(_ label: TTTAttributedLabel!, didSelectLinkWith url: URL!) {
        let strdata = "\(url!)"
        print(strdata)
        if strdata.hasPrefix("mailto:") {
            let startIndex = strdata.index(strdata.startIndex, offsetBy: 7)
            let truncated = strdata.substring(from: startIndex)
            let mailComposeViewController = configuredMailComposeViewController()
            mailComposeViewController.setToRecipients([truncated])
            mailComposeViewController.setSubject("CIG")
            mailComposeViewController.setMessageBody("", isHTML: false)
            if MFMailComposeViewController.canSendMail() {
                self.present(mailComposeViewController, animated: true, completion: nil)
            } else {
                self.showSendMailErrorAlert()
            }
        }else{
            let safariVC = SFSafariViewController(url: url)
            safariVC.delegate = self
            self.present(safariVC, animated: true, completion: nil)
        }
        //UIApplication.shared.openURL(url)
        /*let safariVC = SFSafariViewController(url: url)
        safariVC.delegate = self
        self.present(safariVC, animated: true, completion: nil)*/
    }
    
    @IBAction func btnHelpDesk(_ sender: Any) {
        
        let link =  "https://cig.on.spiceworks.com/portal/tickets"
        if(link.characters.count > 0){
            
            let safariVC = SFSafariViewController(url: NSURL(string: link)! as URL)
            self.present(safariVC, animated: true, completion: nil)
            safariVC.delegate = self
        }
    }
    
    func getCarrierAlertList(){
        
        let user = AppUtilites.sharedInstance.getUserData()
        let param = ["user_id": user.user_id] as NSDictionary
        
        APIClient.showProgress()
        APIClient.sharedInstance.MakeAPICallWithEndURl(BASE_URL.appending(k_carrier_alert_data) as NSString, parameters: param) { (response, error) in
            
            APIClient.dismissProgress()
            
            if (error == nil) {
                // code of responce...
                let resutls = response! as NSDictionary
                print(resutls)
                self.arrCarrirAlers = NSMutableArray(array: resutls.value(forKey: "carrier_alert") as! NSArray)
                self.tableView.reloadData()
                
      
                if(UIDevice.current.userInterfaceIdiom != .pad) {
                    
                    if (self.arrCarrirAlers.count == 0){
                        
                        self.tableViewHeightCon.constant = self.estimatedHeight
                    }
                    else {
                        
                        self.tableViewHeightCon.constant = 0
                        
                        for dic in self.arrCarrirAlers {
                            
                            let message  = "\((dic as! NSDictionary).value(forKey: "message") as! String)"
                            let font = UIFont.systemFont(ofSize: 17.0)
                            
                            self.tableViewHeightCon.constant += CGFloat(self.heightForView(text: message, font: font, width: self.tableView.frame.size.width - 16.0) + 55.0)
                        }
                        
                        if( self.tableViewHeightCon.constant < self.estimatedHeight){
                            
                            self.tableViewHeightCon.constant = self.estimatedHeight
                        }
                    }
                }
                
            }else{
                
                AppUtilites.showError(title: "Oops...",message: "Server timed out!", cancelButtonTitle:"OK")
            }
        }
    }

    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        
        return label.frame.height
    }
    
    func setTableHeight(){
        
        if(UIDevice.current.userInterfaceIdiom != .pad) {
            
            if (self.arrCarrirAlers.count == 0){
                
                self.tableViewHeightCon.constant = estimatedHeight
            }
            else {
                
                self.tableViewHeightCon.constant = 0
                
                for dic in AppUtilites.sharedInstance.allTeam {
                    
                    self.tableViewHeightCon.constant += CGFloat(((dic as! NSDictionary).value(forKey: "team_contacts") as! NSArray).count * 102)
                }
                
                if( self.tableViewHeightCon.constant < estimatedHeight){
                    
                    self.tableViewHeightCon.constant = estimatedHeight
                }
            }
        }
    }
    
    // MARK: - Button Delegate
    
    @IBAction func clkBtnBackCarrierAlert(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func leftMenuButtonClicked(_ sender: Any) {
        self.slideMenuController()?.openLeft()
    }
    @IBAction func rightMenuButtonClicked(_ sender: Any) {
        self.slideMenuController()?.openRight()
    }

}

extension UITapGestureRecognizer {
    
    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)
        
        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        
        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize
        
        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x, y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y)
        
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x, y: locationOfTouchInLabel.y - textContainerOffset.y)
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        return NSLocationInRange(indexOfCharacter, targetRange)
    }
    
}

extension Range where Bound == String.Index {
    var nsRange:NSRange {
        return NSRange(location: self.lowerBound.encodedOffset,
                       length: self.upperBound.encodedOffset -
                        self.lowerBound.encodedOffset)
    }
}

extension NSMutableAttributedString {
    
    public func setAsLink(textToFind:String, linkURL:String) -> Bool {
        
        let foundRange = self.mutableString.range(of: textToFind)
        if foundRange.location != NSNotFound {
            self.addAttribute(NSLinkAttributeName, value: linkURL, range: foundRange)
            return true
        }
        return false
    }
    
}

extension String {
    func nsRange(from range: Range<Index>) -> NSRange {
        return NSRange(range, in: self)
    }
}
