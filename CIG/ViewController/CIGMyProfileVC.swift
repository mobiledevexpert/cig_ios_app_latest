//
//  CIGMyProfileVC.swift
//  CIG
//
//  Created by Harry on 10/28/17.
//  Copyright © 2017 Harry. All rights reserved.
//

import UIKit
import EventKit

class CIGMyProfileVC: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    
    @IBOutlet var viewGoals: UIView!
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var viewProfile: UIView!
    @IBOutlet var imgProfile: UIImageView!
    @IBOutlet var HeaderView: UIView!
    @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!
    
    let imagePicker = UIImagePickerController()
    var eventStore: EKEventStore!
    var reminders: [EKReminder]!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if (UIScreen.main.nativeBounds.height == 2436) {
            self.headerViewHeightConstraint.constant = self.headerViewHeightConstraint.constant + 20
        }
        

        
        let user = AppUtilites.sharedInstance.getUserData()
        print(user.display_name)
        print(user.avatar_image)
        
        
        self.HeaderView.SetViewShadow(view: HeaderView)
        self.imgProfile.setIndicatorStyle(UIActivityIndicatorViewStyle.gray)
        self.imgProfile.setShowActivityIndicator(true)
        self.imgProfile.sd_setImage(with: URL(string: user.avatar_image))
        self.imgProfile.SetRoundImageWithShadow(image: imgProfile)
        self.viewProfile.SetRoundView(view: viewProfile)
        self.viewProfile.SetViewShadow(view: viewProfile)
        lblUserName.text = user.display_name
        
        self.viewGoals.SetViewShadow(view: viewGoals)
         imagePicker.delegate = self
       
    }

    
    // MARK: - Button Click
    @IBAction func btnMyApp(_ sender: UIButton) {
        
        UIApplication.shared.openURL(NSURL(string: "calshow://")! as URL)
    }
    
    @IBAction func btnMyTasks(_ sender: UIButton) {
         UIApplication.shared.openURL(NSURL(string: "x-apple-reminder://") as! URL)
//        self.eventStore = EKEventStore()
//        self.reminders = [EKReminder]()
//        self.eventStore.requestAccess(to: .reminder) { (granted, error) in
//            if granted{
//                // 2
//                let predicate = self.eventStore.predicateForReminders(in: nil)
//                self.eventStore.fetchReminders(matching: predicate, completion: { (reminders: [EKReminder]?) -> Void in
//
//                    self.reminders = reminders
//
//                })
//            }else{
//                print("The app is not permitted to access reminders, make sure to grant permission in the settings and try again")
//            }
//        }
    }
    
    @IBAction func btnCopenlad(_ sender: UIButton) {
        
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGCopelandPerksVC") as! CIGCopelandPerksVC
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    
    @IBAction func btnMyStats(_ sender: UIButton) {
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGMyStatsVC") as! CIGMyStatsVC
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func btnPersonalgoals(_ sender: UIButton) {
        
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGPersonalGoalsVC") as! CIGPersonalGoalsVC
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    @IBAction func btnDone(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)

    }

    @IBAction func btnChangeImageClicked(_ sender: Any) {
        let alertController = UIAlertController(title: "Update Image", message: "", preferredStyle: .actionSheet)
        
        let sendButton = UIAlertAction(title: "Take Photo", style: .default, handler: { (action) -> Void in
            self.OpenImagePicker(openWithCamera: true)
        })
        
        let  deleteButton = UIAlertAction(title: "Choose Photo", style: .default, handler: { (action) -> Void in
            self.OpenImagePicker(openWithCamera: false)
        })
        
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in
           
        })
        
        alertController.addAction(sendButton)
        alertController.addAction(deleteButton)
        alertController.addAction(cancelButton)
        if let popoverController = alertController.popoverPresentationController {
            popoverController.sourceView = sender as! UIView
            popoverController.sourceRect = (sender as AnyObject).bounds
        }
//        self.present(alertController, animated: true, completion: nil)
        
        self.navigationController!.present(alertController, animated: true, completion: nil)

    }
    
    func OpenImagePicker(openWithCamera: Bool) {
        
        imagePicker.allowsEditing = false
        
        if(openWithCamera)
        {
            if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
                
                imagePicker.sourceType = .camera
            }
            else{
                
                AppUtilites.showInfo(message: "Camera not available")
                return;
            }
        }
        else
        {
            imagePicker.sourceType = .photoLibrary
        }
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imgProfile.contentMode = .scaleToFill
            imgProfile.image = pickedImage
            dismiss(animated: true, completion: nil)
            
            let user = AppUtilites.sharedInstance.getUserData()
            
            let params = [
                "user_id": user.user_id,
                "display_name": user.display_name,
                "team_id": user.team_id,
                "state": "",
                "region": ""
                
                ] as [String : Any]
            
            APIClient.showProgress()
            APIClient.sharedInstance.postImageToServer(BASE_URL.appending(k_update_profile) as NSString, image: self.imgProfile.image, parameters: params as NSDictionary , completionHandler: { (response, error ) in
                
                
                APIClient.dismissProgress()
                
                if (error == nil) {
                    // code of responce...
                    let resutls = response! as NSDictionary
                    print(resutls)
                    let status : Int = resutls.value(forKey: "status") as! Int
                    if(status == 1) {
                        
                        let user = resutls.value(forKeyPath: "user") as! NSDictionary
                        print(user)
                        AppUtilites.sharedInstance.saveUserData(data: user)
                        
                    }else{
                        let error = resutls.value(forKey: "msg") as! String
                        AppUtilites.showError(title: "Error",message: error, cancelButtonTitle:"OK")
                    }
                }else{
                    AppUtilites.showError(title: "Oops...",message: "Server timed out!", cancelButtonTitle:"OK")
                    
                }
            })
        }
    }

    

    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
         dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
