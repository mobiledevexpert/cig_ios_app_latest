//
//  CIGOnlineQuotingVC.swift
//  CIG
//
//  Created by Harry on 10/28/17.
//  Copyright © 2017 Harry. All rights reserved.
//

import UIKit
import MessageUI
import SafariServices

class CIGOnlineQuotingVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,SFSafariViewControllerDelegate,MFMailComposeViewControllerDelegate{

    
    @IBOutlet var CollectionViewHeightconstraint: NSLayoutConstraint!
    @IBOutlet var lblGoalPercentage: UILabel!
    @IBOutlet weak var fullProgressView: UIView!
    @IBOutlet weak var progressView: UIView!
    @IBOutlet weak var progressViewCon: NSLayoutConstraint!
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var btnHelpDeak: UIButton!
    @IBOutlet weak var btnCallNow: UIButton!
    @IBOutlet weak var lblGood: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var viewAppStore: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!

    
    var gradeientForHeader : CAGradientLayer!
    var gradeientForProgressView : CAGradientLayer!
    var gradeientForHelpDeskButton : CAGradientLayer!
    var gradeientForbtnCallNow : CAGradientLayer!
    
    var arrCarrier = NSMutableArray()
    var arrCarrierLinks = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()

        if (UIScreen.main.nativeBounds.height == 2436) {
            self.headerViewHeightConstraint.constant = self.headerViewHeightConstraint.constant + 20
        }
        
        // Do any additional setup after loading the view.
        imgProfile.SetRoundImageWithBorder(image: imgProfile)
        
        progressView.layoutIfNeeded()
        btnHelpDeak.layoutIfNeeded()
        btnCallNow.layoutIfNeeded()
        
        let user = AppUtilites.sharedInstance.getUserData()
        print(user.display_name)
        print(user.avatar_image)
        
        self.lblUserName.text = user.display_name
        self.imgProfile.setIndicatorStyle(UIActivityIndicatorViewStyle.gray)
        self.imgProfile.setShowActivityIndicator(true)
          self.lblGood.text = AppUtilites.sharedInstance.getCurrentstatus()
        
        if(user.goal_status == "0" || user.goal_status.characters.count == 0){
            
            self.progressViewCon.constant = 0
            self.lblGoalPercentage.text = "0%"
        }
        else{
            
        
            let goalamount : NSString  = (user.goal_status as NSString).substring(to: 3) as NSString
            let goalamountInt = goalamount.floatValue
            
            if(goalamount == "100"){
                
                self.progressViewCon.constant =  CGFloat(self.view.frame.size.width-170)
            }
            else{
                
                self.progressViewCon.constant =  CGFloat((Float(self.fullProgressView.frame.size.width)*goalamountInt)/100)
            }
            
//            self.progressViewCon.constant =  CGFloat((Float(self.fullProgressView.frame.size.width)*goalamountInt)/100)
            self.lblGoalPercentage.text = "\(user.goal_status)%"

            
        }

        arrCarrierLinks = ["http://www.aetna.com/about-aetna-insurance/sas/aetna-coventry-close.html",
                           "https://www.aetna.com/index.html",
                           "http://agentra.com/",
                           "http://www.aig.com/individual/insurance/life",
                           "https://amerigroup.com/",
                           "https://www.anthem.com/",
                           "https://www.bcbstx.com/",
                           "https://www.christushealth.org/",
                           "https://www.cigna.com/medicare/cigna-healthspring",
                           "http://www.orion-insurance.co.uk/foresters/",
                           "https://www.gerberlife.com/",
                           "https://www.humana.com/",
                           "http://www.gomedico.com/",
                           "http://www.molinahealthcare.com/en-US/Pages/home.aspx",
                           "https://www.mutualofomaha.com/",
                           "https://apps.neweralife.com/site/",
                           "http://www.swhp.org/",
                           "https://www.silverscript.com/",
                           "https://www.uhc.com/",
                            "https://www.wellcare.com/"]
        btnHelpDeak.layoutIfNeeded()
        btnCallNow.layoutIfNeeded()
        headerView.layoutIfNeeded()
        progressView.layoutIfNeeded()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        self.slideMenuController()?.removeLeftGestures()
        self.slideMenuController()?.removeRightGestures()
        self.slideMenuController()?.addLeftGestures()
        self.slideMenuController()?.addRightGestures()
        let screenSize: CGRect = UIScreen.main.bounds
        self.slideMenuController()?.changeLeftViewWidth(screenSize.width - 20);
        self.slideMenuController()?.changeRightViewWidth(screenSize.width - 20)
        

        
        gradeientForHeader = AppUtilites.sharedInstance.GetRedGradient(ScreenSize.SCREEN_WIDTH, headerView.frame.size.height)
        headerView.layer.insertSublayer(gradeientForHeader, at: 0)
        
        gradeientForProgressView = AppUtilites.sharedInstance.GetBlueGradient(progressViewCon.constant, progressView.frame.size.height)
        progressView.layer.insertSublayer(gradeientForProgressView, at: 0)
        
        gradeientForHelpDeskButton = AppUtilites.sharedInstance.GetGreenGradient(self.view.frame.size.width/2, self.btnHelpDeak.frame.size.height)
        btnHelpDeak.layer.insertSublayer(gradeientForHelpDeskButton, at: 0)
        
         gradeientForbtnCallNow = AppUtilites.sharedInstance.GetBlueGradient(self.view.frame.size.width/2, self.btnCallNow.frame.size.height)
        btnCallNow.layer.insertSublayer(gradeientForbtnCallNow, at: 0)
        self.get_carrierApiCall(param: NSDictionary())
        
//        let user = AppUtilites.sharedInstance.getUserData()
        self.imgProfile.sd_setImage(with: URL(string: user.avatar_image))
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let user = AppUtilites.sharedInstance.getUserData()
        self.imgProfile.sd_setImage(with: URL(string: user.avatar_image))
        
    }
    
    // MARK: - Button Delegate

    @IBAction func clkBtnBackOnlineQuoting(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func leftMenuButtonClicked(_ sender: Any) {
        self.slideMenuController()?.openLeft()
    }
    @IBAction func rightMenuButtonClicked(_ sender: Any) {
        self.slideMenuController()?.openRight()
    }
    
    // MARK: - CollectionView Delegate
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrCarrier.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CIGCollectionCell", for: indexPath) as! CIGCollectionCell
        
        let dic = self.arrCarrier[indexPath.item] as! NSDictionary
        
//        cell.blurView.isHidden = true
//        cell.imgCheckMark.isHidden = true
//
//        if(arrSelectedIndexpath.contains(indexPath.item)){
//
//            cell.blurView.isHidden = false
//            cell.imgCheckMark.isHidden = false
//        }
//
        cell.ImgAds.setIndicatorStyle(UIActivityIndicatorViewStyle.gray)
        cell.ImgAds.setShowActivityIndicator(true)
        cell.ImgAds.sd_setImage(with: URL(string: dic.value(forKey : "carrier_img") as! String))
//        cell.ImgAds.image = UIImage(named: "unnamed")
        
//        cell.contentView.backgroundColor = UIColor.blue
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
//        if(arrSelectedIndexpath.contains(indexPath.item)){
//
//            arrSelectedIndexpath.remove(indexPath.item)
//
//        }
//        else {
//
//            arrSelectedIndexpath.add(indexPath.item)
//
//        }
//        self.adsCollectionView.reloadData()
        
        let link = self.arrCarrierLinks[indexPath.item] as! String
        if(link.characters.count > 0){
//             UIApplication.shared.open( URL(string: link)!, options: [:])
            let safariVC = SFSafariViewController(url: NSURL(string: link)! as URL)
            self.present(safariVC, animated: true, completion: nil)
            safariVC.delegate = self
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionElementKindSectionHeader:
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                             withReuseIdentifier: "CIGOnlineQuotingHeaderView",
                                                                             for: indexPath) as! CIGOnlineQuotingHeaderView
            let OpenlinkGesture = UITapGestureRecognizer(target: self, action: #selector(self.myviewTapped(_:)))
            OpenlinkGesture.numberOfTapsRequired = 1
            OpenlinkGesture.numberOfTouchesRequired = 1
            headerView.addGestureRecognizer(OpenlinkGesture)
            headerView.isUserInteractionEnabled = true
            return headerView
        default:
            assert(false, "Unexpected element kind")
        }
        
        return UICollectionReusableView()
    }
    
    func myviewTapped(_ sender: UITapGestureRecognizer) {
        let url = NSURL(string: "https://itunes.apple.com/us/app/copeland-insurance-group/id1213887595?mt=")
        UIApplication.shared.open(url! as URL, options: [:], completionHandler: nil)
        
        //    let url = NSURL(string: "https://itunes.apple.com/us/app/copeland-insurance-group/id1213887595?mt=")
        //    let safariVC = SFSafariViewController(url: url as! URL)
        //    self.present(safariVC, animated: true, completion: nil)
        //    safariVC.delegate = self
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {

        let width = self.view.frame.size.width/2
        
        if(UIDevice.current.userInterfaceIdiom == .pad) {
            return CGSize(width: width - 10 , height: 140)
        }
        else {
            return CGSize(width: width - 10 , height: 70)
        }
        
    }
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients([AppUtilites.sharedInstance.teamLeadEmail])
        mailComposerVC.setSubject("CIG")
      mailComposerVC.setMessageBody(k_email_declaimer, isHTML: true)
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        
        let alert = AppUtilites.SimpleAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.")
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: MFMailComposeViewControllerDelegate
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func btnEmailUs(_ sender: Any) {
        
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
        
    }
    
    
    @IBAction func btnHelpDesk(_ sender: Any) {
        
        let link =  "https://cig.on.spiceworks.com/portal/tickets"
        if(link.characters.count > 0){
            
            let safariVC = SFSafariViewController(url: NSURL(string: link)! as URL)
            self.present(safariVC, animated: true, completion: nil)
            safariVC.delegate = self
        }
    }

    //MARK:-  API Calling
    
    func get_carrierApiCall(param : NSDictionary) {
        
        APIClient.showProgress()
        APIClient.sharedInstance.MakeAPICallWithEndURl(BASE_URL.appending(getCarrier) as NSString, parameters: param as NSDictionary!) { (response, error) in
            
            APIClient.dismissProgress()
            
            if (error == nil) {
                // code of responce...
                let resutls = response! as NSDictionary
                print(resutls)
                let status : Int = resutls.value(forKey: "status") as! Int
                if(status == 1) {
                    
                    // UserDefaults.standard.set(true, forKey: "isLogin")
                    
                    self.arrCarrier = NSMutableArray.init(array: resutls.value(forKey: "carrier") as! NSArray)
                    
                    for dic in self.arrCarrier{
                        
                        if (dic as! NSDictionary).value(forKey: "carrier_name") as! String == "Compliance"{
                            
                            self.arrCarrier.remove(dic)
                        }
                    }
                    if(UIDevice.current.userInterfaceIdiom != .pad) {
                        self.CollectionViewHeightconstraint.constant = CGFloat(self.arrCarrier.count * 45) + 20.0
                    }
                    self.collectionView.reloadData()
                    
                }else{
                    let error = resutls.value(forKey: "msg") as! String
                    
                    AppUtilites.showError(title: "Error",message: error, cancelButtonTitle:"OK")
                }
            }else{
                AppUtilites.showError(title: "Oops...",message: "Server timed out!", cancelButtonTitle:"OK")
                
            }
        }
    }
}
