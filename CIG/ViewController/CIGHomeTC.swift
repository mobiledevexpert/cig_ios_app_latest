//
//  CIGHomeTC.swift
//  CIG
//
//  Created by Harry on 10/26/17.
//  Copyright © 2017 Harry. All rights reserved.
//

import UIKit

class CIGHomeTC: UITableViewCell {

    @IBOutlet weak var imgType: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
