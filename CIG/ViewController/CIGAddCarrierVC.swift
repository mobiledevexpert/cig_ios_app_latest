//
//  CIGAddCarrierVC.swift
//  CIG
//
//  Created by Harry on 11/2/17.
//  Copyright © 2017 Harry. All rights reserved.
//

import UIKit
import MessageUI
import SafariServices

class CIGAddCarrierVC: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,SFSafariViewControllerDelegate,MFMailComposeViewControllerDelegate {

    
     var arrCarrier = NSMutableArray()
    @IBOutlet weak var btnHelpDeak: UIButton!
    @IBOutlet weak var btnCallNow: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
     @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!
    
    var gradeientForHelpDeskButton : CAGradientLayer!
    var gradeientForbtnCallNow : CAGradientLayer!
    var arrSelectedIndexpath =  NSMutableArray()

    override func viewDidLoad() {
        super.viewDidLoad()

        if (UIScreen.main.nativeBounds.height == 2436) {
            self.headerViewHeightConstraint.constant = self.headerViewHeightConstraint.constant + 20
        }
        
        btnHelpDeak.layoutIfNeeded()
        btnCallNow.layoutIfNeeded()
        
        if(UIDevice.current.userInterfaceIdiom == .pad) {
            self.get_carrierApiCall(param:NSDictionary() )
        }
        
        view.setNeedsLayout()
        view.setNeedsDisplay()
        
        gradeientForHelpDeskButton = AppUtilites.sharedInstance.GetGreenGradient(self.view.frame.size.width/2, self.btnHelpDeak.frame.size.height)
        btnHelpDeak.layer.insertSublayer(gradeientForHelpDeskButton, at: 0)
        
         gradeientForbtnCallNow = AppUtilites.sharedInstance.GetBlueGradient(self.view.frame.size.width/2, self.btnCallNow.frame.size.height)
        btnCallNow.layer.insertSublayer(gradeientForbtnCallNow, at: 0)
        
       
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        
        self.get_carrierApiCall(param:NSDictionary())
    }
    @IBAction func btnBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: - CollectionView Delegate
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrCarrier.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CIGCollectionCell", for: indexPath) as! CIGCollectionCell
        
        let dic = self.arrCarrier[indexPath.item] as! NSDictionary
        
        cell.blurView.isHidden = true
        cell.imgCheckMark.isHidden = true
        
        if(arrSelectedIndexpath.contains(indexPath.item)){
            
            cell.blurView.isHidden = false
            cell.imgCheckMark.isHidden = false
        }
        
         cell.ImgAds.setIndicatorStyle(UIActivityIndicatorViewStyle.gray)
         cell.ImgAds.setShowActivityIndicator(true)
        
        cell.ImgAds.sd_setImage(with: URL(string: dic.value(forKey : "carrier_img") as! String))
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
                if(arrSelectedIndexpath.contains(indexPath.item)){
        
                    arrSelectedIndexpath.remove(indexPath.item)
        
                }
                else {
        
                    arrSelectedIndexpath.add(indexPath.item)
        
                }
                self.collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width : CGFloat = self.collectionView.frame.size.width/2.0
        //     let width : CGFloat = self.collectionView.frame.size.width/2.0
//        return CGSize(width: width - 7 , height: 70)
        
        if(UIDevice.current.userInterfaceIdiom == .pad) {
            return CGSize(width: width - 10 , height: 140)
        }
        else {
            return CGSize(width: width - 7 , height: 70)
        }
        
    }
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients([AppUtilites.sharedInstance.teamLeadEmail])
        mailComposerVC.setSubject("CIG")
      mailComposerVC.setMessageBody(k_email_declaimer, isHTML: true)
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        
        let alert = AppUtilites.SimpleAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.")
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: MFMailComposeViewControllerDelegate
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func btnEmailUs(_ sender: Any) {
        
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
        
    }
    
    
    @IBAction func btnHelpDesk(_ sender: Any) {
        
        let link =  "https://cig.on.spiceworks.com/portal/tickets"
        if(link.characters.count > 0){
            
            let safariVC = SFSafariViewController(url: NSURL(string: link)! as URL)
            self.present(safariVC, animated: true, completion: nil)
            safariVC.delegate = self
        }
    }
    
    @IBAction func btn_continues(_ sender: UIButton) {
        
        if (self.arrSelectedIndexpath.count > 0){
        
        var carrierIDs = String()
        var arrSelectedCarrier = NSMutableArray()
        
        for var index in self.arrSelectedIndexpath{
            
            let dic = self.arrCarrier[index as! Int] as! NSDictionary
            arrSelectedCarrier.add(dic)
            
            carrierIDs = "\(carrierIDs),\(dic.value(forKey : "carrier_id")!)"
            
        }
        
            if carrierIDs.characters.count > 0 {
                
                let indexStr = carrierIDs.index(carrierIDs.startIndex, offsetBy: 1)
                
                carrierIDs = carrierIDs.substring(from: indexStr)
                
                let user = AppUtilites.sharedInstance.getUserData()
                let params = [
                    "carrier_id": carrierIDs,
                    "user_id": user.user_id
                    ] as [String : Any]
                
                APIClient.showProgress()
                APIClient.sharedInstance.MakeAPICallWithEndURl(BASE_URL.appending(updateCarrier) as NSString, parameters: params as NSDictionary!) { (response, error) in
                    
                    APIClient.dismissProgress()
                    
                    if (error == nil) {
                        // code of responce...
                        let resutls = response! as NSDictionary
                        print(resutls)
                        let status : Int = resutls.value(forKey: "status") as! Int
                        if(status == 1) {
                            
                            // UserDefaults.standard.set(true, forKey: "isLogin")
                            self.navigationController?.popViewController(animated: true)
                            
                            
                        }else{
                            let error = resutls.value(forKey: "msg") as! String
                            AppUtilites.showError(title: "Error",message: error, cancelButtonTitle:"OK")
                        }
                    }else{
                        AppUtilites.showError(title: "Oops...",message: "Server timed out!", cancelButtonTitle:"OK")
                        
                    }
                }
            }
            else{
                
                AppUtilites.showError(title: "Error",message: "Please select carrier.", cancelButtonTitle:"OK")
            }
        }
        
    }
    //MARK:-  API Calling
    
    func get_carrierApiCall(param : NSDictionary) {
        
        APIClient.showProgress()
        APIClient.sharedInstance.MakeAPICallWithEndURl(BASE_URL.appending(getCarrier) as NSString, parameters: param as NSDictionary!) { (response, error) in
            
            APIClient.dismissProgress()
            
            if (error == nil) {
                // code of responce...
                let resutls = response! as NSDictionary
                print(resutls)
                let status : Int = resutls.value(forKey: "status") as! Int
                if(status == 1) {
                    
                    // UserDefaults.standard.set(true, forKey: "isLogin")
                    
                    self.arrCarrier = NSMutableArray.init(array: resutls.value(forKey: "carrier") as! NSArray)
                    for dic in self.arrCarrier{
                        
                        if (dic as! NSDictionary).value(forKey: "carrier_name") as! String == "Compliance"{
                            
                            self.arrCarrier.remove(dic)
                        }
                    }
                    self.collectionView.reloadData()
                    
                }else{
                    let error = resutls.value(forKey: "msg") as! String
                    
                    AppUtilites.showError(title: "Error",message: error, cancelButtonTitle:"OK")
                }
            }else{
                AppUtilites.showError(title: "Oops...",message: "Server timed out!", cancelButtonTitle:"OK")
                
            }
        }
    }
}
