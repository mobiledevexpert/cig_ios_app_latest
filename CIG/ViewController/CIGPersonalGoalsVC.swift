//
//  CIGPersonalGoalsVC.swift
//  CIG
//
//  Created by Harry on 10/28/17.
//  Copyright © 2017 Harry. All rights reserved.
//

import UIKit
import Foundation

class CIGPersonalGoalsVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
   
    
    @IBOutlet weak var fullProgressView: UIView!
    @IBOutlet weak var progressViewCon: NSLayoutConstraint!
    
    @IBOutlet var goalProgressView: UIView!
    @IBOutlet var lblGoalPercentage: UILabel!
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var btnNewGoale: UIButton!
    var gradeientForbtnSubmit : CAGradientLayer!
    @IBOutlet var tableview: UITableView!
    @IBOutlet var imgView: UIView!
    @IBOutlet var imgUser: UIImageView!
    @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var btnEdit: UIButton!
    var arrGoals = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        if (UIScreen.main.nativeBounds.height == 2436) {
            self.headerViewHeightConstraint.constant = self.headerViewHeightConstraint.constant + 20
        }

        btnNewGoale.layoutIfNeeded()
        gradeientForbtnSubmit = AppUtilites.sharedInstance.GetRedGradient(self.btnNewGoale.frame.size.width, self.btnNewGoale.frame.size.height)
        btnNewGoale.layer.insertSublayer(gradeientForbtnSubmit, at: 0)
        
        
        tableview.delegate = self
        let user = AppUtilites.sharedInstance.getUserData()
        print(user.display_name)
        print(user.avatar_image)
        
        self.lblUserName.text = user.display_name.capitalized
        
        if(user.goal_status == "0" || user.goal_status.characters.count == 0){
            
            self.progressViewCon.constant = 0
            self.lblGoalPercentage.text = "0%"
        }
        else{
            
  
            let goalamount : NSString  = (user.goal_status as NSString).substring(to: 3) as NSString
            let goalamountInt = goalamount.floatValue
            
            if(goalamount == "100"){
                
                self.progressViewCon.constant =  CGFloat(self.view.frame.size.width-100)
            }
            else{
                
                self.progressViewCon.constant =  CGFloat((Float(self.fullProgressView.frame.size.width)*goalamountInt)/100)
            }
            
//            self.progressViewCon.constant =  CGFloat((Float(self.fullProgressView.frame.size.width)*goalamountInt)/100)
            self.lblGoalPercentage.text = "\(user.goal_status)%"
        }

        
    self.imgUser.setIndicatorStyle(UIActivityIndicatorViewStyle.gray)
        self.imgUser.setShowActivityIndicator(true)
        self.imgUser.sd_setImage(with: URL(string: user.avatar_image))
        self.imgUser.SetRoundImageWithShadow(image: imgUser)
        self.imgView.SetRoundView(view: imgView)
        self.imgView.SetViewShadow(view: imgView)
        
        self.getGoalDetails()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        let user = AppUtilites.sharedInstance.getUserData()
        self.imgUser.sd_setImage(with: URL(string: user.avatar_image))
        
        self.getGoalDetails()
    }
    @IBAction func btnNewCarrierClicked(_ sender: Any) {
        let goalDetailVC : CIGAddCarrierVC = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGAddCarrierVC") as! CIGAddCarrierVC
       
        self.navigationController?.pushViewController(goalDetailVC, animated: true)
    }
    
     // MARK: - Table View
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.arrGoals.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: "CIGProfileGoalsTC") as! CIGProfileGoalsTC
       
        
        let goalDic = self.arrGoals[indexPath.row] as! NSDictionary
//        let goalArr = goalDic.value(forKey: "goal_detail") as? NSArray
        
        cell.lblCarrierName.text = goalDic.value(forKey: "carrier_name") as? String
        cell.lblLeftDays.text = "Days Left for completion: \(goalDic.value(forKey: "left_day")!)"
        cell.lblItemLeft.text = "Goal Item Left: \(goalDic.value(forKey: "left_item")!)"

        
        let goalamount  = (goalDic.value(forKey: "goal_status") as! NSString).floatValue
        
        let val = Float(goalamount/100.0)
        
        cell.lblparsantage.text = "\(round(goalamount))"
        
        cell.progressView.setProgress(Double(val), animated: true)
        cell.progressView.trackFillColor = getRandomColor()
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let goalDic = self.arrGoals[indexPath.row] as! NSDictionary
        
        let goalDetailVC : CIGPersonalGoalDetailVC = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGPersonalGoalDetailVC") as! CIGPersonalGoalDetailVC
        goalDetailVC.goalDic = goalDic
        self.navigationController?.pushViewController(goalDetailVC, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            print("Deleted")
            let goalDic = self.arrGoals[indexPath.row] as! NSDictionary
            
            let user = AppUtilites.sharedInstance.getUserData()
            
            let params = [
                "user_id": user.user_id,
                "carrier_id": goalDic.value(forKey: "carrier_id") as! String
                ] as [String : Any]
            
            APIClient.showProgress()
            APIClient.sharedInstance.MakeAPICallWithEndURl(BASE_URL.appending(k_delete_carrier) as NSString, parameters: params as NSDictionary!) { (response, error) in
                
                APIClient.dismissProgress()
                
                if (error == nil) {
                    // code of responce...
                    let resutls = response! as NSDictionary
                    print(resutls)
                    let status : Int = resutls.value(forKey: "status") as! Int
                    if(status == 1) {
                        
                        self.tableview.beginUpdates()
                        self.arrGoals.removeObject(at: indexPath.row)
                        self.tableview.deleteRows(at: [indexPath], with: .fade)
                        self.tableview.endUpdates()
                        
                        
                    }else{
                        let error = resutls.value(forKey: "msg") as! String
                        
                        AppUtilites.showError(title: "Error",message: error, cancelButtonTitle:"OK")
                    }
                }else{
                    AppUtilites.showError(title: "Oops...",message: "Server timed out!", cancelButtonTitle:"OK")
                    
                }
            }
            
        }
    }
    
     // MARK: - Button Click
    
    func getRandomColor() -> UIColor{
        
        let randomRed:CGFloat = CGFloat(drand48())
        let randomGreen:CGFloat = CGFloat(drand48())
        let randomBlue:CGFloat = CGFloat(drand48())
        
        return UIColor(red: randomRed, green: randomGreen, blue: randomBlue, alpha: 1.0)
        
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func editButtonClicked(_ sender: Any) {
        
        if(self.btnEdit.titleLabel?.text == "Edit"){
            
            self.tableview.setEditing(true, animated: true)
            self.btnEdit.setTitle("Done", for: UIControlState.normal)
        }
        else{
            
            self.btnEdit.setTitle("Edit", for: UIControlState.normal)
            self.tableview.setEditing(false, animated: true)
        }
    }
    
    func getGoalDetails(){
        
        let user = AppUtilites.sharedInstance.getUserData()
        
        let params = [
            "user_id": user.user_id
            ] as [String : Any]
        
        APIClient.showProgress()
        APIClient.sharedInstance.MakeAPICallWithEndURl(BASE_URL.appending(k_get_goal_Percentage) as NSString, parameters: params as NSDictionary!) { (response, error) in
            
            APIClient.dismissProgress()
            
            if (error == nil) {
                // code of responce...
                let resutls = response! as NSDictionary
                print(resutls)
                let status : Int = resutls.value(forKey: "status") as! Int
                if(status == 1) {
                    
                    let arrGoalDetail =  resutls.value(forKey: "goal_all_detail") as! NSArray
                    self.arrGoals = NSMutableArray(array: arrGoalDetail)
                    
                    self.tableview.delegate = self
                    self.tableview.dataSource = self
                    self.tableview.reloadData()
                    
                    
                }else{
                    let error = resutls.value(forKey: "msg") as! String
                    
                    AppUtilites.showError(title: "Error",message: error, cancelButtonTitle:"OK")
                }
            }else{
                AppUtilites.showError(title: "Oops...",message: "Server timed out!", cancelButtonTitle:"OK")
                
            }
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
