//
//  CIGSettingsViewController.swift
//  CIG
//
//  Created by Harry on 11/1/17.
//  Copyright © 2017 Harry. All rights reserved.
//

import UIKit

class CIGSettingsViewController: UIViewController {

    @IBOutlet var lblBrowserName: UILabel!
    @IBOutlet var lblLocation: UILabel!
    @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()

        if (UIScreen.main.nativeBounds.height == 2436) {
            self.headerViewHeightConstraint.constant = self.headerViewHeightConstraint.constant + 20
        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnAboutClicked(_ sender: Any) {
       
        let controller = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGAboutViewController") as! CIGAboutViewController
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    
    @IBAction func btnWeatherLocationClicked(_ sender: Any) {
      
        let controller = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGFlashScreenVC") as! CIGFlashScreenVC
        controller.isLoadedFromSettings = true
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    @IBAction func btnDoneClicked(_ sender: Any) {
         self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnSignOutClicked(_ sender: Any) {
        
        UserDefaults.standard.set(false, forKey: "isLogin")
        
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: "user_dis")
        
        let loginVC  = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGLoginVC") as! CIGLoginVC
        let nav = UINavigationController(rootViewController: loginVC)
        nav.navigationBar.isHidden = true
        let appDel : AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDel.window = UIWindow(frame: UIScreen.main.bounds)
        appDel.window?.rootViewController = nav
        appDel.window?.makeKeyAndVisible()
        
    }
    
    @IBAction func btnDefaultBrowserClicked(_ sender: Any) {
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
