//
//  CIGLeadsVC.swift
//  CIG
//
//  Created by Harry on 10/30/17.
//  Copyright © 2017 Harry. All rights reserved.
//

import UIKit
import SafariServices

import MessageUI

class CIGLeadsVC: UIViewController ,SFSafariViewControllerDelegate,MFMailComposeViewControllerDelegate{

    
    @IBOutlet var lblGoalPercentage: UILabel!
    @IBOutlet weak var fullProgressView: UIView!
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var lblGood: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var btnHelpDesk: UIButton!
    @IBOutlet weak var btnEmail: UIButton!
    @IBOutlet weak var progressViewCon: NSLayoutConstraint!
    @IBOutlet weak var progressView: UIView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!
    
    var gradeientForHeader : CAGradientLayer!
    var gradeientForBtnEdit : CAGradientLayer!
    var gradeientForProgressView : CAGradientLayer!
    var gradeientForHelpDeskButton : CAGradientLayer!
    var gradeientForbtnCallNow : CAGradientLayer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (UIScreen.main.nativeBounds.height == 2436) {
            self.headerViewHeightConstraint.constant = self.headerViewHeightConstraint.constant + 20
        }

        
        imgProfile.SetRoundImageWithBorder(image: imgProfile)
        btnHelpDesk.layoutIfNeeded()
        btnEmail.layoutIfNeeded()
        headerView.layoutIfNeeded()
        progressView.layoutIfNeeded()
//        progressViewCon.constant = AppUtilites.sharedInstance.getWidthFromPercentage(percentage: 75)
        
        self.slideMenuController()?.removeLeftGestures()
        self.slideMenuController()?.removeRightGestures()
        self.slideMenuController()?.addLeftGestures()
        self.slideMenuController()?.addRightGestures()
        let screenSize: CGRect = UIScreen.main.bounds
        self.slideMenuController()?.changeLeftViewWidth(screenSize.width - 20);
        self.slideMenuController()?.changeRightViewWidth(screenSize.width - 20)
        
        progressView.layoutIfNeeded()
        btnHelpDesk.layoutIfNeeded()
        btnEmail.layoutIfNeeded()
        
       
        
        
        let user = AppUtilites.sharedInstance.getUserData()
        print(user.display_name)
        print(user.avatar_image)
        
         self.lblGood.text = AppUtilites.sharedInstance.getCurrentstatus()
        self.lblName.text = user.display_name
        self.imgProfile.setIndicatorStyle(UIActivityIndicatorViewStyle.gray)
        self.imgProfile.setShowActivityIndicator(true)
        
        if(user.goal_status == "0" || user.goal_status.characters.count == 0){
            
            self.progressViewCon.constant = 0
            self.lblGoalPercentage.text = "0%"
        }
        else{
            
            let goalamount : NSString  = (user.goal_status as NSString).substring(to: 3) as NSString
            let goalamountInt = goalamount.floatValue
            
            if(goalamount == "100"){
                
                self.progressViewCon.constant =  CGFloat(self.view.frame.size.width-150)
            }
            else{
                
                self.progressViewCon.constant =  CGFloat((Float(self.fullProgressView.frame.size.width)*goalamountInt)/100)
            }
            
//            self.progressViewCon.constant =  CGFloat((Float(self.fullProgressView.frame.size.width)*goalamountInt)/100)
            self.lblGoalPercentage.text = "\(user.goal_status)%"
        }
        
        self.imgProfile.sd_setImage(with: URL(string: user.avatar_image))
        
        gradeientForHeader = AppUtilites.sharedInstance.GetRedGradient(ScreenSize.SCREEN_WIDTH, headerView.frame.size.height)
        headerView.layer.insertSublayer(gradeientForHeader, at: 0)
        
        gradeientForProgressView = AppUtilites.sharedInstance.GetBlueGradient(progressViewCon.constant, progressView.frame.size.height)
        progressView.layer.insertSublayer(gradeientForProgressView, at: 0)
        
        gradeientForHelpDeskButton = AppUtilites.sharedInstance.GetGreenGradient(self.btnHelpDesk.frame.size.width, self.btnHelpDesk.frame.size.height)
        btnHelpDesk.layer.insertSublayer(gradeientForHelpDeskButton, at: 0)
        
        gradeientForbtnCallNow = AppUtilites.sharedInstance.GetBlueGradient(self.btnEmail.frame.size.width, self.btnEmail.frame.size.height)
        btnEmail.layer.insertSublayer(gradeientForbtnCallNow, at: 0)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
     
        let user = AppUtilites.sharedInstance.getUserData()
        self.imgProfile.sd_setImage(with: URL(string: user.avatar_image))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    

    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
       mailComposerVC.setToRecipients([AppUtilites.sharedInstance.teamLeadEmail])
        mailComposerVC.setSubject("CIG")
      mailComposerVC.setMessageBody(k_email_declaimer, isHTML: true)
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        
        let alert = AppUtilites.SimpleAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.")
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: MFMailComposeViewControllerDelegate
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
   
    // MARK: - Button Clicked

    @IBAction func clkBtnBackLeads(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnMenuClicked(_ sender: Any) {
         self.slideMenuController()?.openLeft()
    }
    
    @IBAction func btnLeftMenuClicked(_ sender: Any) {
          self.slideMenuController()?.openRight()
    }
    
    @IBAction func btnRequestLeadClicked(_ sender: Any) {
        
     
        let controller = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGRequestLeadsVC") as! CIGRequestLeadsVC
   
        
        
        let leftSideMenuVC  = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGLeftSideMenuVC") as? CIGLeftSideMenuVC
        
        let rightSideMenuVC  = AppUtilites.sharedInstance.appDelegateShared.mainStoryboard.instantiateViewController(withIdentifier: "CIGRightSideMenuVC") as? CIGRightSideMenuVC
        
        let slideMenuController = SlideMenuController(mainViewController: controller, leftMenuViewController: leftSideMenuVC!, rightMenuViewController: rightSideMenuVC!)
        let nav = UINavigationController(rootViewController: slideMenuController)
        nav.navigationBar.isHidden = true
        self.navigationController?.pushViewController(slideMenuController, animated: true)
        
    }
    
    @IBAction func btnReturnClicked(_ sender: Any) {
        
    }
    
    @IBAction func btnHelpClicked(_ sender: Any) {
        
        let link =  "https://cig.on.spiceworks.com/portal/tickets"
        if(link.characters.count > 0){
            
            let safariVC = SFSafariViewController(url: NSURL(string: link)! as URL)
            self.present(safariVC, animated: true, completion: nil)
            safariVC.delegate = self
        }
        
    }
    
    @IBAction func btnEmailClicked(_ sender: Any) {
        
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
        
    }
}
