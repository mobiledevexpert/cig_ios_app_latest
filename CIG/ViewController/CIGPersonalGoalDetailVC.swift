//
//  CIGPersonalGoalDetailVC.swift
//  CIG
//
//  Created by Harry on 10/31/17.
//  Copyright © 2017 Harry. All rights reserved.
//

import UIKit

class CIGPersonalGoalDetailVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {

    
    @IBOutlet weak var fullProgressView: UIView!
    @IBOutlet weak var progressViewCon: NSLayoutConstraint!
    @IBOutlet var goalProgressView: UIView!
    @IBOutlet var lblGoalPercentage: UILabel!
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var btnNewGoale: UIButton!
    var gradeientForbtnSubmit : CAGradientLayer!
    @IBOutlet var tableview: UITableView!
    @IBOutlet var imgView: UIView!
    @IBOutlet var imgUser: UIImageView!
    @IBOutlet var blurView: UIView!
    @IBOutlet var txtYearlyGoal: UITextField!
    @IBOutlet var txtCommision: UITextField!
    @IBOutlet var txtPlanName: UITextField!
    @IBOutlet var addGoalView: UIView!
    @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var btnSubmit: UIButton!
    var goalDic = NSDictionary()
    var arrGolaData = NSMutableArray()
    var selectedGoalId = String()
    
    
    @IBAction func CloseButtonClicked(_ sender: Any) {
        
        self.blurView.isHidden = true
        self.addGoalView.isHidden = true
        
        self.txtPlanName.text = ""
        self.txtCommision.text = ""
        self.txtYearlyGoal.text = ""
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (UIScreen.main.nativeBounds.height == 2436) {
            self.headerViewHeightConstraint.constant = self.headerViewHeightConstraint.constant + 20
        }

        
        btnNewGoale.layoutIfNeeded()
        gradeientForbtnSubmit = AppUtilites.sharedInstance.GetRedGradient(self.btnNewGoale.frame.size.width, self.btnNewGoale.frame.size.height)
        btnNewGoale.layer.insertSublayer(gradeientForbtnSubmit, at: 0)
        
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = blurView.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.blurView.addSubview(blurEffectView)
        
        
        txtCommision.delegate = self
        txtYearlyGoal.delegate = self
        
        tableview.delegate = self
        let user = AppUtilites.sharedInstance.getUserData()
        print(user.display_name)
        print(user.avatar_image)
        
        self.arrGolaData = NSMutableArray(array: self.goalDic.value(forKey: "goal_detail") as! NSArray)
        
        self.lblUserName.text = user.display_name.capitalized
        
        self.imgUser.setIndicatorStyle(UIActivityIndicatorViewStyle.gray)
        self.imgUser.setShowActivityIndicator(true)
        self.imgUser.sd_setImage(with: URL(string: user.avatar_image))
        self.imgUser.SetRoundImageWithShadow(image: imgUser)
        self.imgView.SetRoundView(view: imgView)
        self.imgView.SetViewShadow(view: imgView)
        
        if(user.goal_status == "0" || user.goal_status.characters.count == 0){
            
            self.progressViewCon.constant = 0
            self.lblGoalPercentage.text = "0%"
        }
        else{
            
            let goalamount : NSString  = (user.goal_status as NSString).substring(to: 3) as NSString
            let goalamountInt = goalamount.floatValue
//            self.progressViewCon.constant =  CGFloat((Float(self.fullProgressView.frame.size.width)*goalamountInt)/100)
            
            if(goalamount == "100"){
                
                self.progressViewCon.constant =  CGFloat(self.view.frame.size.width-100)
            }
            else{
                
                self.progressViewCon.constant =  CGFloat((Float(self.fullProgressView.frame.size.width)*goalamountInt)/100)
            }
            
            self.lblGoalPercentage.text = "\(user.goal_status)%"
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        let user = AppUtilites.sharedInstance.getUserData()
        self.imgUser.sd_setImage(with: URL(string: user.avatar_image))
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        let allowedCharacters = CharacterSet.decimalDigits
//        let DecimalPointSet = CharacterSet(charactersIn: ".")
//        let mainCharSet = allowedCharacters.union(DecimalPointSet)
        let characterSet = CharacterSet(charactersIn: string)
        return allowedCharacters.isSuperset(of: characterSet)
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func btnBack(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSubmitClicked(_ sender: Any) {
        
        
        if(self.txtPlanName.text!.characters.count == 0){
            
            AppUtilites.showError(title: "Error",message: "Enter plan name", cancelButtonTitle:"OK")
        }
        else if(self.txtCommision.text!.characters.count == 0){
            
            AppUtilites.showError(title: "Error",message: "Enter commission", cancelButtonTitle:"OK")
            
        }else if(self.txtYearlyGoal.text!.characters.count == 0){
            
            AppUtilites.showError(title: "Error",message: "Enter yearly goal", cancelButtonTitle:"OK")
        }
        else{
            
            let alertController = UIAlertController(title: "Info", message: "This change will affect your yearly Commission Goal. Would you like to Continue?", preferredStyle: UIAlertControllerStyle.alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
            {
                (result : UIAlertAction) -> Void in
                
                let user = AppUtilites.sharedInstance.getUserData()
                var param : [String : Any]!
                
                APIClient.showProgress()
               
                var apiURL = ""
                
                if(self.btnSubmit.titleLabel?.text == "EDIT"){
                    
                    apiURL = k_edit_goal
                     param = ["carrier_id": self.goalDic.value(forKey: "carrier_id") as! String,
                                 "user_id": user.user_id,
                                 "product_name":self.txtPlanName.text as! String,
                                 "price1":self.txtCommision.text as! String,
                                 "price2":self.txtYearlyGoal.text as! String,
                                 "goal_id":self.selectedGoalId
                        ] as [String : Any]
                    print(param.description)
                }
                else{
                    apiURL = k_update_user_goal
                     param = ["carrier_id": self.goalDic.value(forKey: "carrier_id") as! String,
                                 "user_id": user.user_id,
                                 "product_name":self.txtPlanName.text as! String,
                                 "price1":self.txtCommision.text as! String,
                                 "price2":self.txtYearlyGoal.text as! String
                        ] as [String : Any]
                    print(param.description)
                }
                
                APIClient.sharedInstance.MakeAPICallWithEndURl(BASE_URL.appending(apiURL) as NSString, parameters: param as NSDictionary!) { (response, error) in
                    
                    APIClient.dismissProgress()
                    
                    if (error == nil) {
                        // code of responce...
                        let resutls = response! as NSDictionary
                        print(resutls)
                        let status : Int = resutls.value(forKey: "status") as! Int
                        if(status == 1) {
                            
                            self.blurView.isHidden = true
                            self.addGoalView.isHidden = true
                            
                            self.txtPlanName.text = ""
                            self.txtCommision.text = ""
                            self.txtYearlyGoal.text = ""
                            
                            self.getGoalDetail()
                            
                            
                        }else{
                            let error = resutls.value(forKey: "msg") as! String
                            
                            AppUtilites.showError(title: "Error",message: error, cancelButtonTitle:"OK")
                        }
                    }else{
                        AppUtilites.showError(title: "Oops...",message: "Server timed out!", cancelButtonTitle:"OK")
                        
                    }
                }
            }
            alertController.addAction(okAction)
            
            let CancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default)
            {
                (result : UIAlertAction) -> Void in
                print("You pressed Cancel")
            }
            alertController.addAction(CancelAction)
            
            self.present(alertController, animated: true, completion: nil)
            
        }
        
    }
    
    func editGoalDetails (){
        
    }
    func getGoalDetail(){
        
        let user = AppUtilites.sharedInstance.getUserData()
        
        let params = [
            "user_id": user.user_id
            ] as [String : Any]
        
        APIClient.showProgress()
        APIClient.sharedInstance.MakeAPICallWithEndURl(BASE_URL.appending(k_get_goal_Percentage) as NSString, parameters: params as NSDictionary!) { (response, error) in
            
            APIClient.dismissProgress()
            
            if (error == nil) {
                // code of responce...
                let resutls = response! as NSDictionary
                print(resutls)
                let status : Int = resutls.value(forKey: "status") as! Int
                if(status == 1) {
                    
                    let arrGoalDetail =  resutls.value(forKey: "goal_all_detail") as! NSArray
                    
                    for dic in arrGoalDetail{
                        
                        if ((dic as! NSDictionary).value(forKey: "carrier_id") as! String) == (self.goalDic.value(forKey: "carrier_id") as! String){
                            
                            self.goalDic = dic as! NSDictionary
                            self.arrGolaData = NSMutableArray(array: self.goalDic.value(forKey: "goal_detail") as! NSArray)
                            break
                        }
                        
                    }
//                    let goalDic = self.arrGoals[indexPath.row] as! NSDictionary
                    
                    self.tableview.delegate = self
                    self.tableview.dataSource = self
                    self.tableview.reloadData()
                    
                    
                }else{
                    let error = resutls.value(forKey: "msg") as! String
                    
                    AppUtilites.showError(title: "Error",message: error, cancelButtonTitle:"OK")
                }
            }else{
                AppUtilites.showError(title: "Oops...",message: "Server timed out!", cancelButtonTitle:"OK")
                
            }
        }
    }
    // MARK: - Table View
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
         return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if(self.arrGolaData.count > 0 ){
            
            return self.arrGolaData.count + 2
        }
        return 1

    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if(indexPath.row == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "CIGPersonalGoalHeaderTC") as! CIGPersonalGoalHeaderTC
                        
            cell.lblTitle.text = self.goalDic.value(forKey: "carrier_name") as? String
            
            let goalamount  = (goalDic.value(forKey: "goal_status") as! NSString).floatValue
            
            let val = Float(goalamount/100.0)
            
            cell.lblPercentage.text = "\(round(goalamount))"
            cell.progressView.setProgress(Double(val), animated: true)
            cell.progressView.trackFillColor = getRandomColor()
            
            return cell
        }
        if(indexPath.row ==  self.arrGolaData.count + 1) {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "CIGPersonalGoalCommisionTC") as! CIGPersonalGoalCommisionTC
            cell.lblCommitionTotal.text = "=  $\(self.goalDic.value(forKey: "carrier_total") as! NSNumber)"
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CIGPersonalGoalProductTC") as! CIGPersonalGoalProductTC
        
        let Dic = (self.arrGolaData[indexPath.row - 1]) as! NSDictionary

        cell.lblPlanName.text = Dic.value(forKey: "product_name") as? String
        cell.lblCommision.text = "$\(Dic.value(forKey: "price1")!)"
        cell.lblItemLeft.text = "\(Dic.value(forKey: "price2")!)"
        if ("\(Dic.value(forKey: "complete_goal")!)" == "0"){
            
             cell.btnDelete.isHidden = false
        }
        else{
             cell.btnDelete.isHidden = true
        }
        cell.btnDelete.tag = indexPath.row - 1
        
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.tableview.deselectRow(at: indexPath, animated: true)
        
        let Dic = (self.arrGolaData[indexPath.row - 1]) as! NSDictionary
        self.blurView.isHidden = false
        self.addGoalView.isHidden =  false
        self.selectedGoalId = (Dic.value(forKey: "goal_id") as? String)!
        self.btnSubmit.setTitle("EDIT", for: UIControlState.normal)
        self.txtPlanName.text = Dic.value(forKey: "product_name") as? String
        self.txtCommision.text = "\(Dic.value(forKey: "price1")!)"
        self.txtYearlyGoal.text = "\(Dic.value(forKey: "price2")!)"
    }
    
//     func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
//
//        if editingStyle == .delete {
//
//            let Dic = ((self.goalDic.value(forKey: "goal_detail") as! NSArray)[indexPath.row - 1]) as! NSDictionary
//
//            if (Dic.value(forKey: "complete_goal") as! String == "0"){
//
//            }
//            else{
//
//                return
//            }
//        }
//    }
    
//    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
//
//        var Dic = NSDictionary()
//        print(indexPath.row)
//        print((self.goalDic.value(forKey: "goal_detail") as? NSArray)!.count)
//
//        if(indexPath.row > 0 && indexPath.row < (self.goalDic.value(forKey: "goal_detail") as? NSArray)!.count + 1){
//
//             Dic = ((self.goalDic.value(forKey: "goal_detail") as! NSArray)[indexPath.row - 1]) as! NSDictionary
//        }
//
//        if(indexPath.row > 0 && Dic.value(forKey: "complete_goal") as! String == "0") || (indexPath.row < (self.goalDic.value(forKey: "goal_detail") as? NSArray)!.count + 1 && Dic.value(forKey: "complete_goal") as! String == "0"){
//
//                let delete = UITableViewRowAction(style: .destructive, title: "DELETE") { (action, indexPath) in
//                    // delete item at indexPath
//                }
//                return [delete]
//        }else {
//            return []
//        }
//    }
    @IBAction func btnDeleteGoalClicked(_ sender: Any) {
        
        let Dic = ((self.goalDic.value(forKey: "goal_detail") as! NSArray)[(sender as! UIButton).tag]) as! NSDictionary
        
        let params = [
            "goal_id": Dic.value(forKey:"goal_id") as! String
            ] as [String : Any]
        
        APIClient.showProgress()
        APIClient.sharedInstance.MakeAPICallWithEndURl(BASE_URL.appending(k_delete_goal) as NSString, parameters: params as NSDictionary!) { (response, error) in
            
            APIClient.dismissProgress()
            
            if (error == nil) {
                // code of responce...
                let resutls = response! as NSDictionary
                print(resutls)
                let status : Int = resutls.value(forKey: "status") as! Int
                if(status == 1) {
                    
//                    self.tableview.beginUpdates()
//                    (self.goalDic.value(forKey: "goal_detail") as! NSArray).removeObject(at: (sender as! UIButton).tag - 1])
//                    self.arrGolaData.removeObject(at: (sender as! UIButton).tag)
//                    self.tableview.deleteRows(at: [IndexPath(row: (sender as! UIButton).tag - 1, section: 1)], with: UITableViewRowAnimation.fade)
//                    self.tableview.endUpdates()
//                    self.tableview.reloadData()
                    
                    self.getGoalDetail()
                    
                    
                }else{
                    
                    let error = resutls.value(forKey: "msg") as! String
                    AppUtilites.showError(title: "Error",message: error, cancelButtonTitle:"OK")
                }
            }else{
                AppUtilites.showError(title: "Oops...",message: "Server timed out!", cancelButtonTitle:"OK")
                
            }
        }

    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
       
    }
    @IBAction func newGoalClicked(_ sender: Any) {
        
        self.btnSubmit.setTitle("SUBMIT", for: UIControlState.normal)

        self.blurView.isHidden = false
        self.addGoalView.isHidden = false
    }
    func getRandomColor() -> UIColor{
        
        let randomRed:CGFloat = CGFloat(drand48())
        let randomGreen:CGFloat = CGFloat(drand48())
        let randomBlue:CGFloat = CGFloat(drand48())
        
        return UIColor(red: randomRed, green: randomGreen, blue: randomBlue, alpha: 1.0)
        
    }
}
