//
//  CIGMyStatsVC.swift
//  CIG
//
//  Created by Harry on 11/4/17.
//  Copyright © 2017 Harry. All rights reserved.
//

import UIKit

class CIGMyStatsVC: UIViewController {
    
    @IBOutlet var btnReturn: UIButton!
    @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if (UIScreen.main.nativeBounds.height == 2436) {
            self.headerViewHeightConstraint.constant = self.headerViewHeightConstraint.constant + 20
        }
        
        let  gradeientForbtnCallNow = AppUtilites.sharedInstance.GetGreenGradient(ScreenSize.SCREEN_WIDTH-100, self.btnReturn.frame.size.height)
        btnReturn.layer.insertSublayer(gradeientForbtnCallNow, at: 0)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   
    // MARK: - Navigation

 @IBAction func btnreturnClicked(_ sender: Any) {
 self.navigationController?.popViewController(animated: true)
 
 }
 
 @IBAction func btnbackClicked(_ sender: Any) {
 
 self.navigationController?.popViewController(animated: true)
 
 }

}
