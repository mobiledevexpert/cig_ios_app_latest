//
//  CIGGoalProductTC.swift
//  CIG
//
//  Created by Harry on 10/30/17.
//  Copyright © 2017 Harry. All rights reserved.
//

import UIKit

class CIGGoalProductTC: UITableViewCell {

    @IBOutlet var lblPrice2: UILabel!
    @IBOutlet var lblPrice1: UILabel!
    @IBOutlet var lblProductName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
