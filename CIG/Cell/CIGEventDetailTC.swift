//
//  CIGEventDetailTC.swift
//  CIG
//
//  Created by Harry on 11/3/17.
//  Copyright © 2017 Harry. All rights reserved.
//

import UIKit

class CIGEventDetailTC: UITableViewCell {

    @IBOutlet var btnViewMore: UIButton!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblDateTime: UILabel!
    @IBOutlet var btnSavetoCalender: UIButton!
    @IBOutlet var lblLocation: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
