
//
//  CIGCopelandContactsTC.swift
//  CIG
//
//  Created by Harry on 10/30/17.
//  Copyright © 2017 Harry. All rights reserved.
//

import UIKit



class CIGCopelandContactsTC: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnCall: MyButton!
    @IBOutlet weak var btnSendMail: MyButton!
    @IBOutlet weak var btnGetDirection: MyButton!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var mainView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
