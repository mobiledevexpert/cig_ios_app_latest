//
//  CIGPersonalGoalHeaderTC.swift
//  CIG
//
//  Created by Harry on 10/31/17.
//  Copyright © 2017 Harry. All rights reserved.
//

import UIKit
import CircleProgressView

class CIGPersonalGoalHeaderTC: UITableViewCell {

    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var btnEdit: UIButton!
    @IBOutlet var progressView: CircleProgressView!
    @IBOutlet var lblPercentage: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
