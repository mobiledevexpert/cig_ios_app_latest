//
//  CIGCollectionCell.swift
//  CIG
//
//  Created by Harry on 10/24/17.
//  Copyright © 2017 Harry. All rights reserved.
//

import UIKit

class CIGCollectionCell: UICollectionViewCell {
    
    @IBOutlet var blurView: UIView!
    @IBOutlet var imgCheckMark: UIImageView!
    @IBOutlet var ImgAds: UIImageView!
    
}
