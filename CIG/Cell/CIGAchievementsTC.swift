//
//  CIGAchievementsTC.swift
//  CIG
//
//  Created by Harry on 10/31/17.
//  Copyright © 2017 Harry. All rights reserved.
//

import UIKit

class CIGAchievementsTC: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnOnePlus: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        
         if(UIDevice.current.userInterfaceIdiom == .pad) {
            AppUtilites.sharedInstance.setCornerRadius(view: btnOnePlus, cornerRadius: 20, borderColor: UIColor.clear, borderWidth: 0)
        }else
         {
        AppUtilites.sharedInstance.setCornerRadius(view: btnOnePlus, cornerRadius: 15, borderColor: UIColor.clear, borderWidth: 0)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
