//
//  CIGCarrierAlertTC.swift
//  CIG
//
//  Created by Harry on 11/6/17.
//  Copyright © 2017 Harry. All rights reserved.
//

import UIKit
import KILabel
import TTTAttributedLabel

class CIGCarrierAlertTC: UITableViewCell {

    @IBOutlet var headerView: UIView!
    @IBOutlet var lblDisc: TTTAttributedLabel!
    @IBOutlet var lblDateTime: UILabel!
    @IBOutlet var headerImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        lblDisc.enabledTextCheckingTypes = NSTextCheckingResult.CheckingType.link.rawValue
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
