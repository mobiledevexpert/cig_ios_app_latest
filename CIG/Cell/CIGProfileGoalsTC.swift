//
//  CIGProfileGoalsTC.swift
//  CIG
//
//  Created by Harry on 10/28/17.
//  Copyright © 2017 Harry. All rights reserved.
//

import UIKit
import CircleProgressView

class CIGProfileGoalsTC: UITableViewCell {

    @IBOutlet var lblItemLeft: UILabel!
    @IBOutlet var lblLeftDays: UILabel!
    @IBOutlet var lblCarrierName: UILabel!
    @IBOutlet var lblparsantage: UILabel!
    @IBOutlet var progressView: CircleProgressView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
