//
//  CIGEventTC.swift
//  CIG
//
//  Created by Harry on 10/28/17.
//  Copyright © 2017 Harry. All rights reserved.
//

import UIKit

class CIGEventTC: UITableViewCell {

    @IBOutlet var btnViewMore: UIButton!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblDateTime: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func btnViewMoreClicked(_ sender: Any) {
    }
}
