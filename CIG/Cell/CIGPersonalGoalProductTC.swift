//
//  CIGPersonalGoalProductTC.swift
//  CIG
//
//  Created by Harry on 10/31/17.
//  Copyright © 2017 Harry. All rights reserved.
//

import UIKit

class CIGPersonalGoalProductTC: UITableViewCell {

    @IBOutlet var btnDelete: UIButton!
    @IBOutlet var lblItemLeft: UILabel!
    @IBOutlet var lblCommision: UILabel!
    @IBOutlet var lblPlanName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
