//
//  AMUserInfo.swift
//  ApployMe
//
//  Created by Harry on 8/29/17.
//  Copyright © 2017 Harry. All rights reserved.
//

import UIKit

class AMUserInfo: NSObject {

    var avatar_image =  ""
    var created_at = ""
    var display_name = ""
    var email = ""
    var goal_status = ""
    var updated_at = ""
    var user_id = ""
    var team_id = ""
    
    override init() {
    }
    
    
    class func AMUserInfoFromDictionary(userData: NSDictionary)-> AMUserInfo
    {
        let data:AMUserInfo = AMUserInfo()
        
        let avatar_image = userData.value(forKeyPath: "avatar") as? String
        let created_at = userData.value(forKeyPath: "created") as? String
        let display_name = userData.value(forKeyPath: "display_name") as? String
        let email = userData.value(forKeyPath: "email") as? String
        
        let updated_at = userData.value(forKeyPath: "updated") as? String
        let user_id = userData.value(forKeyPath: "user_id") as? String
        let team_id = userData.value(forKeyPath: "team_id") as? String
      
        if let goal_status = userData.value(forKeyPath: "goal_status") as? String{
            
            data.goal_status = String(describing: goal_status)
        }
        
        if(team_id != "" && team_id != nil)
        {
            data.team_id = team_id!
        }
        
        if(display_name != "" && display_name != nil)
        {
            data.display_name = display_name!
        }
        if(avatar_image != "" && avatar_image != nil)
        {
            data.avatar_image = avatar_image!
        }
        if(updated_at != "" && updated_at != nil)
        {
            data.updated_at = updated_at!
        }
        if(user_id != "" && user_id != nil)
        {
            data.user_id = user_id!
        }
        if(email != "" && email != nil)
        {
            data.email = email!
        }
        if(created_at != "" && created_at != nil)
        {
            data.created_at = created_at!
        }
      
        return data
    }
}
