//
//  Constants.swift
//  ApployMe
//
//  Created by Harry on 8/4/17.
//  Copyright © 2017 Harry. All rights reserved.
//


import Foundation


//test
//let BASE_URL = "http://appdev.copelandgroupusa.com/cigapp/index.php/apiv1/"

//updated live
//let BASE_URL = "http://app.copelandgroupusa.com/cigapp/index.php/apiv3/"
let BASE_URL = "http://app.copelandgroupusa.com/cigapp/index.php/apiv4/"

//live
//let BASE_URL = "http://app.copelandgroupusa.com/cigapp/index.php/apiv1/"



let loginUrl = "login"
let signupUrl = "signup"
let getCarrier = "get_carrier"
let updateCarrier = "update_user_carrier"
let getGoalDetail = "get_goal_detail"
let k_ForgotPassword = "forgot_password"
let k_verify_token = "verify_token"
let k_reset_password = "reset_password"
let k_update_user_goal = "update_user_goal"
let k_get_team = "get_team_contacts"
let k_get_weather = "weather"
let k_get_goal_Percentage = "get_goal_pecetage"
let k_delete_goal = "delete_goal"
let k_delete_carrier = "delete_user_carrier"
let k_update_profile = "update_profile"
let k_lead_Request = "save_lead"
let k_increase_goal = "increase_goal"
let k_event_list = "event_list"
let k_get_profile = "get_profile"
let k_get_team_contact_detail = "get_team_contact_detail"
let k_change_team = "change_team"
let k_carrier_alert_data = "carrier_alert_data"
let k_edit_goal = "update_goal"
let k_add_token = "add_update_token"



let k_email_declaimer = """
<br/>
<br/>
<br/>
<hr>
<p>Thanks,</p>
<p>APP MARKETING</p>
<p>Visit us online at: www.copelandgroupusa.com</p>
<p>Agents: www.copelandgroupmarketing.com</p>
<p>Visit us: 1203 W. Loop 281 – Longview, TX 75604</p>
<p>Disclaimer: This electronic message, e-mail transmission, and any documents, files or previous e-mail messages attached to it, may contain information that is Proprietary, Confidential, or legally privileged or protected. It is intended only for the use of the individual(s) and entity named in the message. If you are not an intended recipient of this message, please notify the sender immediately and delete the material from your computer. DO NOT use “reply” or “reply all” feature to notify sender. Please create a “new e-mail” between you and the sender to notify the sender or call 1-877-619-0025 and ask for a Compliance Department officer. If you are not the intended recipient, or a person responsible for delivering it to the intended recipient, you are hereby notified: Do not deliver, distribute or copy this message and do not disclose its contents or take any action in reliance on the information it contains.</p>
"""
//<pre>Thanks,
//APP MARKETING
//
//Visit us online at: www.copelandgroupusa.com
//Agents: www.copelandgroupmarketing.com
//Visit us: 1203 W. Loop 281 – Longview, TX 75604
//Disclaimer: This electronic message, e-mail transmission, and any documents, files or previous e-mail messages attached to it, may contain information that is Proprietary, Confidential, or legally privileged or protected. It is intended only for the use of the individual(s) and entity named in the message. If you are not an intended recipient of this message, please notify the sender immediately and delete the material from your computer. DO NOT use “reply” or “reply all” feature to notify sender. Please create a “new e-mail” between you and the sender to notify the sender or call 1-877-619-0025 and ask for a Compliance Department officer. If you are not the intended recipient, or a person responsible for delivering it to the intended recipient, you are hereby notified: Do not deliver, distribute or copy this message and do not disclose its contents or take any action in reliance on the information it contains.</pre>
//"""



