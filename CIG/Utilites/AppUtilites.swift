//
//  AppUtilites.swift
//  ApployMe
//
//  Created by Harry on 8/4/17.
//  Copyright © 2017 Harry. All rights reserved.
//

import UIKit
import SVProgressHUD
import OpinionzAlertView
import Photos
import SafariServices



class AppUtilites: NSObject,SFSafariViewControllerDelegate {
    
    
    public var amUserInfo: AMUserInfo = AMUserInfo()
    var scanStatus = true
    var ready = false
    let regiterDis : NSDictionary = NSDictionary()
    let skillTempArray : NSMutableArray = NSMutableArray()
//    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let appDelegateShared : AppDelegate = UIApplication.shared.delegate as! AppDelegate
    var IconImage = UIImage()
    var allTeam : NSArray = NSArray()
    var selectedTeam : NSDictionary = NSDictionary()
    var currentTemp : String = String()
    var isgoalUpdated = Bool()
    var teamLeadEmail = String()
    
    class var sharedInstance : AppUtilites {
        
        struct Static {
            static let instance : AppUtilites = AppUtilites()
        }
        return Static.instance
    }
    
    func setCornerRadius(view: UIView,cornerRadius: CGFloat,borderColor: UIColor,borderWidth: CGFloat) {
        
        view.layer.cornerRadius = cornerRadius
        view.layer.borderColor = borderColor.cgColor
        view.layer.borderWidth = borderWidth
        view.clipsToBounds = true
    }
    

    class func timeAgoStringFromDate(date: Date) -> String? {
        let formatter = DateComponentsFormatter()
        formatter.unitsStyle = .full

        let now = Date()

        let calendar = NSCalendar.current
        let unitFlags = Set<Calendar.Component>([.day, .month, .year, .hour, .weekOfMonth, .minute, .second])
        let components = calendar.dateComponents(unitFlags, from: date, to: now)


        if components.year! > 0 {
            formatter.allowedUnits = .year
        } else if components.month! > 0 {
            formatter.allowedUnits = .month
        } else if components.weekOfMonth! > 0 {
            formatter.allowedUnits = .weekOfMonth
        } else if components.day! > 0 {
            formatter.allowedUnits = .day
        } else if components.hour! > 0 {
            formatter.allowedUnits = .hour
        } else if components.minute! > 0 {
            formatter.allowedUnits = .minute
        } else {
            formatter.allowedUnits = .second
        }

        let formatString = NSLocalizedString("%@ ago", comment: "Used to say how much time has passed. e.g. '2 hours ago'")

        guard let timeString = formatter.string(from: components) else {
            return nil
        }
        return String(format: formatString, timeString) as String
    }
    
    func getCurrentstatus() -> String
    {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh a"
        let str: String = dateFormatter.string(from: date)
        print("\(str)")
        var array = str.components(separatedBy: " ")
        var message = "Good Morning"
        let timeInHour: String = array[0]
        let am_pm: String = array[1]
        if Int(timeInHour) ?? 0 < 12 && (am_pm == "AM" || am_pm == "am") {
            message = "Good Morning"
        }
        else if Int(timeInHour) ?? 0 <= 4 && (am_pm == "PM" || am_pm == "pm") {
            message = "Good Afternoon"
        }
        else if Int(timeInHour) ?? 0 == 12 && (am_pm == "PM" || am_pm == "pm") {
            message = "Good Afternoon"
        }
        else if Int(timeInHour) ?? 0 > 4 && (am_pm == "PM" || am_pm == "pm") {
            
            if  Int(timeInHour) ?? 0 > 8
            {
                 message = "Good Night"
            } else
            {
                 message = "Good Evening"
            }
        }
        
       print("\(message)")
        
        return message
    }
    
    func getWidthFromPercentage(percentage: CGFloat)->CGFloat
    {
        let totalProgressWidth = ScreenSize.SCREEN_WIDTH - 170
        return totalProgressWidth * percentage / 100
    }
    
    func isValidatePresence(string: String) -> Bool {
        
        let trimmed: String = string.trimmingCharacters(in: CharacterSet.whitespaces)
        return !trimmed.isEmpty
    }
    
    func isValidPostalCode(string:String) -> Bool {
        let emailRegEx = "^[0-9A-Za-z]{5,7}$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: string)
    }
    
    
    
    func isValidEmail(string:String) -> Bool {
        
       // let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
       let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
      
//        let emailRegEx = "^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: string)
    }
    
  
    func isValidPhoneNumber(string: String) -> Bool {
        let PHONE_REGEX = "^\\d{10}"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        return  phoneTest.evaluate(with: string)
    }
    
    func isOnlyNumber(string: String) -> Bool {
        let PHONE_REGEX = "^[0-9]*"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        return  phoneTest.evaluate(with: string)
    }
    
    func isValidPhoneNumberWithCode(string: String) -> Bool {
        let PHONE_REGEX = "^\\+?91?\\s*\\(?-*\\.*(\\d{3})\\)?\\.*-*\\s*(\\d{3})\\.*-*\\s*(\\d{4})$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        return  phoneTest.evaluate(with: string)
    }
    
    func setCallContactNO(number: String) -> String {
        
        let contact_no : String = number
        var finalContactNo = contact_no.replacingOccurrences(of: " ", with: "")
        finalContactNo = finalContactNo.replacingOccurrences(of: "(", with: "")
        finalContactNo = finalContactNo.replacingOccurrences(of: ")", with: "")
        finalContactNo = finalContactNo.replacingOccurrences(of: "-", with: "")
        
        return finalContactNo
        
    }
    

    func GetRedGradient(_ widthofView:CGFloat,_ heightofView: CGFloat) -> CAGradientLayer
    {
        let redGradientLayerForHeader = CAGradientLayer()
        redGradientLayerForHeader.frame = CGRect(x: 0, y: 0, width: widthofView, height: heightofView)
        redGradientLayerForHeader.colors = [UIColor(red: 162.0/255.0, green: 14.0/255.0, blue: 6.0/255.0, alpha: 1).cgColor,
                                            UIColor(red: 241.0/255.0, green: 73.0/255.0, blue: 64.0/255.0, alpha: 1).cgColor,
                                            UIColor(red: 162.0/255.0, green: 14.0/255.0, blue: 6.0/255.0, alpha: 1).cgColor]
        redGradientLayerForHeader.startPoint = CGPoint(x: 0 , y: 0)
        redGradientLayerForHeader.endPoint = CGPoint(x: 1.0, y: 1)
        redGradientLayerForHeader.locations = [0.0,0.25,0.60]
     
     return redGradientLayerForHeader
        
    }
    
    func GetGreenGradient(_ widthofView:CGFloat,_ heightofView: CGFloat) -> CAGradientLayer
    {
        let redGradientLayerForHeader = CAGradientLayer()
        redGradientLayerForHeader.frame = CGRect(x: 0, y: 0, width: widthofView, height: heightofView)
        redGradientLayerForHeader.colors = [UIColor(red: 76.0/255.0, green: 164.0/255.0, blue: 88.0/255.0, alpha: 1.0).cgColor,
                                            UIColor(red: 92.0/255.0, green: 203.0/255.0, blue: 108.0/255.0, alpha: 1.0).cgColor,
                                            UIColor(red: 17.0/255.0, green: 126.0/255.0, blue: 32.0/255.0, alpha: 1.0).cgColor]
        redGradientLayerForHeader.startPoint = CGPoint(x: 0 , y: 0)
        redGradientLayerForHeader.endPoint = CGPoint(x: 1.0, y: 1)
        redGradientLayerForHeader.locations = [0.0,0.25,0.60]
        
        return redGradientLayerForHeader
        
    }
    
    func GetBlueGradient(_ widthofView:CGFloat,_ heightofView: CGFloat) -> CAGradientLayer
    {
        let redGradientLayerForHeader = CAGradientLayer()
        redGradientLayerForHeader.frame = CGRect(x: 0, y: 0, width: widthofView, height: heightofView)
        redGradientLayerForHeader.colors = [UIColor(red: 15.0/255.0, green: 77.0/255.0, blue: 199.0/255.0, alpha: 1.0).cgColor,
                                            UIColor(red: 42.0/255.0, green: 101.0/255.0, blue: 219.0/255.0, alpha: 1.0).cgColor,
                                            UIColor(red: 1.0/255.0, green: 51.0/255.0, blue: 148.0/255.0, alpha: 1.0).cgColor]
        redGradientLayerForHeader.startPoint = CGPoint(x: 0 , y: 0)
        redGradientLayerForHeader.endPoint = CGPoint(x: 1.0, y: 1)
        redGradientLayerForHeader.locations = [0.0,0.25,0.60]
        
        return redGradientLayerForHeader
        
    }
    

    func setPhoneNoFormate(no: String) -> String {
        let testNo = String(no.characters.reversed())
        var ab = testNo.toPhoneNumber()
        ab = String(ab.characters.prefix(14))
        return String(ab.characters.reversed())
    }
    
   //MARK: - SimpleAlertView
    
    class func SimpleAlertView(title:String, message: String) -> UIAlertController{
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
        {
            (result : UIAlertAction) -> Void in
            print("You pressed OK")
        }
        alertController.addAction(okAction)
        
        return alertController
        
//        calling time
//        let alert = AppUtilites.SimpleAlertView(title: "ok", message: "data")
//        self.present(alert, animated: true, completion: nil)
    }
    
    
    

    //MARK: - AlertView
  
    
     class func showError(title: String,message: String,cancelButtonTitle: String) {
        let alert = OpinionzAlertView(title: title, message: message, cancelButtonTitle: cancelButtonTitle, otherButtonTitles: nil)
        alert?.iconType = OpinionzAlertIconWarning
        alert?.show()
    }
    
 
    class func showSucess(message: String) {
        let alert = OpinionzAlertView(title: "Congratulations", message: message, cancelButtonTitle: "OK", otherButtonTitles: nil)
        alert?.iconType = OpinionzAlertIconSuccess
        alert?.color = UIColor(red: 0.15, green: 0.68, blue: 0.38, alpha: 1)
        alert?.show()
    }
    
   
    
    class func showWarning(message: String) {
        let alert = OpinionzAlertView(title: "Warning", message: message, cancelButtonTitle: "OK", otherButtonTitles: nil)
        alert?.iconType = OpinionzAlertIconWarning
        alert?.show()
    }
    
    
    class func showInfo(message: String) {
        let alert = OpinionzAlertView(title: "Info", message: message, cancelButtonTitle: "OK", otherButtonTitles: nil)
        alert?.iconType = OpinionzAlertIconInfo
        alert?.show()
    }
    
    
    class func showCustome(title:String,message: String) {
        let alert = OpinionzAlertView(title: title, message: message, cancelButtonTitle: "OK", otherButtonTitles: nil)
        alert?.icon = UIImage(named: "heart")
        alert?.color = UIColor(red: 0.61, green: 0.35, blue: 0.71, alpha: 1)
        alert?.show()
    }
    
    
    
    //MARK: - ProgressView
    class func showProgress() {
        
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.gradient)
        SVProgressHUD.show()
    }
    
    class func dismissProgress() {
        
        SVProgressHUD.dismiss()
    }
    
    func CallNow(_ sender: Any) {
        
        if let url = URL(string: "tel://1-800-220-8899") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url)
            } else {
                // Fallback on earlier versions
            }
        }
    }
    
    func openHelpDesk(_ sender: Any) {
        
        
        let link =  "https://cig.on.spiceworks.com/portal/tickets"
//        if(link.characters.count > 0){
//
//            let safariVC = SFSafariViewController(url: NSURL(string: link)! as URL)
//            self.present(safariVC, animated: true, completion: nil)
//            safariVC.delegate = self
//        }
    }
    
    
  
    func saveUserData(data: NSDictionary) {
        
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: "user_dis")
        defaults.setValue(data, forKey: "user_dis")
        defaults.synchronize()
    }
    
    func getUserData() ->AMUserInfo {
        
        let defaults = UserDefaults.standard
        let data = defaults.value(forKey: "user_dis") as! NSDictionary
        AppUtilites.sharedInstance.amUserInfo = AMUserInfo.AMUserInfoFromDictionary(userData: data)
        let UserData = AppUtilites.sharedInstance.amUserInfo
        return UserData
        
    }
    
    
    
//    func saveBusinessData(data: NSDictionary) {
//
//
//        let defaults = UserDefaults.standard
//        defaults.removeObject(forKey: "business_dis")
//        defaults.setValue(data, forKey: "business_dis")
//        defaults.synchronize()
//     }
//
//    func getBusinessData() ->AMBusinessInfo {
//
//       let defaults = UserDefaults.standard
//       let data = defaults.value(forKey: "business_dis") as! NSDictionary
//       AppUtilites.sharedInstance.amBusinessInfo = AMBusinessInfo.AMBusinessInfoFromDictionary(userData: data)
//       let BusinessData = AppUtilites.sharedInstance.amBusinessInfo
//
//       return BusinessData
//
//    }
    
  
   
    func UIImage2CGimage(_ image: UIImage?) -> CGImage? {
        if let tryImage = image, let tryCIImage = CIImage(image: tryImage) {
            return CIContext().createCGImage(tryCIImage, from: tryCIImage.extent)
        }
        return nil
    }
}

extension UILabel {
    func SetRoundLabel(view : UILabel) {
        view.layer.masksToBounds = false
        view.layer.cornerRadius = view.frame.size.width/2
        view.clipsToBounds = true
    }
}
extension UIView {
    func SetRoundView(view : UIView) {
        view.layer.masksToBounds = false
        view.layer.cornerRadius = view.frame.size.width/2
        view.clipsToBounds = true
    }
}
extension UIImageView {
    func SetRoundImage(image : UIImageView) {
        image.layer.borderWidth = 1.0
        image.layer.masksToBounds = false
        image.layer.borderColor = UIColor(red:160.0/255.0, green:171.0/255.0, blue:192.0/255.0, alpha: 1.0).cgColor
        image.layer.cornerRadius = image.frame.size.width/2
        image.clipsToBounds = true
    }
    func SetRoundImageWithBorder(image : UIImageView) {
        image.layer.borderWidth = 2.0
        image.layer.borderColor = UIColor.white.cgColor
        image.layer.masksToBounds = false
        image.layer.cornerRadius = image.frame.size.width/2
        image.clipsToBounds = true
    }
}

extension UIImageView {
    func SetRoundImageWithoutBorder(image : UIImageView) {
        image.layer.masksToBounds = false
        image.layer.cornerRadius = image.frame.size.width/2
        image.clipsToBounds = true
    }
}
extension UIView {
    func SetRoundButton(view : UIView) {
        view.layer.cornerRadius = 22
        view.layer.borderWidth = 1
        
    }
}

extension UIImageView {
    func SetRoundImageWithShadow(image : UIImageView) {
        
        image.layer.borderWidth = 3.0
        image.layer.masksToBounds = false
        image.layer.borderColor = UIColor.white.cgColor
        image.layer.cornerRadius = image.frame.size.width/2
        image.clipsToBounds = true
    
    }
}


extension UIView {
    func SetViewShadow(view : UIView) {
        
        view.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        view.layer.shadowOffset = CGSize(width:0.0,height: 0.8)
        view.layer.shadowOpacity = 0.8
        view.layer.shadowRadius = 0.0
        view.layer.masksToBounds = false
      }
}

extension UIView {
    func SetView(view : UIView) {
        view.layer.cornerRadius = 10
        view.layer.borderWidth = 2
        view.layer.borderColor = UIColor(red:160.0/255.0, green:171.0/255.0, blue:192.0/255.0, alpha: 1.0).cgColor
    }
}
extension UIViewController
{
    func hideKeyboard()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(UIViewController.dismissKeyboard))
        
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard()
    {
        view.endEditing(true)
    }
}

extension String {
    //: ### Base64 encoding a string
    func base64Encoded() -> String? {
        if let data = self.data(using: .utf8) {
            return data.base64EncodedString()
        }
        return nil
    }
    
    //: ### Base64 decoding a string
    func base64Decoded() -> String? {
        if let data = Data(base64Encoded: self) {
            return String(data: data, encoding: .utf8)
        }
        return nil
    }
}
extension String {
    public func toPhoneNumber() -> String {
        return self.replacingOccurrences(of: "(\\d{4})(\\d{3})(\\d{3})(\\d+)", with: "$1-$2 )$3( $4", options: .regularExpression, range: nil)
        
    }
    
    
}
public class MyButton:UIButton{
    
    var myRow: Int?
    var mySection: Int?
    
}
