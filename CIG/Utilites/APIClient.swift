//
//  APIClient.swift
//  ApployMe
//
//  Created by Harry on 8/4/17.
//  Copyright © 2017 Harry. All rights reserved.
//

import UIKit
import AFNetworking
import SVProgressHUD
//import ReachabilitySwift



class APIClient: NSObject,NSURLConnectionDataDelegate{
    
    class var sharedInstance : APIClient {
        
        struct Static {
            static let instance : APIClient = APIClient()
        }
        return Static.instance
    }
    
    
    let manager : AFHTTPRequestOperationManager = AFHTTPRequestOperationManager()
    let sessionManager : AFHTTPSessionManager = AFHTTPSessionManager()
    let JSONResponse : AFJSONResponseSerializer = AFJSONResponseSerializer()
    var responseData : NSMutableData!
    
    func MakeAPICallWithEndURl(_ url: NSString!, parameters: NSDictionary!, completionHandler:@escaping (NSDictionary?, NSError?)->()) ->() {
        
        print(url)
        print(parameters)
        
         manager.responseSerializer = JSONResponse
         manager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Content-Type")
        // manager.requestSerializer.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
        if NetConnection.isConnectedToNetwork() == true
        {
            manager.post(url as String, parameters: parameters, constructingBodyWith: { (formData: AFMultipartFormData!) -> Void in
                
            }, success: { (operation: AFHTTPRequestOperation!, responseData: Any!) -> Void in
                var responseDict : NSDictionary!
                
                if((responseData as AnyObject).count > 0){
                    responseDict = responseData as! NSDictionary
                }
                completionHandler(responseDict,nil)
            }, failure: { (operation,error : Error) -> Void in
                completionHandler(nil, error as NSError?)
                print("Error \(error)")
            })
        }
        else
        {
            AppUtilites.dismissProgress()
            AppUtilites.showInfo(message: "No internet, please check network connection and try again.")
        }
        
    }
    
    func postImageToServer(_ url: NSString, image: UIImage!, parameters: NSDictionary!, completionHandler:@escaping (NSDictionary?, NSError?)->())->(){
        
        print("Requesting \( url)")
        print("Parameters: \(parameters!)")
        
        let imageData = UIImageJPEGRepresentation(image!, 0.5)
        
//        let imageData  = UIImagePNGRepresentation(image)
        
        if (imageData != nil)
        {
            manager.post(url as String, parameters: parameters!, constructingBodyWith: { (formData: AFMultipartFormData!) -> Void in
                formData.appendPart(withFileData: imageData!, name: "avatar", fileName: "photo.jpg", mimeType: "image/jpeg")
            }, success: { (operation :AFHTTPRequestOperation, responseData : Any!) -> Void in
                var responseDict : NSDictionary!
                if((responseData as AnyObject).count > 0) {
                    
                    responseDict = responseData as! NSDictionary
                    print(responseDict)
                    completionHandler(responseDict,nil)
                    
                }
            },
               failure: { (operation, error : Error) -> Void in
                completionHandler(nil, error as NSError?)
            })
        }
    }
    
    func postImageToServerForLead(_ url: NSString, image: UIImage!, parameters: NSDictionary!, completionHandler:@escaping (NSDictionary?, NSError?)->())->(){
        
        print("Requesting \( url)")
        print("Parameters: \(parameters!)")
        
        let imageData = UIImageJPEGRepresentation(image!, 0.5)
        
        //        let imageData  = UIImagePNGRepresentation(image)
        
        if (imageData != nil)
        {
            manager.post(url as String, parameters: parameters!, constructingBodyWith: { (formData: AFMultipartFormData!) -> Void in
                formData.appendPart(withFileData: imageData!, name: "referance_material", fileName: "photo.jpg", mimeType: "image/jpeg")
            }, success: { (operation :AFHTTPRequestOperation, responseData : Any!) -> Void in
                var responseDict : NSDictionary!
                if((responseData as AnyObject).count > 0) {
                    
                    responseDict = responseData as! NSDictionary
                    print(responseDict)
                    completionHandler(responseDict,nil)
                    
                }
            },
               failure: { (operation, error : Error) -> Void in
                completionHandler(nil, error as NSError?)
            })
        }
    }
    
    //MARK: - ProgressView
    
    class func showProgress() {
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.black)
        SVProgressHUD.show()
    }
    
    class func dismissProgress() {
        SVProgressHUD.dismiss()
    }
}

