//
//  AppDelegate.swift
//  CIG
//
//  Created by Harry on 10/23/17.
//  Copyright © 2017 Harry. All rights reserved.
//

import UIKit
import IQKeyboardManager
import UserNotifications
import CoreLocation
import MapKit



@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate {

    var window: UIWindow?
    let locationManager:CLLocationManager = CLLocationManager()
     var mainStoryboard: UIStoryboard!


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        IQKeyboardManager.shared().isEnabled = true
        IQKeyboardManager.shared().isEnableAutoToolbar = true
        UIApplication.shared.statusBarStyle = .lightContent
    
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
       
        if(UIDevice.current.userInterfaceIdiom == .pad) {
            mainStoryboard = UIStoryboard(name: "iPadStoryboard", bundle: nil)
        }
        else {
            mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        }
        
        
        let flashScreenVC  = mainStoryboard.instantiateViewController(withIdentifier: "CIGFlashScreenVC") as? CIGFlashScreenVC
        

        let nav = UINavigationController(rootViewController: flashScreenVC!)
        nav.navigationBar.isHidden = true
       
        self.window?.rootViewController = nav
        self.window?.makeKeyAndVisible()
        
        
        // Check for Location Services
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        }
        
        // iOS 10 support
        if #available(iOS 10, *) {
            UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
            application.registerForRemoteNotifications()
        }
            // iOS 9 support
        else if #available(iOS 9, *) {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
            // iOS 8 support
        else if #available(iOS 8, *) {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
            // iOS 7 support
        else {
            application.registerForRemoteNotifications(matching: [.badge, .sound, .alert])
        }
        
        return true
    }

    // MARK - CLLocationManagerDelegate
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        locationManager.stopUpdatingLocation()
        let location = locations.last
        let lat = String(describing: location?.coordinate.latitude as Any)
        let lon = String(describing: location?.coordinate.longitude as Any)

        //UserDefaults.standard.set(location?.coordinate.latitude, forKey: "savedlatitude")
        //UserDefaults.standard.set(location?.coordinate.longitude, forKey: "savedlongitude")
        
        //UserDefaults.standard.set("\(location?.coordinate.latitude),\(location?.coordinate.longitude)", forKey: "savedCurrentLocation")
        
        let params = [
            "latitude":  location?.coordinate.latitude as Any  ,
            "longitude":  location?.coordinate.longitude as Any
            ] as [String : Any]
        
        
        CLGeocoder().reverseGeocodeLocation(location!) { (placemarks, error) in
            if((error) != nil){
                
            }
            if let placemarkcount = placemarks?.count
            {
                if placemarkcount > 0{
                    let pm = placemarks![0] as! CLPlacemark
                    if ((pm.postalCode) != nil){
                        let zipcode = pm.locality
                        UserDefaults.standard.set(zipcode, forKey: "currentCity")
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateLocationNotification"), object: nil)
                        //print(zipcode)
                    }else{
                        
                    }
                }else{
                    
                }
            }
        }
        
        APIClient.sharedInstance.MakeAPICallWithEndURl(BASE_URL.appending(k_get_weather) as NSString, parameters: params as NSDictionary!) { (response, error) in

            if (error == nil) {
                // code of responce...
                let resutls = response! as NSDictionary
                print(resutls)
                print(((resutls.value(forKey: "weather") as! NSDictionary).value(forKey: "main") as! NSDictionary).value(forKey: "temp") as! NSNumber)
                
                UserDefaults.standard.set(((resutls.value(forKey: "weather") as! NSDictionary).value(forKey: "main") as! NSDictionary).value(forKey: "temp") as! NSNumber, forKey: "CurrentTemp")
                UserDefaults.standard.synchronize()
                AppUtilites.sharedInstance.currentTemp = "\(((resutls.value(forKey: "weather") as! NSDictionary).value(forKey: "main") as! NSDictionary).value(forKey: "temp") as! NSNumber)"

            }else{

            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
    }
    
    
    // Called when APNs has assigned the device a unique token
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // Convert token to string
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        
        // Print it to console
        print("APNs device token: \(deviceTokenString)")
        
        UserDefaults.standard.set(deviceTokenString, forKey: "k_DeviceToken")
        
         if(UserDefaults.standard.bool(forKey: "isLogin")){
            let user = AppUtilites.sharedInstance.getUserData()
            self.addDeviceToken(token: deviceTokenString , userId: (user.user_id) as! String)
        }
        
        // Persist it in your backend in case it's new
    }
    
    // Called when APNs failed to register the device for push notifications
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        // Print the error to console (you should alert the user that registration failed)
        print("APNs registration failed: \(error)")
    }

    func addDeviceToken(token : String ,userId : String){
        
        APIClient.showProgress()
        let param = ["user_id": userId, "device_token": token]
        APIClient.sharedInstance.MakeAPICallWithEndURl(BASE_URL.appending(k_add_token) as NSString, parameters: param as NSDictionary!) { (response, error) in
            
            APIClient.dismissProgress()
            
            if (error == nil) {
                // code of responce...
                let resutls = response! as NSDictionary
                print(resutls)
                
            }else{
                
                //                AppUtilites.showError(title: "Oops...",message: "Server timed out!", cancelButtonTitle:"OK")
            }
        }
    }
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

